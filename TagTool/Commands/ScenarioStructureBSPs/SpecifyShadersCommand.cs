﻿using System;
using System.Collections.Generic;
using BlamCore.Cache;
using BlamCore.TagDefinitions;

namespace TagTool.Commands.ScenarioStructureBSPs
{
    class SpecifyShadersCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private CachedTagInstance Tag { get; }
        private ScenarioStructureBsp Definition { get; }

        public SpecifyShadersCommand(GameCacheContext cacheContext, CachedTagInstance tag, ScenarioStructureBsp definition)
            : base(CommandFlags.Inherit,

                  "SpecifyShaders",
                  "Allows the shaders of a render_model to be respecified.",

                  "SpecifyShaders",

                  "Allows the shaders of a scenario_sbsp to be respecified.")
        {
            CacheContext = cacheContext;
            Tag = tag;
            Definition = definition;
        }

        public override bool Execute(List<string> args)
        {
            foreach (var material in Definition.Materials)
            {
                if (material.RenderMethod != null)
                    Console.Write("Please enter the replacement {0:X8} index: ", material.RenderMethod.Index);
                else
                    Console.Write("Please enter the replace material #{0} index: ", Definition.Materials.IndexOf(material));

                try
                {
                    material.RenderMethod = ArgumentParser.ParseTagSpecifier(CacheContext, Console.ReadLine());
                }
                catch (Exception)
                {
                    material.RenderMethod = CacheContext.GetTag(0x101F);
                }
                
            }

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                var context = new TagSerializationContext(cacheStream, CacheContext, Tag);
                CacheContext.Serializer.Serialize(context, Definition);
            }

            Console.WriteLine("Done!");

            return true;
        }
    }
}
