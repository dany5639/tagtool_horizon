﻿using System.Collections.Generic;
using BlamCore.Cache;
using BlamCore.TagDefinitions;

namespace TagTool.Commands.Models
{
    class ExtractModelCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private Model Definition { get; }

        public ExtractModelCommand(GameCacheContext cacheContext, Model model)
            : base(CommandFlags.Inherit,

                  "ExtractModel",
                  "Extracts a render model from the current model definition.",

                  "ExtractModel <variant> <filetype> <filename>",

                  "Extracts a variant of the render model to a file.\n" +
                  "Use the \"ListVariants\" command to list available variants.\n" +
                  "If the model does not have any variants, just use \"default\".\n" +
                  "Supported file types: obj")
        {
            CacheContext = cacheContext;
            Definition = model;
        }
        
        public override bool Execute(List<string> args)
        {
            if (args.Count != 3)
                return false;

            var variantName = args[0];
            var fileType = args[1];
            var fileName = args[2];

            if (fileType != "obj")
                return false;

            return Definition.ExtractObj(CacheContext, variantName, fileName);
        }
    }
}