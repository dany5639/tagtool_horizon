﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using BlamCore.Cache;
using BlamCore.TagDefinitions;
using BlamCore.TagResources;
using BlamCore.Common;
using BlamCore.Serialization;
using BlamCore.IO;
using BlamCore.Legacy.Base;
using System.Linq;

namespace TagTool.Commands.Tags
{
    class TestCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private CachedTagInstance Tag { get; }

        public TestCommand(GameCacheContext cacheContext) : base(
            CommandFlags.None,

            "T",
            "",

            "T",

            "")
        {
            CacheContext = cacheContext;
        }

        public bool Execute1(List<string> args)
        {
            #region Load ED tag
            CachedTagInstance Tag = CacheContext.GetTag(0x444B);

            ScenarioStructureBsp sbspDefault;

            Console.Write("Deserializing; ");
            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                var edContext = new TagSerializationContext(cacheStream, CacheContext, Tag);
                sbspDefault = CacheContext.Deserializer.Deserialize<ScenarioStructureBsp>(edContext);
            }
            #endregion

            #region Load ED tag
            CachedTagInstance Tag2 = CacheContext.GetTag(0x444A);

            ScenarioStructureBsp sbspRekt;

            Console.Write("Deserializing; ");
            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                var edContext2 = new TagSerializationContext(cacheStream, CacheContext, Tag2);
                sbspRekt = CacheContext.Deserializer.Deserialize<ScenarioStructureBsp>(edContext2);
            }
            #endregion

            // using (var stream = CacheContext.TagCacheFile.Open(FileMode.Open, FileAccess.ReadWrite))
            // {
            //     var context = new TagSerializationContext(stream, CacheContext, Tag);
            //     CacheContext.Serializer.Serialize(context, edSbsp2);
            // }

            return true;
        }

        public override bool Execute(List<string> args)
        {
            #region Open cache file.
            Console.WriteLine("Reading Blam cache file...");

            var blamCacheFile = new FileInfo(@"D:\Halo\Map packs\ODSTmaps\L200.map");

            if (!blamCacheFile.Exists)
                throw new FileNotFoundException(blamCacheFile.FullName);

            CacheFile BlamCache = null;

            using (var fs = new FileStream(blamCacheFile.FullName, FileMode.Open, FileAccess.Read))
            {
                var reader = new EndianReader(fs, EndianFormat.BigEndian);

                var head = reader.ReadInt32();

                if (head == 1684104552)
                    reader.Format = EndianFormat.LittleEndian;

                var v = reader.ReadInt32();

                reader.SeekTo(284);
                var version = CacheVersionDetection.GetFromBuildName(reader.ReadString(32));

                switch (version)
                {
                    case CacheVersion.Halo3Beta:
                        BlamCache = new BlamCore.Legacy.Halo3Beta.CacheFile(blamCacheFile, version);
                        break;

                    case CacheVersion.Halo3Retail:
                        BlamCache = new BlamCore.Legacy.Halo3Retail.CacheFile(blamCacheFile, version);
                        break;

                    case CacheVersion.Halo3ODST:
                        BlamCache = new BlamCore.Legacy.Halo3ODST.CacheFile(blamCacheFile, version);
                        break;

                    default:
                        throw new NotSupportedException(CacheVersionDetection.GetBuildName(version));
                }
            }

            BlamCache.LoadResourceTags();
            #endregion

            #region Read blam tag.
            Console.WriteLine("Reading Blam Tag ...");

            CacheFile.IndexItem blamTag = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "sbsp" && tag.Filename == @"levels\atlas\l200\l200_000")
                {
                    blamTag = tag;
                    break;
                }
            }

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamSbsp = blamDeserializer.Deserialize<ScenarioStructureBsp_test>(blamContext);
            #endregion

            #region Load ED tag
            Console.WriteLine("Reading ED Tag...");

            CachedTagInstance Tag = CacheContext.GetTag(0x4446);

            Scenario edSbsp;
            
            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                var edContext = new TagSerializationContext(cacheStream, CacheContext, Tag);
                edSbsp = CacheContext.Deserializer.Deserialize<Scenario>(edContext);
            }
            #endregion

            // for (int i = 0; i < blamSbsp.InstancedGeometryInstances.Count; i++)
            // {
            //     if (blamSbsp.InstancedGeometryInstances[i].CollisionDefinitions.Count > 0)
            //         edSbsp.InstancedGeometryInstances[i].CollisionDefinitions[0] = blamSbsp.InstancedGeometryInstances[i].CollisionDefinitions[0];
            // }

            Console.WriteLine("Writing new tag...");

            using (var stream = CacheContext.TagCacheFile.Open(FileMode.Open, FileAccess.ReadWrite))
            {
                var context = new TagSerializationContext(stream, CacheContext, Tag);
                CacheContext.Serializer.Serialize(context, blamSbsp);
            }

            Console.WriteLine("Done.");

            return true;
        }


        public void AddSpawnpoint(List<string> args)
        {
            CachedTagInstance Tag = CacheContext.GetTag(0x4451);

            Scenario blamScenario;

            Console.Write("Deserializing; ");
            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                var edContext = new TagSerializationContext(cacheStream, CacheContext, Tag);
                blamScenario = CacheContext.Deserializer.Deserialize<Scenario>(edContext);
            }


            #region Add spawnpoint
            blamScenario.SceneryPalette = new List<Scenario.SceneryPaletteBlock>();
            blamScenario.Scenery = new List<Scenario.SceneryBlock>();

            CachedTagInstance mp_spawnpoint = CacheContext.GetTag(0x2E90);  // default spawnpoint; neutral; default 0x2E90

            foreach (var tag2 in CacheContext.TagCache.Index.FindAllInGroup("scen"))
            {
                if (CacheContext.TagNames[tag2.Index].Contains("mp_spawn_point"))
                {
                    mp_spawnpoint = tag2;
                }
            }

            blamScenario.SceneryPalette.Add(new Scenario.SceneryPaletteBlock { Scenery = mp_spawnpoint });

            float spawnpointX = 17f;
            float spawnpointY = -20f;
            float spawnpointZ = 20f;

            float spawnpointI = 0;
            float spawnpointJ = 0;
            float spawnpointK = 0;

            var scenerySpawnpointDefault = new Scenario.SceneryBlock
            {
                PaletteIndex = (short)blamScenario.SceneryPalette.IndexOf(blamScenario.SceneryPalette.Find(x => x.Scenery == mp_spawnpoint)),
                NameIndex = 0,
                PlacementFlags = Scenario.ObjectPlacementFlags.None,
                Position = new RealPoint3d(spawnpointX, spawnpointY, spawnpointZ),
                Rotation = new RealEulerAngles3d(Angle.FromDegrees(spawnpointI), Angle.FromDegrees(spawnpointJ), Angle.FromDegrees(spawnpointK)),
                UniqueIdIndex = 49096,
                UniqueIdSalt = 8,
                ObjectTypeOld = GameObjectTypeHalo3ODST.Scenery,
                ObjectTypeNew = GameObjectTypeHaloOnline.Scenery,
                Source = Scenario.SceneryBlock.SourceValue.Editor,
                Unknown3 = -1,
                ParentNameIndex = -1,
                AllowedZoneSets = 1,
                Unknown12 = -1,
                Team = Scenario.SceneryBlock.TeamValue.Neutral,
                Unknown18 = 0
            };

            blamScenario.Scenery.Add(scenerySpawnpointDefault);

            Console.WriteLine("Added Spawnpoints");
            #endregion

            #region Add Camera for use as mainmenu map. Change map type to Mainmenu
            float cameraX = 13.95461f;
            float cameraY = 4.51311f;
            float cameraZ = 0.6763648f;
            float cameraI = 139.1801f;
            float cameraJ = 14.51703f;
            float cameraK = -12.21716f;
            blamScenario.CutsceneCameraPoints = new List<Scenario.CutsceneCameraPoint>();
            blamScenario.CutsceneCameraPoints.Add(new Scenario.CutsceneCameraPoint
            {
                Name = "main_menu_cam_point_0",
                Position = new RealPoint3d(cameraX, cameraY, cameraZ),
                Orientation = new RealEulerAngles3d(
                    Angle.FromDegrees(cameraI),
                    Angle.FromDegrees(cameraJ),
                    Angle.FromDegrees(cameraK)
                )
            });
            #endregion


            using (var stream = CacheContext.TagCacheFile.Open(FileMode.Open, FileAccess.ReadWrite))
            {
                var context = new TagSerializationContext(stream, CacheContext, Tag);
                CacheContext.Serializer.Serialize(context, blamScenario);
            }




            // Console.Write("blah(0x44D3); ");
            // blah(0x44D3);
            // Console.Write("blah(0x44DA); ");
            // blah(0x44DA);
            // Console.Write("blah(0x44DB); ");
            // blah(0x44DB);
            // Console.Write("blah(0x44DC);");
            // blah(0x44DC);

            Console.Write("End");

        }

        public void blah(Int32 tag)
        {
            CachedTagInstance Tag = CacheContext.GetTag(tag);

            ScenarioStructureBsp sbsp;

            Console.Write("Deserializing; ");
            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                var edContext = new TagSerializationContext(cacheStream, CacheContext, Tag);
                sbsp = CacheContext.Deserializer.Deserialize<ScenarioStructureBsp>(edContext);
            }
            Console.Write("CollisionMaterials; ");
            foreach (var a in sbsp.CollisionMaterials)
            {
                a.RenderMethod = CacheContext.GetTag(0x101F);
                Console.Write(".");
            }

            Console.Write("Materials; ");
            foreach (var a in sbsp.Materials)
            {
                a.RenderMethod = CacheContext.GetTag(0x101F);
                Console.Write(".");
            }

            Console.Write("Serialize; ");
            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                var context = new TagSerializationContext(cacheStream, CacheContext, Tag);
                CacheContext.Serializer.Serialize(context, sbsp);
            }
            Console.Write("End blah()");
        }
    }
}