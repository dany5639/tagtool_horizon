﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlamCore.Cache;
using BlamCore.Common;

namespace TagTool.Commands.Tags
{
    class ListUnusedTagsCommand : Command
    {
        public GameCacheContext CacheContext { get; }

        public ListUnusedTagsCommand(GameCacheContext cacheContext)
            : base(CommandFlags.None,

                  "ListUnusedTags",
                  "Lists all unreferenced tags in the current tag cache",

                  "ListUnusedTags",

                  "Lists all unreferenced tags in the current tag cache")
        {
            CacheContext = cacheContext;
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count != 0)
                return false;

            var nonNullTags = CacheContext.TagCache.Index.NonNull();

            foreach (var tag in CacheContext.TagCache.Index)
            {
                if (tag == null || tag.IsInGroup("cfgt") || tag.IsInGroup("scnr"))
                    continue;
                
                var dependsOn = nonNullTags.Where(t => t.Dependencies.Contains(tag.Index));

                if (dependsOn.Count() == 0)
                {
                    Console.Write($"{CacheContext.TagNames[tag.Index]} ");
                    TagPrinter.PrintTagShort(tag);
                }
            }

            return true;
        }
    }
}