﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.TagDefinitions;
using System;

namespace TagTool.Commands.Tags
{
    class SaveTagNamesCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private Dictionary<int, object> LoadedDefinitions { get; } = new Dictionary<int, object>();

        public SaveTagNamesCommand(GameCacheContext cacheContext) : base(
            CommandFlags.None,

            "SaveTagNames",

            "",

            "SaveTagNames",

            "")
        {
            CacheContext = cacheContext;
        }

        public override bool Execute(List<string> args)
        {
            FileInfo csvFile;

            if (args.Count == 1)
            {
                 csvFile = new FileInfo(args[0]);
            }
            else
            {
                csvFile = new FileInfo(Path.Combine(CacheContext.Directory.FullName, "tag_list.csv"));
            }

            var sortedNames = CacheContext.TagNames.ToList();
            sortedNames.Sort((a, b) => a.Key.CompareTo(b.Key));

            if (csvFile.Exists)
                csvFile.Delete();

            using (var csvStream = csvFile.Create())
            {
                var writer = new StreamWriter(csvStream);

                foreach (var entry in sortedNames)
                {
                    var value = entry.Value;

                    writer.WriteLine($"0x{entry.Key:X8},{value}");
                }

                writer.Close();
            }

            Console.WriteLine("Done");

            return true;
        }
    }
}
