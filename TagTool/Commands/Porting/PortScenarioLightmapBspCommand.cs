using System;
using System.Collections.Generic;
using System.IO;
using BlamCore.Cache;
using BlamCore.Legacy.Base;
using BlamCore.Common;
using BlamCore.Geometry;
using BlamCore.TagDefinitions;
using BlamCore.TagResources;
using BlamCore.IO;
using BlamCore.Serialization;

namespace TagTool.Commands.Porting
{
    class PortScenarioLightmapBspCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private CacheFile BlamCache { get; }

        public PortScenarioLightmapBspCommand(GameCacheContext cacheContext, CacheFile blamCache)
            : base(CommandFlags.Inherit,

                  "PortScenarioLightmapBspData",
                  "Ports a Blam scenario_lightmap_bsp_data tag to the current cache.",

                  "PortScenarioLightmapBspData [New <Blam Tag>] | [<Blam Tag> <ElDorado Tag>]",

                  "Ports a Blam scenario_lightmap_bsp_data tag to the current cache.")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        private void ConvertVertices<T>(int count, Func<T> readVertex, Action<T> writeVertex)
        {
            for (var i = 0; i < count; i++)
                writeVertex(readVertex());
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count < 2 || args.Count > 3)
                return false;

            bool isNew = false;
            if (args[0].ToLower() == "new")
            {
                isNew = true;
                args.RemoveAt(0);
            }

            var initialStringIDCount = CacheContext.StringIdCache.Strings.Count;

            //
            // Verify the Blam scenario_lightmap_bsp_data tag
            //

            var blamTagName = args[0];

            CacheFile.IndexItem blamTag = null;

            Console.WriteLine("Verifying Blam tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "Lbsp" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam tag does not exist: " + args[0]);
                return false;
            }

            //
            // Verify the ED scenario_lightmap_bsp_data tag
            //

            CachedTagInstance edTag = null;

            if (!isNew)
            {
                Console.WriteLine("Verifying ED tag index...");

                edTag = ArgumentParser.ParseTagSpecifier(CacheContext, args[1]);

                if (edTag.Group.Name != CacheContext.GetStringId("scenario_lightmap_bsp_data"))
                {
                    Console.WriteLine("Specified tag index is not a scenario_lightmap_bsp_data: " + args[1]);
                    return false;
                }
            }

            //
            // Finalize the new ElDorado scenario_lightmap_bsp_data tag
            //

            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating the new ElDorado scenario_lightmap_bsp_data tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("Lbsp")]);

                Console.WriteLine("done.");
            }
            else
            {
                newTag = edTag;
            }

            CacheContext.TagNames[newTag.Index] = blamTagName;

            //
            // Load Blam resource fixups
            //

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamLbsp = blamDeserializer.Deserialize<ScenarioLightmapBspData>(blamContext);

            var fixupEntry = BlamCache.ResourceGestalt.DefinitionEntries[blamLbsp.Geometry.ZoneAssetIndex & ushort.MaxValue];

            var resourceDefinition = new RenderGeometryApiResourceDefinition
            {
                VertexBuffers = new List<D3DPointer<VertexBufferDefinition>>(),
                IndexBuffers = new List<D3DPointer<IndexBufferDefinition>>()
            };

            using (var fixupStream = new MemoryStream(BlamCache.ResourceGestalt.FixupData))
            using (var reader = new EndianReader(fixupStream, EndianFormat.BigEndian))
            {
                var dataContext = new DataSerializationContext(reader, null);

                reader.SeekTo(fixupEntry.Offset + (fixupEntry.Size - 24)); // crashes on levels\atlas\sc100\h100_shared_per_vertex_all

                var vertexBufferCount = reader.ReadInt32();
                reader.Skip(8);
                var indexBufferCount = reader.ReadInt32();

                reader.SeekTo(fixupEntry.Offset);

                for (var i = 0; i < vertexBufferCount; i++)
                {
                    resourceDefinition.VertexBuffers.Add(new D3DPointer<VertexBufferDefinition>
                    {
                        Definition = new VertexBufferDefinition
                        {
                            Count = reader.ReadInt32(),
                            Format = (VertexBufferFormat)reader.ReadInt16(),
                            VertexSize = reader.ReadInt16(),
                            Data = new ResourceDataReference
                            {
                                Size = reader.ReadInt32(),
                                Unused4 = reader.ReadInt32(),
                                Unused8 = reader.ReadInt32(),
                                Address = new ResourceAddress(ResourceAddressType.Memory, reader.ReadInt32()),
                                Unused10 = reader.ReadInt32()
                            }
                        }
                    });
                }



                reader.Skip(vertexBufferCount * 12);

                for (var i = 0; i < indexBufferCount; i++)
                {
                    resourceDefinition.IndexBuffers.Add(new D3DPointer<IndexBufferDefinition>
                    {
                        Definition = new IndexBufferDefinition
                        {
                            Type = (PrimitiveType)reader.ReadInt32(),
                            Data = new ResourceDataReference
                            {
                                Size = reader.ReadInt32(),
                                Unused4 = reader.ReadInt32(),
                                Unused8 = reader.ReadInt32(),
                                Address = new ResourceAddress(ResourceAddressType.Memory, reader.ReadInt32()),
                                Unused10 = reader.ReadInt32()
                            }
                        }
                    });
                }
            }

            //
            // Convert Blam data to ElDorado data
            //

            using (var edResourceStream = new MemoryStream())
            {
                using (var blamResourceStream = new MemoryStream(BlamCache.GetRawFromID(blamLbsp.Geometry.ZoneAssetIndex)))
                {
                    //
                    // Convert Blam render_geometry_api_resource_definition
                    //

                    var inVertexStream = VertexStreamFactory.Create(BlamCache.Version, blamResourceStream);
                    var outVertexStream = VertexStreamFactory.Create(CacheContext.Version, edResourceStream);

                    for (var i = 0; i < resourceDefinition.VertexBuffers.Count; i++)
                    {
                        var vertexBuffer = resourceDefinition.VertexBuffers[i].Definition;

                        var count = vertexBuffer.Count;

                        var startPos = (int)edResourceStream.Position;
                        vertexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                        blamResourceStream.Position = fixupEntry.Fixups[i].Offset;

                        switch (vertexBuffer.Format)
                        {
                            case VertexBufferFormat.World:
                                ConvertVertices(count, inVertexStream.ReadWorldVertex, outVertexStream.WriteWorldVertex);
                                break;

                            case VertexBufferFormat.Rigid:
                                ConvertVertices(count, inVertexStream.ReadRigidVertex, outVertexStream.WriteRigidVertex);
                                break;

                            case VertexBufferFormat.Skinned:
                                ConvertVertices(count, inVertexStream.ReadSkinnedVertex, outVertexStream.WriteSkinnedVertex);
                                break;

                            case VertexBufferFormat.StaticPerPixel:
                                ConvertVertices(count, inVertexStream.ReadStaticPerPixelData, outVertexStream.WriteStaticPerPixelData);
                                break;

                            case VertexBufferFormat.StaticPerVertex: // C100
                                ConvertVertices(count, inVertexStream.ReadStaticPerVertexData, v =>
                                {
                                    v.Texcoord = v.Texcoord - 0x80808081;
                                    v.Texcoord2 = v.Texcoord2 - 0x80808081;
                                    v.Texcoord3 = v.Texcoord3 - 0x80808081;
                                    v.Texcoord4 = v.Texcoord4 - 0x80808081;
                                    v.Texcoord5 = v.Texcoord5 - 0x80808081;

                                    outVertexStream.WriteStaticPerVertexData(v);
                                });
                                break;

                            case VertexBufferFormat.AmbientPrt:
                                ConvertVertices(count, inVertexStream.ReadAmbientPrtData, outVertexStream.WriteAmbientPrtData);
                                break;

                            case VertexBufferFormat.LinearPrt:
                                ConvertVertices(count, inVertexStream.ReadLinearPrtData, outVertexStream.WriteLinearPrtData);
                                break;

                            case VertexBufferFormat.QuadraticPrt:
                                ConvertVertices(count, inVertexStream.ReadQuadraticPrtData, outVertexStream.WriteQuadraticPrtData);
                                break;

                            case VertexBufferFormat.StaticPerVertexColor:
                                ConvertVertices(count, inVertexStream.ReadStaticPerVertexColorData, outVertexStream.WriteStaticPerVertexColorData);
                                break;

                            case VertexBufferFormat.Decorator:
                                ConvertVertices(count, inVertexStream.ReadDecoratorVertex, outVertexStream.WriteDecoratorVertex);
                                break;

                            case VertexBufferFormat.World2:
                                vertexBuffer.Format = VertexBufferFormat.World;
                                goto case VertexBufferFormat.World;

                            default:
                                // throw new NotSupportedException(vertexBuffer.Format.ToString());
                                Console.Write("{0} ", vertexBuffer.Format.ToString());
                                break;
                        }

                        vertexBuffer.Data.Size = (int)edResourceStream.Position - startPos;
                        vertexBuffer.VertexSize = (short)(vertexBuffer.Data.Size / vertexBuffer.Count);
                    }

                    for (var i = 0; i < resourceDefinition.IndexBuffers.Count; i++)
                    {
                        var indexBuffer = resourceDefinition.IndexBuffers[i].Definition;

                        var indexCount = indexBuffer.Data.Size / 2;

                        var inIndexStream = new IndexBufferStream(
                            blamResourceStream,
                            IndexBufferFormat.UInt16,
                            EndianFormat.BigEndian);

                        var outIndexStream = new IndexBufferStream(
                            edResourceStream,
                            IndexBufferFormat.UInt16,
                            EndianFormat.LittleEndian);

                        var startPos = (int)edResourceStream.Position;
                        indexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                        blamResourceStream.Position = fixupEntry.Fixups[resourceDefinition.VertexBuffers.Count * 2 + i].Offset;

                        for (var j = 0; j < indexCount; j++)
                            outIndexStream.WriteIndex(inIndexStream.ReadIndex());
                    }
                }

                blamLbsp.Geometry.Resource = new ResourceReference
                {
                    Type = 5, // FIXME: hax
                    DefinitionFixups = new List<ResourceDefinitionFixup>(),
                    D3DObjectFixups = new List<D3DObjectFixup>(),
                    Unknown68 = 1,
                    Owner = newTag
                };

                Console.WriteLine("Writing resource data...");

                edResourceStream.Position = 0;

                var resourceContext = new ResourceSerializationContext(blamLbsp.Geometry.Resource);
                CacheContext.Serializer.Serialize(resourceContext, resourceDefinition);
                CacheContext.AddResource(blamLbsp.Geometry.Resource, ResourceLocation.Resources, edResourceStream);

                //
                // Convert UnknownSection.Unknown byte[] endian
                //

                for (int i = 0; i < blamLbsp.Geometry.UnknownSections.Count; i++) // different between H3 and ODST or HO
                {
                    var dataref = blamLbsp.Geometry.UnknownSections[i].Unknown; // different between H3 and ODST or HO

                    LbspUnknownSection Section = new LbspUnknownSection();

                    if (dataref.Length != 0)
                    {
                        using (var outStream = new MemoryStream())
                        using (var outReader = new BinaryReader(outStream))
                        using (var outWriter = new EndianWriter(outStream, EndianFormat.LittleEndian))
                        using (var stream = new MemoryStream(dataref))
                        using (var reader = new EndianReader(stream, EndianFormat.BigEndian))
                        {
                            var dataContext = new DataSerializationContext(reader, null); // at block 2, reader buffer is 0x0's instead of data

                            Section.Headers = new List<LbspUnknownSection.LbspUnknownSectionHeader>();

                            Section.Headers.Add(CacheContext.Deserializer.Deserialize<LbspUnknownSection.LbspUnknownSectionHeader>(dataContext));

                            Section.VertexLists = new LbspUnknownSection.VertexList { Vertex = new List<byte>() };

                            var a = Section.Headers[0];
                            int vertex1size = a.BytesCount;

                            outWriter.Write(a.Unknown00);
                            outWriter.Write(a.Unknown01);
                            outWriter.Write(a.Unknown02);
                            outWriter.Write(a.Unknown03);
                            outWriter.Write(a.Unknown04);
                            outWriter.Write(a.Unknown05);
                            outWriter.Write(a.Unknown06);
                            outWriter.Write(a.Unknown07);
                            outWriter.Write(a.Unknown08);
                            outWriter.Write(a.Unknown09);
                            outWriter.Write(a.Unknown10);
                            outWriter.Write(a.Unknown11);
                            outWriter.Write(a.X);
                            outWriter.Write(a.Y);
                            outWriter.Write(a.Z);
                            outWriter.Write(a.Unknown15);
                            outWriter.Write(a.Unknown16);
                            outWriter.Write(a.BytesCount);
                            outWriter.Write(a.BytesCount2);

                            while (reader.BaseStream.Position < dataref.Length) // read the rest of dataref
                            {
                                try
                                {
                                    if (Section.Headers.Count == 2) // remove "wrongfully" added ones
                                        Section.Headers.RemoveAt(1);

                                    Section.Headers.Add(CacheContext.Deserializer.Deserialize<LbspUnknownSection.LbspUnknownSectionHeader>(dataContext));

                                    if (Section.Headers[0].X == Section.Headers[1].X &
                                        Section.Headers[0].Y == Section.Headers[1].Y &
                                        Section.Headers[0].Z == Section.Headers[1].Z) // if some values match header1, continue
                                    {
                                        a = Section.Headers[1];

                                        outWriter.Write(a.Unknown00);
                                        outWriter.Write(a.Unknown01);
                                        outWriter.Write(a.Unknown02);
                                        outWriter.Write(a.Unknown03);
                                        outWriter.Write(a.Unknown04);
                                        outWriter.Write(a.Unknown05);
                                        outWriter.Write(a.Unknown06);
                                        outWriter.Write(a.Unknown07);
                                        outWriter.Write(a.Unknown08);
                                        outWriter.Write(a.Unknown09);
                                        outWriter.Write(a.Unknown10);
                                        outWriter.Write(a.Unknown11);
                                        outWriter.Write(a.X);
                                        outWriter.Write(a.Y);
                                        outWriter.Write(a.Z);
                                        outWriter.Write(a.Unknown15);
                                        outWriter.Write(a.Unknown16);
                                        outWriter.Write(a.BytesCount);
                                        outWriter.Write(a.BytesCount2);

                                        while (reader.BaseStream.Position < dataref.Length)
                                        {
                                            Section.VertexLists.Vertex.Add(reader.ReadByte());
                                            outWriter.Write(Section.VertexLists.Vertex[Section.VertexLists.Vertex.Count - 1]);
                                        }
                                    }
                                    else // if read data doesn't match, go back and just read 4 bytes
                                    {
                                        reader.BaseStream.Position = reader.BaseStream.Position - 0x2C; // if read data doesn't match, go back and serialize 

                                        Section.VertexLists.Vertex.Add(reader.ReadByte());
                                        outWriter.Write(Section.VertexLists.Vertex[Section.VertexLists.Vertex.Count - 1]);

                                        Section.VertexLists.Vertex.Add(reader.ReadByte());
                                        outWriter.Write(Section.VertexLists.Vertex[Section.VertexLists.Vertex.Count - 1]);

                                        Section.VertexLists.Vertex.Add(reader.ReadByte());
                                        outWriter.Write(Section.VertexLists.Vertex[Section.VertexLists.Vertex.Count - 1]);

                                        Section.VertexLists.Vertex.Add(reader.ReadByte());
                                        outWriter.Write(Section.VertexLists.Vertex[Section.VertexLists.Vertex.Count - 1]);
                                    }
                                }
                                catch (Exception) { }
                            }
                            // Write back to tag
                            outStream.Position = 0;

                            blamLbsp.Geometry.UnknownSections[i].Unknown = outReader.ReadBytes((int)outStream.Length);
                        }
                    }
                    else // some tagblocks are empty
                        continue;
                }

                //
                // Fix Unknown byte[]
                //

                var tagblocks = blamLbsp.Geometry.Unknown2;
                for (int i = 0; i < tagblocks.Count; i++) // different between H3 and ODST or HO
                {
                    var dataref = tagblocks[i].Unknown3; // different between H3 and ODST or HO

                    if (dataref.Length != 0)
                    {
                        using (var outStream = new MemoryStream())
                        using (var outReader = new BinaryReader(outStream))
                        using (var outWriter = new EndianWriter(outStream, EndianFormat.LittleEndian))
                        using (var stream = new MemoryStream(dataref))
                        using (var reader = new EndianReader(stream, EndianFormat.BigEndian))
                        {
                            var dataContext = new DataSerializationContext(reader, null); // at block 2, reader buffer is 0x0's instead of data

                            var ints = new List<UInt32>();

                            while (reader.BaseStream.Position < dataref.Length) // read the rest of dataref
                            {
                                try
                                {
                                    outWriter.Write(reader.ReadUInt32());
                                }
                                catch (Exception) { }
                            }
                            // Write back to tag
                            outStream.Position = 0;

                            blamLbsp.Geometry.Unknown2[i].Unknown3 = outReader.ReadBytes((int)outStream.Length);
                        }
                    }
                    else // some tagblocks are empty
                        continue;
                }

                //
                // Set bitm
                //

                blamLbsp.PrimaryMap = CacheContext.GetTag(0x2E88);
                blamLbsp.IntensityMap = CacheContext.GetTag(0x2E89);

                // blamMode.PrimaryMap = CacheContext.GetTag(0x0343);
                // blamMode.IntensityMap = CacheContext.GetTag(0x0343);

                for (int i = 0; i < blamLbsp.Geometry.Meshes.Count; i++)
                {
                    blamLbsp.Geometry.Meshes[i].PrtType = PrtType.None;
                    // blamMode.Geometry.Meshes[i].VertexBuffers[0] = 65535;
                    // blamMode.Geometry.Meshes[i].VertexBuffers[1] = 65535;
                    // blamMode.Geometry.Meshes[i].VertexBuffers[2] = 65535;
                    // blamMode.Geometry.Meshes[i].VertexBuffers[3] = 65535;
                    // blamMode.Geometry.Meshes[i].VertexBuffers[4] = 65535;
                    // blamMode.Geometry.Meshes[i].VertexBuffers[5] = 65535;
                    // blamMode.Geometry.Meshes[i].VertexBuffers[6] = 65535;
                    // blamMode.Geometry.Meshes[i].VertexBuffers[7] = 65535;
                }

                // Cheap temporary lightning fix alongside the test lightmap bitmap
                blamLbsp.Shadows = 4;
                blamLbsp.Midtones = 2;
                blamLbsp.Highlights = 2;
                blamLbsp.TopDownWhites = 2;
                blamLbsp.TopDownBlacks = 2;

                using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
                {
                    Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.scenario_lightmap_bsp_data\" tag data...");

                    var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                    CacheContext.Serializer.Serialize(context, blamLbsp);
                }

                //
                // Done!
                //

                Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.scenario_lightmap_bsp_data\" successfully!");

            }

            return true;
        }
    }
}