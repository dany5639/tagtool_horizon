﻿using BlamCore.Bitmaps;
using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Legacy.Base;
using BlamCore.TagDefinitions;
using System;
using System.Collections.Generic;
using System.IO;

namespace TagTool.Commands.Porting
{
    class PortShaderCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private CacheFile BlamCache { get; }

        public PortShaderCommand(GameCacheContext cacheContext, CacheFile blamCache)
            : base(CommandFlags.None,

                  "PortShader",
                  "Ports a Blam shader tag to the current cache. WIP",

                  "PortShader <Blam Tag>",

                  "Ports a Blam shader tag to the current cache. WIP")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count != 1)
                return false;

            //
            // Verify and load the Blam shader
            //

            var shaderName = args[0];

            CacheFile.IndexItem item = null;

            Console.WriteLine("Verifying Blam shader tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if ((tag.ParentClass == "rm") && tag.Filename == shaderName)
                {
                    item = tag;
                    break;
                }
            }

            if (item == null)
            {
                Console.WriteLine("Blam shader tag does not exist: " + shaderName);
                return false;
            }

            var BlamShader = new BlamCore.Legacy.Halo3Beta.shader(BlamCache, item.Offset);

            var templateItem = BlamCache.IndexItems.Find(i =>
                i.ID == BlamShader.Properties[0].TemplateTagID);

            var template = new BlamCore.Legacy.Halo3Beta.render_method_template(BlamCache, templateItem.Offset);

            //
            // Determine the Blam shader's base bitmap
            //

            var bitmapIndex = -1;
            var bitmapArgName = "";

            for (var i = 0; i < template.UsageBlocks.Count; i++)
            {
                var entry = template.UsageBlocks[i];

                if (entry.Usage.StartsWith("base_map") ||
                    entry.Usage.StartsWith("diffuse_map") ||
                    entry.Usage == "foam_texture")
                {
                    bitmapIndex = i;
                    bitmapArgName = entry.Usage;
                    break;
                }
            }

            //
            // Load and decode the Blam shader's base bitmap
            //

            try
            {
                var bitmItem = BlamCache.IndexItems.Find(i =>
                i.ID == BlamShader.Properties[0].ShaderMaps[bitmapIndex].BitmapTagID);

                var bitm = new BlamCore.Legacy.Halo3Beta.bitmap(BlamCache, bitmItem.Offset);

                var submap = bitm.Bitmaps[0];

                byte[] raw;

                if (bitm.RawChunkBs.Count > 0)
                {
                    int rawID = bitm.RawChunkBs[submap.InterleavedIndex].RawID;
                    byte[] buffer = BlamCache.GetRawFromID(rawID);
                    raw = new byte[submap.RawSize];
                    Array.Copy(buffer, submap.Index2 * submap.RawSize, raw, 0, submap.RawSize);
                }
                else
                {
                    int rawID = bitm.RawChunkAs[0].RawID;
                    raw = BlamCache.GetRawFromID(rawID, submap.RawSize);
                }

                var vHeight = submap.VirtualHeight;
                var vWidth = submap.VirtualWidth;

                var ms = new MemoryStream();
                var bw = new BinaryWriter(ms);

                if (submap.Flags.Values[3])
                    raw = DxtDecoder.ConvertToLinearTexture(raw, vWidth, vHeight, submap.Format);

                if (submap.Format != BitmapFormat.A8R8G8B8)
                    for (int i = 0; i < raw.Length; i += 2)
                        Array.Reverse(raw, i, 2);
                else
                    for (int i = 0; i < (raw.Length); i += 4)
                        Array.Reverse(raw, i, 4);

                new DdsImage(submap).Write(bw);
                bw.Write(raw);

                raw = ms.ToArray();

                bw.Close();
                bw.Dispose();

                //
                // ElDorado Serialization. Create the new ElDorado shader
                //

                using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
                {
                    var bitmap = new Bitmap
                    {
                        Flags = Bitmap.RuntimeFlags.UseResource,
                        Sequences = new List<Bitmap.Sequence>
                    {
                        new Bitmap.Sequence
                        {
                            Name = "",
                            FirstBitmapIndex = 0,
                            BitmapCount = 1
                        }
                    },
                        Images = new List<Bitmap.Image>
                    {
                        new Bitmap.Image
                        {
                            Signature = new Tag("bitm").Value,
                            Unknown28 = -1
                        }
                    },
                        Resources = new List<Bitmap.BitmapResource>
                    {
                        new Bitmap.BitmapResource()
                    }
                    };

                    using (var imageStream = new MemoryStream(raw))
                    {
                        var injector = new BitmapDdsInjector(CacheContext);
                        imageStream.Seek(0, SeekOrigin.Begin);
                        injector.InjectDds(CacheContext.Serializer, CacheContext.Deserializer, bitmap, 0, imageStream);
                    }

                    var newBitm = CacheContext.TagCache.DuplicateTag(cacheStream, CacheContext.GetTag(0x101F));

                    string bitmName = shaderName;

                    CacheContext.TagNames[newBitm.Index] = bitmItem.Filename;

                    var context = new TagSerializationContext(cacheStream, CacheContext, newBitm);
                    CacheContext.Serializer.Serialize(context, bitmap);

                    //
                    // Create shader tag and serialize
                    // 

                    var newRmsh = CacheContext.TagCache.DuplicateTag(cacheStream, CacheContext.GetTag(0x101F));

                    context = new TagSerializationContext(cacheStream, CacheContext, newRmsh);

                    var shader = CacheContext.Deserializer.Deserialize<Shader>(context);

                    shader.ShaderProperties[0].ShaderMaps[0].Bitmap = newBitm;

                    CacheContext.Serializer.Serialize(context, shader);

                    CacheContext.TagNames[newRmsh.Index] = shaderName;
                    
                    Console.WriteLine($"Ported \"{CacheContext.TagNames[newRmsh.Index]}.shader\" successfully!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
                {
                    var newRmsh = CacheContext.TagCache.DuplicateTag(cacheStream, CacheContext.GetTag(0x101F));

                    CacheContext.TagNames[newRmsh.Index] = shaderName;

                    Console.WriteLine($"Ported \"{CacheContext.TagNames[newRmsh.Index]}.shader\" successfully!");
                }
            }

            return true;
        }
    }
}
