﻿using BlamCore.Cache;
using System;
using System.Collections.Generic;
using System.IO;
using BlamCore.Legacy.Base;
using BlamCore.IO;

namespace TagTool.Commands.Porting
{
    class OpenCacheFileCommand : Command
    {
        private CommandContextStack ContextStack { get; }
        private GameCacheContext CacheContext { get; }

        public OpenCacheFileCommand(CommandContextStack contextStack, GameCacheContext cacheContext)
            : base(CommandFlags.None,

                  "OpenCacheFile",
                  "Opens a porting context on a cache file from H3B/H3/ODST.",

                  "OpenCacheFile <Cache File>",

                  "Opens a porting context on a cache file from H3B/H3/ODST.")
        {
            ContextStack = contextStack;
            CacheContext = cacheContext;
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count != 1)
                return false;

            var blamCacheFile = new FileInfo(args[0]);

            if (!blamCacheFile.Exists)
                throw new FileNotFoundException(blamCacheFile.FullName);

            Console.Write("Loading blam cache file...");

            CacheFile blamCache = null;

            using (var fs = new FileStream(blamCacheFile.FullName, FileMode.Open, FileAccess.Read))
            {
                var reader = new EndianReader(fs, EndianFormat.BigEndian);

                var head = reader.ReadInt32();

                if (head == 1684104552)
                    reader.Format = EndianFormat.LittleEndian;

                var v = reader.ReadInt32();

                reader.SeekTo(284);
                var version = CacheVersionDetection.GetFromBuildName(reader.ReadString(32));
                
                switch (version)
                {
                    case CacheVersion.Halo3Beta:
                        blamCache = new BlamCore.Legacy.Halo3Beta.CacheFile(blamCacheFile, version);
                        break;

                    case CacheVersion.Halo3Retail:
                        blamCache = new BlamCore.Legacy.Halo3Retail.CacheFile(blamCacheFile, version);
                        break;

                    case CacheVersion.Halo3ODST:
                        blamCache = new BlamCore.Legacy.Halo3ODST.CacheFile(blamCacheFile, version);
                        break;

                    default:
                        throw new NotSupportedException(CacheVersionDetection.GetBuildName(version));
                }
            }

            Console.Write("loading resource layout table...");

            blamCache.LoadResourceTags();

            Console.WriteLine("done.");

            ContextStack.Push(PortingContextFactory.Create(ContextStack, CacheContext, blamCache));
            
            return true;
        }
    }
}