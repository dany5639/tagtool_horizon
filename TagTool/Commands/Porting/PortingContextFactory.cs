﻿using BlamCore.Legacy.Base;
using BlamCore.Cache;

namespace TagTool.Commands.Porting
{
    static class PortingContextFactory
    {
        public static CommandContext Create(CommandContextStack contextStack, GameCacheContext cacheContext, CacheFile blamCache)
        {
            var context = new CommandContext(contextStack.Context, blamCache.Header.scenarioName);

            Populate(context, cacheContext, blamCache);

            return context;
        }

        public static void Populate(CommandContext context, GameCacheContext cacheContext, CacheFile blamCache)
        {
            context.AddCommand(new ListBitmapsCommand(cacheContext, blamCache));
            context.AddCommand(new PortShaderCommand(cacheContext, blamCache));
            context.AddCommand(new PortRenderModelCommand(cacheContext, blamCache));
            context.AddCommand(new PortCollisionModelCommand(cacheContext, blamCache));
            context.AddCommand(new PortPhysicsModelCommand(cacheContext, blamCache));
            context.AddCommand(new PortModelAnimationGraphCommand(cacheContext, blamCache));
            context.AddCommand(new PortModelCommand(cacheContext, blamCache));
            context.AddCommand(new PortScenarioLightmap(cacheContext, blamCache));
            context.AddCommand(new PortScenarioStructureBspCommand(cacheContext, blamCache));
            context.AddCommand(new PortScenarioCommand(cacheContext, blamCache));
            context.AddCommand(new PortScenarioLightmapBspCommand(cacheContext, blamCache));
            context.AddCommand(new ListBlamTagsCommand(cacheContext, blamCache));
            context.AddCommand(new ListBlamShadersCommand(cacheContext, blamCache));
            context.AddCommand(new TestBlamCommand(cacheContext, blamCache));
        }
    }
}
