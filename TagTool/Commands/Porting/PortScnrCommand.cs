﻿using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Legacy.Base;
using BlamCore.TagDefinitions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BlamCore.IO;
using BlamCore.Geometry;
using BlamCore.Serialization;
using BlamCore.TagResources;
using BlamCore.Bitmaps;

namespace TagTool.Commands.Porting
{
    class PortScnrCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private CacheFile BlamCache { get; }

        public PortScnrCommand(GameCacheContext cacheContext, CacheFile blamCache) :
            base(CommandFlags.None,

                "PortScnr",
                "Ports a Blam Scenario tag with sbsp, sLdT and Lbsp. WIP",

                "PortScnr [Blam Scenario Tagname] [Blam Sbsp Tagname]",

                "Ports a Blam Scenario tag with sbsp, sLdT and Lbsp. WIP")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        public override bool Execute(List<string> args)
        {
            #region Load Tags
            if (args.Count < 2 || args.Count > 3)
                return false;

            bool isNew = false;
            if (args[0].ToLower() == "new")
            {
                isNew = true;
                args.RemoveAt(0);
            }

            var initialStringIDCount = CacheContext.StringIdCache.Strings.Count;

            var blamTagName = args[0];

            CacheFile.IndexItem blamTag = null;

            Console.WriteLine("Verifying Blam scenario tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "scnr" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam scenario tag does not exist: " + args[0]);
                return false;
            }

            CachedTagInstance edTag = null;

            if (!isNew)
            {
                Console.WriteLine("Verifying ElDorado scenario tag index...");

                edTag = ArgumentParser.ParseTagSpecifier(CacheContext, args[1]);

                if (edTag.Group.Name != CacheContext.GetStringId("scenario"))
                {
                    Console.WriteLine("Specified tag index is not a scenario: " + args[1]);
                    return false;
                }
            }

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var scnr = blamDeserializer.Deserialize<Scenario>(blamContext);
            Console.WriteLine("Scenario blam tag deserialized.");

            #endregion

            #region Setup tags
            for (int j = 0; j < scnr.StructureBsps.Count; j++)
                scnr.StructureBsps[j].StructureBsp = PortScenarioStructureBsp(j, BlamCache.IndexItems.Find(i => i.ID == scnr.StructureBsps[j].StructureBsp.Index).Filename);

            for (int i = 0; i < scnr.SkyReferences.Count; i++)
                scnr.SkyReferences[i].SkyObject = CacheContext.GetTag(0x27C6);

            scnr.Lightmap = PortScenarioLightmap(BlamCache.IndexItems.Find(i => i.ID == scnr.Lightmap.Index).Filename);

            scnr.SkyParameters = CacheContext.GetTag(0x27F7);

            scnr.GlobalLighing = CacheContext.GetTag(0x27F8);

            scnr.MapType = MapTypeValue.MainMenu;
            scnr.MapSubType = 0;
            scnr.Flags = ScenarioFlags.None;
            scnr.SandboxBudget = 13;
            scnr.CampaignId = 0;
            #endregion

            #region Add spawnpoint
            scnr.SceneryPalette = new List<Scenario.SceneryPaletteBlock>();
            scnr.Scenery = new List<Scenario.SceneryBlock>();

            var mp_spawnpoint_default = CacheContext.GetTag(0x2E90); // default spawnpoint; neutral

            scnr.SceneryPalette.Add(new Scenario.SceneryPaletteBlock { Scenery = mp_spawnpoint_default });

            /*
            foreach (var a in scnr.Equipment)
            {
                float X = a.PositionX;
                float Y = a.PositionY;
                float Z = a.PositionZ;

                var scenerySpawnpointDefault = new Scenario.SceneryBlock
                {
                    PaletteIndex = (short)scnr.SceneryPalette.IndexOf(scnr.SceneryPalette.Find(x => x.Scenery == mp_spawnpoint_default)),
                    NameIndex = 0,
                    PlacementFlags = Scenario.ObjectPlacementFlags.None,
                    Position = new RealPoint3d(X, Y, Z),
                    Rotation = new RealEulerAngles3d(new Angle(), new Angle(), new Angle()),
                    UniqueIdIndex = 49096,
                    UniqueIdSalt = 8,
                    ObjectTypeOld = GameObjectTypeHalo3ODST.Scenery,
                    ObjectTypeNew = GameObjectTypeHaloOnline.Scenery,
                    Source = Scenario.SceneryBlock.SourceValue.Editor,
                    Unknown3 = -1,
                    ParentNameIndex = -1,
                    AllowedZoneSets = 1,
                    Unknown12 = -1,
                    Team = Scenario.SceneryBlock.TeamValue.Neutral,
                    Unknown18 = 0
                };

                scnr.Scenery.Add(scenerySpawnpointDefault);
            }

            foreach (var a in scnr.Weapons)
            {
                float X = a.PositionX;
                float Y = a.PositionY;
                float Z = a.PositionZ;

                var scenerySpawnpointDefault = new Scenario.SceneryBlock
                {
                    PaletteIndex = (short)scnr.SceneryPalette.IndexOf(scnr.SceneryPalette.Find(x => x.Scenery == mp_spawnpoint_default)),
                    NameIndex = 0,
                    PlacementFlags = Scenario.ObjectPlacementFlags.None,
                    Position = new RealPoint3d(X, Y, Z),
                    Rotation = new RealEulerAngles3d(new Angle(), new Angle(), new Angle()),
                    UniqueIdIndex = 49096,
                    UniqueIdSalt = 8,
                    ObjectTypeOld = GameObjectTypeHalo3ODST.Scenery,
                    ObjectTypeNew = GameObjectTypeHaloOnline.Scenery,
                    Source = Scenario.SceneryBlock.SourceValue.Editor,
                    Unknown3 = -1,
                    ParentNameIndex = -1,
                    AllowedZoneSets = 1,
                    Unknown12 = -1,
                    Team = Scenario.SceneryBlock.TeamValue.Neutral,
                    Unknown18 = 0
                };

                scnr.Scenery.Add(scenerySpawnpointDefault);
            }

            foreach (var a in scnr.PlayerStartingLocations)
            {
                float X = a.Position.X;
                float Y = a.Position.Y;
                float Z = a.Position.Z;

                var scenerySpawnpointDefault = new Scenario.SceneryBlock
                {
                    PaletteIndex = (short)scnr.SceneryPalette.IndexOf(scnr.SceneryPalette.Find(x => x.Scenery == mp_spawnpoint_default)),
                    NameIndex = 0,
                    PlacementFlags = Scenario.ObjectPlacementFlags.None,
                    Position = new RealPoint3d(X, Y, Z),
                    Rotation = new RealEulerAngles3d(new Angle(), new Angle(), new Angle()),
                    UniqueIdIndex = 49096,
                    UniqueIdSalt = 8,
                    ObjectTypeOld = GameObjectTypeHalo3ODST.Scenery,
                    ObjectTypeNew = GameObjectTypeHaloOnline.Scenery,
                    Source = Scenario.SceneryBlock.SourceValue.Editor,
                    Unknown3 = -1,
                    ParentNameIndex = -1,
                    AllowedZoneSets = 1,
                    Unknown12 = -1,
                    Team = Scenario.SceneryBlock.TeamValue.Neutral,
                    Unknown18 = 0
                };

                scnr.Scenery.Add(scenerySpawnpointDefault);
            }
            */

            float X = 0f;
            float Y = 0f;
            float Z = 0f;

            var scenerySpawnpointDefault = new Scenario.SceneryBlock
            {
                PaletteIndex = (short)scnr.SceneryPalette.IndexOf(scnr.SceneryPalette.Find(x => x.Scenery == mp_spawnpoint_default)),
                NameIndex = 0,
                PlacementFlags = Scenario.ObjectPlacementFlags.None,
                Position = new RealPoint3d(X, Y, Z),
                Rotation = new RealEulerAngles3d(new Angle(), new Angle(), new Angle()),
                UniqueIdIndex = 49096,
                UniqueIdSalt = 8,
                ObjectTypeOld = GameObjectTypeHalo3ODST.Scenery,
                ObjectTypeNew = GameObjectTypeHaloOnline.Scenery,
                Source = Scenario.SceneryBlock.SourceValue.Editor,
                Unknown3 = -1,
                ParentNameIndex = -1,
                AllowedZoneSets = 1,
                Unknown12 = -1,
                Team = Scenario.SceneryBlock.TeamValue.Neutral,
                Unknown18 = 0
            };

            scnr.Scenery.Add(scenerySpawnpointDefault);

            Console.WriteLine("Added Spawnpoints");
            #endregion

            #region Add Camera
            float cameraX = 13.95461f;
            float cameraY = 4.51311f;
            float cameraZ = 0.6763648f;
            float cameraI = 139.1801f;
            float cameraJ = 14.51703f;
            float cameraK = -12.21716f;
            scnr.CutsceneCameraPoints = new List<Scenario.CutsceneCameraPoint>();
            scnr.CutsceneCameraPoints.Add(new Scenario.CutsceneCameraPoint
            {
                Name = "main_menu_cam_point_0",
                Position = new RealPoint3d(cameraX, cameraY, cameraZ),
                Orientation = new RealEulerAngles3d(
                    Angle.FromDegrees(cameraI),
                    Angle.FromDegrees(cameraJ),
                    Angle.FromDegrees(cameraK)
                )
            });
            #endregion

            #region Fix Strings
            /*
            foreach (var zoneSet in scnr.ZoneSets)
            {
                var value = BlamCache.Strings.GetItemByID((int)zoneSet.Name.Value);
                if (value == "<blank>") value = "";
                zoneSet.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var bspAtlas in scnr.BspAtlas)
            {
                var value = BlamCache.Strings.GetItemByID((int)bspAtlas.Name.Value);
                if (value == "<blank>") value = "";
                bspAtlas.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var campaignPlayer in scnr.CampaignPlayers)
            {
                var value = BlamCache.Strings.GetItemByID((int)campaignPlayer.PlayerRepresentationName.Value);
                if (value == "<blank>") value = "";
                campaignPlayer.PlayerRepresentationName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var placement in scnr.Scenery)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Variant.Value);
                if (value == "<blank>") value = "";
                placement.Variant = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.SceneryPalette)
            {
                palette.Scenery = ConvertTagReference(palette.Scenery.Index);
            }

            foreach (var placement in scnr.Bipeds)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Variant.Value);
                if (value == "<blank>") value = "";
                placement.Variant = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.BipedPalette)
            {
                palette.Biped = ConvertTagReference(palette.Biped.Index);
            }

            foreach (var placement in scnr.Vehicles)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Variant.Value);
                if (value == "<blank>") value = "";
                placement.Variant = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.VehiclePalette)
            {
                palette.Vehicle = ConvertTagReference(palette.Vehicle.Index);
            }

            foreach (var placement in scnr.Equipment)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.EquipmentPalette)
            {
                palette.Equipment = ConvertTagReference(palette.Equipment.Index);
            }

            foreach (var placement in scnr.Weapons)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Variant.Value);
                if (value == "<blank>") value = "";
                placement.Variant = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.WeaponPalette)
            {
                palette.Weapon = ConvertTagReference(palette.Weapon.Index);
            }

            foreach (var placement in scnr.Machines)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Variant.Value);
                if (value == "<blank>") value = "";
                placement.Variant = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.MachinePalette)
            {
                palette.Machine = ConvertTagReference(palette.Machine.Index);
            }

            foreach (var placement in scnr.Terminals)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Variant.Value);
                if (value == "<blank>") value = "";
                placement.Variant = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.TerminalPalette)
            {
                palette.Terminal = ConvertTagReference(palette.Terminal.Index);
            }

            foreach (var placement in scnr.AlternateRealityDevices)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Variant.Value);
                if (value == "<blank>") value = "";
                placement.Variant = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.AlternateRealityDevicePalette)
            {
                palette.ArgDevice = ConvertTagReference(palette.ArgDevice.Index);
            }

            foreach (var placement in scnr.Controls)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Variant.Value);
                if (value == "<blank>") value = "";
                placement.Variant = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.ControlPalette)
            {
                palette.Control = ConvertTagReference(palette.Control.Index);
            }

            foreach (var placement in scnr.SoundScenery)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.SoundSceneryPalette)
            {
                palette.SoundScenery = ConvertTagReference(palette.SoundScenery.Index);
            }

            foreach (var placement in scnr.Giants)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Variant.Value);
                if (value == "<blank>") value = "";
                placement.Variant = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.GiantPalette)
            {
                palette.Giant = ConvertTagReference(palette.Giant.Index);
            }

            foreach (var placement in scnr.EffectScenery)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.EffectSceneryPalette)
            {
                palette.EffectScenery = ConvertTagReference(palette.EffectScenery.Index);
            }

            foreach (var placement in scnr.LightVolumes)
            {
                var value = BlamCache.Strings.GetItemByID((int)placement.UniqueName.Value);
                if (value == "<blank>") value = "";
                placement.UniqueName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.ChildName.Value);
                if (value == "<blank>") value = "";
                placement.ChildName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)placement.Unknown5.Value);
                if (value == "<blank>") value = "";
                placement.Unknown5 = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var palette in scnr.LightVolumePalette)
            {
                palette.LightVolume = ConvertTagReference(palette.LightVolume.Index);
            }

            foreach (var sandboxObject in scnr.SandboxVehicles)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                var value = BlamCache.Strings.GetItemByID((int)sandboxObject.Name.Value);
                if (value == "<blank>") value = "";
                sandboxObject.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var sandboxObject in scnr.SandboxWeapons)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                var value = BlamCache.Strings.GetItemByID((int)sandboxObject.Name.Value);
                if (value == "<blank>") value = "";
                sandboxObject.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var sandboxObject in scnr.SandboxEquipment)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                var value = BlamCache.Strings.GetItemByID((int)sandboxObject.Name.Value);
                if (value == "<blank>") value = "";
                sandboxObject.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var sandboxObject in scnr.SandboxScenery)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                var value = BlamCache.Strings.GetItemByID((int)sandboxObject.Name.Value);
                if (value == "<blank>") value = "";
                sandboxObject.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var sandboxObject in scnr.SandboxTeleporters)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                var value = BlamCache.Strings.GetItemByID((int)sandboxObject.Name.Value);
                if (value == "<blank>") value = "";
                sandboxObject.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var sandboxObject in scnr.SandboxGoalObjects)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                var value = BlamCache.Strings.GetItemByID((int)sandboxObject.Name.Value);
                if (value == "<blank>") value = "";
                sandboxObject.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var sandboxObject in scnr.SandboxSpawning)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                var value = BlamCache.Strings.GetItemByID((int)sandboxObject.Name.Value);
                if (value == "<blank>") value = "";
                sandboxObject.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var softCeiling in scnr.SoftCeilings)
            {
                var value = BlamCache.Strings.GetItemByID((int)softCeiling.Name.Value);
                if (value == "<blank>") value = "";
                softCeiling.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var profile in scnr.PlayerStartingProfile)
            {
                profile.PrimaryWeapon = ConvertTagReference(profile.PrimaryWeapon.Index);
                profile.SecondaryWeapon = ConvertTagReference(profile.SecondaryWeapon.Index);
            }

            foreach (var triggerVolume in scnr.TriggerVolumes)
            {
                var value = BlamCache.Strings.GetItemByID((int)triggerVolume.Name.Value);
                if (value == "<blank>") value = "";
                triggerVolume.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                value = BlamCache.Strings.GetItemByID((int)triggerVolume.NodeName.Value);
                if (value == "<blank>") value = "";
                triggerVolume.NodeName = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var squad in scnr.Squads)
            {
                var value = BlamCache.Strings.GetItemByID((int)squad.ModuleId.Value);
                if (value == "<blank>") value = "";
                squad.ModuleId = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);

                foreach (var formation in squad.SpawnFormations)
                {
                    value = BlamCache.Strings.GetItemByID((int)formation.Name.Value);
                    if (value == "<blank>") value = "";
                    formation.Name = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);

                    value = BlamCache.Strings.GetItemByID((int)formation.FormationType.Value);
                    if (value == "<blank>") value = "";
                    formation.FormationType = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);

                    value = BlamCache.Strings.GetItemByID((int)formation.InitialState.Value);
                    if (value == "<blank>") value = "";
                    formation.InitialState = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);

                    foreach (var point in formation.Points)
                    {
                        value = BlamCache.Strings.GetItemByID((int)point.ActivityName.Value);
                        if (value == "<blank>") value = "";
                        point.ActivityName = CacheContext.StringIdCache.Contains(value) ?
                            CacheContext.StringIdCache.GetStringId(value) :
                            CacheContext.StringIdCache.AddString(value);
                    }
                }

                foreach (var spawnPoint in squad.SpawnPoints)
                {
                    value = BlamCache.Strings.GetItemByID((int)spawnPoint.Name.Value);
                    if (value == "<blank>") value = "";
                    spawnPoint.Name = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);

                    value = BlamCache.Strings.GetItemByID((int)spawnPoint.ActorVariantName.Value);
                    if (value == "<blank>") value = "";
                    spawnPoint.ActorVariantName = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);

                    value = BlamCache.Strings.GetItemByID((int)spawnPoint.VehicleVariantName.Value);
                    if (value == "<blank>") value = "";
                    spawnPoint.VehicleVariantName = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);

                    value = BlamCache.Strings.GetItemByID((int)spawnPoint.ActivityName.Value);
                    if (value == "<blank>") value = "";
                    spawnPoint.ActivityName = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);

                    foreach (var point in spawnPoint.Points)
                    {
                        value = BlamCache.Strings.GetItemByID((int)point.ActivityName.Value);
                        if (value == "<blank>") value = "";
                        point.ActivityName = CacheContext.StringIdCache.Contains(value) ?
                            CacheContext.StringIdCache.GetStringId(value) :
                            CacheContext.StringIdCache.AddString(value);
                    }
                }

                foreach (var cell in squad.DesignerCells)
                {
                    value = BlamCache.Strings.GetItemByID((int)cell.Name.Value);
                    if (value == "<blank>") value = "";
                    cell.Name = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);

                    value = BlamCache.Strings.GetItemByID((int)cell.VehicleVariant.Value);
                    if (value == "<blank>") value = "";
                    cell.VehicleVariant = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);
                }

                foreach (var cell in squad.TemplatedCells)
                {
                    value = BlamCache.Strings.GetItemByID((int)cell.Name.Value);
                    if (value == "<blank>") value = "";
                    cell.Name = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);

                    value = BlamCache.Strings.GetItemByID((int)cell.VehicleVariant.Value);
                    if (value == "<blank>") value = "";
                    cell.VehicleVariant = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);
                }
            }

            foreach (var element in scnr.Unknown84)
            {
                var value = BlamCache.Strings.GetItemByID((int)element.Unknown.Value);
                if (value == "<blank>") value = "";
                element.Unknown = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            for (var i = 0; i < scnr.DecalPalette.Count; i++)
                scnr.DecalPalette[i] = ConvertTagReference(scnr.DecalPalette[i].Index);

            for (var i = 0; i < scnr.StylePalette.Count; i++)
                scnr.StylePalette[i] = ConvertTagReference(scnr.StylePalette[i].Index);

            foreach (var squad in scnr.Squads)
            {
                squad.SquadTemplate = ConvertTagReference(squad.SquadTemplate.Index);
            }

            for (var i = 0; i < scnr.CharacterPalette.Count; i++)
                scnr.CharacterPalette[i] = ConvertTagReference(scnr.CharacterPalette[i].Index);

            foreach (var datum in scnr.AiPathfindingData)
            {
                foreach (var element in datum.Unknown9)
                {
                    var value = BlamCache.Strings.GetItemByID((int)element.Unknown.Value);
                    if (value == "<blank>") value = "";
                    element.Unknown = CacheContext.StringIdCache.Contains(value) ?
                        CacheContext.StringIdCache.GetStringId(value) :
                        CacheContext.StringIdCache.AddString(value);
                }
            }

            for (var i = 0; i < scnr.ScriptReferences.Count; i++)
                scnr.ScriptReferences[i] = ConvertTagReference(scnr.ScriptReferences[i].Index);

            foreach (var flag in scnr.CutsceneFlags)
            {
                var value = BlamCache.Strings.GetItemByID((int)flag.Name.Value);
                if (value == "<blank>") value = "";
                flag.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }

            foreach (var title in scnr.CutsceneTitles)
            {
                var value = BlamCache.Strings.GetItemByID((int)title.Name.Value);
                if (value == "<blank>") value = "";
                title.Name = CacheContext.StringIdCache.Contains(value) ?
                    CacheContext.StringIdCache.GetStringId(value) :
                    CacheContext.StringIdCache.AddString(value);
            }
            */
            #endregion

            #region Nuke stuf
            scnr.ChapterTitleStrings = null;
            scnr.CustomObjectNameStrings = null;
            scnr.DefaultCameraFx = null;
            scnr.DefaultScreenFx = null;
            scnr.MissionVisionMode = null;
            scnr.MissionVisionModeEffect = null;
            scnr.MissionVisionModeTheaterEffect = null;
            scnr.ObjectiveStrings = null;
            scnr.PerformanceThrottles = null;
            scnr.ScenarioPda = null;
            scnr.SoundEffectsCollection = null;
            scnr.SubtitleStrings = null;
            scnr.TerritoryLocationNameStrings = null;
            scnr.Unknown133 = null;
            scnr.Unknown156 = null;
            scnr.Unknown = null;

            scnr.CharacterPalette = new List<CachedTagInstance>();
            scnr.Cinematics = new List<CachedTagInstance>();
            scnr.FlockPalette = new List<CachedTagInstance>();
            scnr.MissionDialogue = new List<CachedTagInstance>();
            scnr.Unknown155 = new List<CachedTagInstance>();

            scnr.LocalNorth = new Angle();
            scnr.AlternateRealityDevices = new List<Scenario.AlternateRealityDevice>();
            scnr.AlternateRealityDevicePalette = new List<Scenario.AlternateRealityDevicePaletteBlock>();
            scnr.Bipeds = new List<Scenario.Biped>();
            scnr.BipedPalette = new List<Scenario.BipedPaletteBlock>();
            scnr.BspAtlas = new List<Scenario.BspAtlasBlock>();
            scnr.CameraFx = new List<Scenario.CameraFxBlock>();
            scnr.CampaignPlayers = new List<Scenario.CampaignPlayer>();
            scnr.CinematicLighting = new List<Scenario.CinematicLightingBlock>();
            scnr.Controls = new List<Scenario.Control>();
            scnr.ControlPalette = new List<Scenario.ControlPaletteBlock>();
            scnr.Crates = new List<Scenario.Crate>();
            scnr.CratePalette = new List<Scenario.CratePaletteBlock>();
            scnr.CreaturePalette = new List<Scenario.CreaturePaletteBlock>();
            scnr.CutsceneFlags = new List<Scenario.CutsceneFlag>();
            scnr.CutsceneTitles = new List<Scenario.CutsceneTitle>();
            scnr.DeviceGroups = new List<Scenario.DeviceGroup>();
            scnr.EffectSceneryPalette = new List<Scenario.EffectSceneryBlock2>();
            scnr.EffectScenery = new List<Scenario.EffectSceneryBlock>();
            scnr.Equipment = new List<Scenario.EquipmentBlock>();
            scnr.EquipmentPalette = new List<Scenario.EquipmentPaletteBlock>();
            scnr.Flocks = new List<Scenario.Flock>();
            scnr.Fog = new List<Scenario.FogBlock>();
            scnr.Giants = new List<Scenario.Giant>();
            scnr.GiantPalette = new List<Scenario.GiantPaletteBlock>();
            scnr.Interpolators = new List<Scenario.Interpolator>();
            scnr.LightVolumes = new List<Scenario.LightVolume>();
            scnr.LightVolumePalette = new List<Scenario.LightVolumesPaletteBlock>();
            scnr.Machines = new List<Scenario.Machine>();
            scnr.MachinePalette = new List<Scenario.MachinePaletteBlock>();
            scnr.ScenarioSafeTriggers = new List<Scenario.ScenarioSafeTrigger>();
            scnr.SoundScenery = new List<Scenario.SoundSceneryBlock>();
            scnr.SoundSceneryPalette = new List<Scenario.SoundSceneryPaletteBlock>();
            scnr.Squads = new List<Scenario.Squad>();
            scnr.SquadGroups = new List<Scenario.SquadGroup>();
            scnr.StylePalette = new List<CachedTagInstance>();
            scnr.Terminals = new List<Scenario.Terminal>();
            scnr.TerminalPalette = new List<Scenario.TerminalPaletteBlock>();
            scnr.Vehicles = new List<Scenario.Vehicle>();
            scnr.VehiclePalette = new List<Scenario.VehiclePaletteBlock>();
            scnr.Weapons = new List<Scenario.Weapon>();
            scnr.WeaponPalette = new List<Scenario.WeaponPaletteBlock>();

            scnr.AiObjectives = new List<Scenario.AiObjective>();
            scnr.AiPathfindingData = new List<Scenario.AiPathfindingDatum>();
            scnr.ObjectNames = new List<Scenario.ObjectName>();
            scnr.ScenarioMetagame = new List<Scenario.ScenarioMetagameBlock>();
            scnr.UnitSeatsMapping = new List<Scenario.UnitSeatsMappingBlock>();

            scnr.EditorFolders = new List<Scenario.EditorFolder>();
            scnr.ScenarioResources = new List<Scenario.ScenarioResource>();

            scnr.PlayerStartingLocations = new List<Scenario.PlayerStartingLocation>();
            scnr.PlayerStartingProfile = new List<Scenario.PlayerStartingProfileBlock>();
            scnr.SimulationDefinitionTable = new List<Scenario.SimulationDefinitionTableBlock>();
            scnr.SpawnData = new List<Scenario.SpawnDatum>();

            scnr.Unknown84 = new List<Scenario.UnknownBlock2>();
            scnr.Unknown109 = new List<Scenario.UnknownBlock3>();
            scnr.Unknown134 = new List<Scenario.UnknownBlock4>();
            scnr.Unknown135 = new List<Scenario.UnknownBlock5>();
            scnr.Unknown143 = new List<Scenario.UnknownBlock7>();
            scnr.Unknown147 = new List<Scenario.UnknownBlock8>();
            scnr.Unknown32 = new List<Scenario.UnknownBlock>();
            scnr.Unknown44 = new List<Scenario.UnknownBlock>();
            // scnr.Unknown142 = new List<Scenario.UnknownBlock6>(); // REQUIRED

            scnr.ScenarioKillTriggers = new List<Scenario.ScenarioKillTrigger>();
            scnr.TriggerVolumes = new List<Scenario.TriggerVolume>();
            scnr.SoftCeilings = new List<Scenario.SoftCeiling>();

            // scnr.Zones = new List<Scenario.Zone>();
            // scnr.ZonesetSwitchTriggerVolumes = new List<Scenario.ZoneSetSwitchTriggerVolume>();
            // scnr.DesignerZoneSets = new List<Scenario.DesignerZoneSet>();
            // scnr.ScenarioClusterData = new List<Scenario.ScenarioClusterDatum>();
            // scnr.StructureBsps = new List<Scenario.StructureBspBlock>(); // REQUIRED
            // scnr.ZoneSets = new List<Scenario.ZoneSet>(); // REQUIRED
            // scnr.ZoneSetAudibility = new List<Scenario.ZoneSetAudibilityBlock>(); // REQUIRED
            // scnr.ZoneSetPotentiallyVisibleSets = new List<Scenario.ZoneSetPotentiallyVisibleSet>(); // REQUIRED

            scnr.SandboxEquipment = new List<Scenario.SandboxObject>();
            scnr.SandboxGoalObjects = new List<Scenario.SandboxObject>();
            scnr.SandboxScenery = new List<Scenario.SandboxObject>();
            scnr.SandboxSpawning = new List<Scenario.SandboxObject>();
            scnr.SandboxTeleporters = new List<Scenario.SandboxObject>();
            scnr.SandboxVehicles = new List<Scenario.SandboxObject>();
            scnr.SandboxWeapons = new List<Scenario.SandboxObject>();

            scnr.Globals = new List<BlamCore.Scripting.ScriptGlobal>();
            scnr.Scripts = new List<BlamCore.Scripting.Script>();
            scnr.ScriptReferences = new List<CachedTagInstance>();
            scnr.ScriptExpressions = new List<BlamCore.Scripting.ScriptExpression>();
            scnr.ScriptingData = new List<Scenario.ScriptingDatum>();
            scnr.EditorScenarioData = new byte[0];
            scnr.ObjectSalts = new int[32];

            scnr.Unknown0 = 0;
            scnr.Unknown100 = 0;
            scnr.Unknown101 = 0;
            scnr.Unknown102 = 0;
            scnr.Unknown103 = 0;
            scnr.Unknown104 = 0;
            scnr.Unknown105 = 0;
            scnr.Unknown106 = 0;
            scnr.Unknown107 = 0;
            scnr.Unknown108 = 0;
            scnr.Unknown10 = 0;
            scnr.Unknown110 = 0;
            scnr.Unknown111 = 0;
            scnr.Unknown112 = 0;
            scnr.Unknown113 = 0;
            scnr.Unknown114 = 0;
            scnr.Unknown115 = 0;
            scnr.Unknown116 = 0;
            scnr.Unknown117 = 0;
            scnr.Unknown118 = 0;
            scnr.Unknown119 = 0;
            scnr.Unknown11 = 0;
            scnr.Unknown120 = 0;
            scnr.Unknown121 = 0;
            scnr.Unknown122 = 0;
            scnr.Unknown123 = 0;
            scnr.Unknown124 = 0;
            scnr.Unknown125 = 0;
            scnr.Unknown126 = 0;
            scnr.Unknown127 = 0;
            scnr.Unknown128 = 0;
            scnr.Unknown129 = 0;
            scnr.Unknown12 = 0;
            scnr.Unknown130 = 0;
            scnr.Unknown131 = 0;
            scnr.Unknown132 = 0;
            scnr.Unknown136 = 0;
            scnr.Unknown137 = 0;
            scnr.Unknown138 = 0;
            scnr.Unknown139 = 0;
            scnr.Unknown13 = 0;
            scnr.Unknown140 = 0;
            scnr.Unknown141 = 0;
            scnr.Unknown144 = 0;
            scnr.Unknown145 = 0;
            scnr.Unknown146 = 0;
            scnr.Unknown14 = 0;
            scnr.Unknown150 = 0;
            scnr.Unknown151 = 0;
            scnr.Unknown152 = 0;
            scnr.Unknown15 = 0;
            scnr.Unknown16 = 0;
            scnr.Unknown17 = 0;
            scnr.Unknown1 = 0;
            scnr.Unknown23 = 0;
            scnr.Unknown24 = 0;
            scnr.Unknown25 = 0;
            scnr.Unknown26 = 0;
            scnr.Unknown27 = 0;
            scnr.Unknown28 = 0;
            scnr.Unknown2 = 0;
            scnr.Unknown35 = 0;
            scnr.Unknown36 = 0;
            scnr.Unknown37 = 0;
            scnr.Unknown38 = 0;
            scnr.Unknown39 = 0;
            scnr.Unknown40 = 0;
            scnr.Unknown41 = 0;
            scnr.Unknown42 = 0;
            scnr.Unknown43 = 0;
            scnr.Unknown45 = 0;
            scnr.Unknown46 = 0;
            scnr.Unknown47 = 0;
            scnr.Unknown48 = 0;
            scnr.Unknown49 = 0;
            scnr.Unknown50 = 0;
            scnr.Unknown51 = 0;
            scnr.Unknown52 = 0;
            scnr.Unknown53 = 0;
            scnr.Unknown54 = 0;
            scnr.Unknown55 = 0;
            scnr.Unknown56 = 0;
            scnr.Unknown57 = 0;
            scnr.Unknown58 = 0;
            scnr.Unknown59 = 0;
            scnr.Unknown60 = 0;
            scnr.Unknown61 = 0;
            scnr.Unknown62 = 0;
            scnr.Unknown63 = 0;
            scnr.Unknown64 = 0;
            scnr.Unknown65 = 0;
            scnr.Unknown66 = 0;
            scnr.Unknown67 = 0;
            scnr.Unknown68 = 0;
            scnr.Unknown69 = 0;
            scnr.Unknown70 = 0;
            scnr.Unknown71 = 0;
            scnr.Unknown72 = 0;
            scnr.Unknown73 = 0;
            scnr.Unknown74 = 0;
            scnr.Unknown75 = 0;
            scnr.Unknown76 = 0;
            scnr.Unknown77 = 0;
            scnr.Unknown78 = 0;
            scnr.Unknown79 = 0;
            scnr.Unknown80 = 0;
            scnr.Unknown81 = 0;
            scnr.Unknown82 = 0;
            scnr.Unknown83 = 0;
            scnr.Unknown85 = 0;
            scnr.Unknown86 = 0;
            scnr.Unknown87 = 0;
            scnr.Unknown88 = 0;
            scnr.Unknown89 = 0;
            scnr.Unknown90 = 0;
            scnr.Unknown91 = 0;
            scnr.Unknown92 = 0;
            scnr.Unknown93 = 0;
            scnr.Unknown94 = 0;
            scnr.Unknown95 = 0;
            scnr.Unknown96 = 0;
            scnr.Unknown97 = 0;
            scnr.Unknown98 = 0;
            scnr.Unknown99 = 0;
            scnr.Unknown9 = 0;

            // REQUIRED
            // List<Scenario.UnknownBlock6> Unknown142// REQUIRED
            // List<Scenario.ZoneSet> ZoneSets// REQUIRED
            // List<Scenario.ZoneSetAudibilityBlock> ZoneSetAudibility// REQUIRED
            // List<Scenario.ZoneSetPotentiallyVisibleSet> ZoneSetPotentiallyVisibleSets// REQUIRED

            // scnr.DecalPalette = new List<Scenario.DecalPaletteBlock>(); //REQUIRED
            // scnr.Decals = new List<Scenario.Decal>(); //REQUIRED
            // scnr.StructureBsps = new List<Scenario.StructureBsp>(); // REQUIRED: causes map to load but instantly back out to lobby
            #endregion

            #region Finalize
            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating new ElDorado scenario tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("scnr")]);

                Console.WriteLine("Done.");
            }
            else
            {
                newTag = edTag;
            }

            CacheContext.TagNames[newTag.Index] = blamTagName;

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.scenario\" tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, scnr);
            }

            //
            // Save new string_ids
            //

            if (CacheContext.StringIdCache.Strings.Count != initialStringIDCount)
            {
                Console.Write("Saving string_ids...");

                using (var stringIdStream = CacheContext.OpenStringIdCacheReadWrite())
                    CacheContext.StringIdCache.Save(stringIdStream);

                Console.WriteLine("Done.");
            }

            //
            // Done!
            //

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.scenario\" successfully!");

            Console.WriteLine("Successfully glassed.");
            #endregion

            return true;
        }

        private CachedTagInstance PortScenarioStructureBsp(int sbspIndex, string blamTagName, CachedTagInstance edTag = null)
        {
            #region Main
            var isNew = (edTag == null);

            //
            // Verify Blam scenario_structure_bsp tag
            //

            Console.Write("Verifying Blam scenario_structure_bsp tag {0}...", blamTagName);

            CacheFile.IndexItem blamTag = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "sbsp" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam scenario_structure_bsp tag does not exist: " + blamTagName);
                return null;
            }

            Console.WriteLine("Done.");

            //
            // Verify ED scenario_structure_bsp tag
            //

            if (!isNew)
            {
                Console.Write("Verifying ElDorado scenario_structure_bsp tag index...");

                if (edTag.Group.Name != CacheContext.GetStringId("scenario_structure_bsp"))
                {
                    Console.WriteLine($"Specified tag index is not a scenario_structure_bsp: 0x{edTag.Index:X4}");
                    return null;
                }

                Console.WriteLine("done.");
            }

            //
            // Load Blam scenario_structure_bsp tag
            //

            Console.Write("Loading Blam scenario_structure_bsp tag...");

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamSbsp = blamDeserializer.Deserialize<ScenarioStructureBsp>(blamContext);

            Console.WriteLine("Done.");

            var serializer = new TagSerializer(CacheVersion.HaloOnline106708);

            #endregion

            //
            // Create new tag
            //

            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating new ElDorado scenario_structure_bsp tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("sbsp")]);

                Console.WriteLine("Done.");
            }
            else
            {
                newTag = edTag;
            }

            //
            // Update Blam scenario_structure_bsp tag definition
            //

            #region Geometry 1

            Console.WriteLine("Ignoring Geometry 1...");

            // Nuke decorators, used by geometry1
            for (int i = 0; i < blamSbsp.Clusters.Count; i++)
            {
                blamSbsp.Clusters[i].DecoratorGrids = new List<ScenarioStructureBsp.Cluster.DecoratorGrid>();
            }

            // if (BlamCache.GetRawFromID(sbsp.Geometry.ZoneAssetIndex) != null)
            // {
            //     var geometryFixups = new List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry>(); // has list of all groups and list of members, for members offsets
            // 
            //     var RawID = sbsp.Geometry.ZoneAssetIndex;
            //     var Entry = BlamCache.ResourceGestalt.RawEntries[RawID & ushort.MaxValue];
            //     geometryFixups.Add(Entry);
            // 
            //     sbsp.Geometry.Resource = new ResourceReference
            //     {
            //         Type = 5, // FIXME: hax
            //         DefinitionFixups = new List<ResourceDefinitionFixup>(),
            //         D3DObjectFixups = new List<D3DObjectFixup>(),
            //         Unknown68 = 1
            //     };
            // 
            //     foreach (var fixup in Entry.Fixups)
            //     {
            //         var fixup2 = new ResourceDefinitionFixup
            //         {
            //             DefinitionDataOffset = (uint)fixup.BlockOffset
            //         };
            //         if (fixup.FixupType == 4)
            //         {
            //             fixup2.Address = new ResourceAddress(ResourceAddressType.Resource, fixup.Offset);
            //         }
            //         else
            //         {
            //             fixup2.Address = new ResourceAddress(ResourceAddressType.Definition, fixup.Offset);
            //         }
            //         sbsp.Geometry.Resource.DefinitionFixups.Add(fixup2);
            //     }
            // 
            //     sbsp.Geometry.Resource.DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, Entry.DefinitionAddress);
            //     sbsp.Geometry.Resource.DefinitionData = BlamCache.ResourceGestalt.FixupData.Skip(Entry.FixupOffset).Take(Entry.FixupSize).ToArray();
            // 
            //     var resourceDataStream = new MemoryStream(BlamCache.GetRawFromID(RawID));
            // 
            //     var geostream = new MemoryStream(sbsp.Geometry.Resource.DefinitionData);
            //     var georeader = new EndianReader(geostream, EndianFormat.BigEndian);
            // 
            //     georeader.BaseStream.Position = sbsp.Geometry.Resource.DefinitionAddress.Offset;
            //     georeader.Format = EndianFormat.BigEndian;
            // 
            //     var geodeserializer = new TagDeserializer(CacheVersion.Halo3ODST);
            //     var geodataContext = new DataSerializationContext(georeader, null);
            // 
            //     var geowriter = new EndianWriter(geostream, EndianFormat.BigEndian);
            //     foreach (var fixup in sbsp.Geometry.Resource.DefinitionFixups)
            //     {
            //         geostream.Position = fixup.DefinitionDataOffset;
            //         geowriter.Write(fixup.Address.Value);
            //     }
            //     geostream.Position = sbsp.Geometry.Resource.DefinitionAddress.Offset;
            // 
            //     var geodef = geodeserializer.Deserialize<RenderGeometryApiResourceDefinition>(geodataContext);
            // 
            //     using (var blamResourceStream = new MemoryStream(BlamCache.GetRawFromID(RawID)))
            //     using (var edResourceStream = new MemoryStream())
            //     {
            //         //
            //         // Convert Blam render_geometry_api_resource_definition
            //         //
            // 
            //         var inVertexStream = VertexStreamFactory.Create(BlamCache.Version, blamResourceStream);
            //         var outVertexStream = VertexStreamFactory.Create(CacheContext.Version, edResourceStream);
            // 
            //         for (var i = 0; i < geodef.VertexBuffers.Count; i++)
            //         {
            //             var vertexBuffer = geodef.VertexBuffers[i].Definition;
            // 
            //             var count = vertexBuffer.Count;
            // 
            //             var startPos = (int)edResourceStream.Position;
            //             vertexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);
            // 
            //             blamResourceStream.Position = Entry.Fixups[i].Offset;
            // 
            //             Console.Write("{0}, ", vertexBuffer.Format);
            //             switch (vertexBuffer.Format)
            //             {
            //                 case VertexBufferFormat.World:
            //                     ConvertVertices(count, inVertexStream.ReadWorldVertex, v =>
            //                     {
            //                         v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
            //             outVertexStream.WriteWorldVertex(v);
            //                     });
            //                     break;
            // 
            //                 case VertexBufferFormat.Rigid:
            //                     ConvertVertices(count, inVertexStream.ReadRigidVertex, v =>
            //                     {
            //                         v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
            //             outVertexStream.WriteRigidVertex(v);
            //                     });
            //                     break;
            // 
            //                 case VertexBufferFormat.Skinned:
            //                     ConvertVertices(count, inVertexStream.ReadSkinnedVertex, v =>
            //                     {
            //                         v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
            //             outVertexStream.WriteSkinnedVertex(v);
            //                     });
            //                     break;
            // 
            //                 case VertexBufferFormat.StaticPerPixel:
            //                     ConvertVertices(count, inVertexStream.ReadStaticPerPixelData, outVertexStream.WriteStaticPerPixelData);
            //                     break;
            // 
            //                 case VertexBufferFormat.StaticPerVertex:
            //                     ConvertVertices(count, inVertexStream.ReadStaticPerVertexData, outVertexStream.WriteStaticPerVertexData);
            //                     break;
            // 
            //                 case VertexBufferFormat.AmbientPrt:
            //                     ConvertVertices(count, inVertexStream.ReadAmbientPrtData, outVertexStream.WriteAmbientPrtData);
            //                     break;
            // 
            //                 case VertexBufferFormat.LinearPrt:
            //                     ConvertVertices(count, inVertexStream.ReadLinearPrtData, outVertexStream.WriteLinearPrtData);
            //                     break;
            // 
            //                 case VertexBufferFormat.QuadraticPrt:
            //                     ConvertVertices(count, inVertexStream.ReadQuadraticPrtData, outVertexStream.WriteQuadraticPrtData);
            //                     break;
            // 
            //                 case VertexBufferFormat.StaticPerVertexColor:
            //                     ConvertVertices(count, inVertexStream.ReadStaticPerVertexColorData, outVertexStream.WriteStaticPerVertexColorData);
            //                     break;
            // 
            //                 case VertexBufferFormat.Decorator:
            //                     ConvertVertices(count, inVertexStream.ReadDecoratorVertex, outVertexStream.WriteDecoratorVertex);
            //                     break;
            // 
            //                 case VertexBufferFormat.World2:
            //                     vertexBuffer.Format = VertexBufferFormat.World;
            //                     goto default;
            // 
            //                 default:
            //                     throw new NotSupportedException(vertexBuffer.Format.ToString());
            //             }
            // 
            //             vertexBuffer.Data.Size = (int)edResourceStream.Position - startPos;
            //             vertexBuffer.VertexSize = (short)(vertexBuffer.Data.Size / vertexBuffer.Count);
            //         }
            // 
            //         for (var i = 0; i < geodef.IndexBuffers.Count; i++)
            //         {
            //             var indexBuffer = geodef.IndexBuffers[i].Definition;
            // 
            //             var indexCount = indexBuffer.Data.Size / 2;
            // 
            //             var inIndexStream = new IndexBufferStream(
            //                 blamResourceStream,
            //                 IndexBufferFormat.UInt16,
            //                 EndianFormat.BigEndian);
            // 
            //             var outIndexStream = new IndexBufferStream(
            //                 edResourceStream,
            //                 IndexBufferFormat.UInt16,
            //                 EndianFormat.LittleEndian);
            // 
            //             var startPos = (int)edResourceStream.Position;
            //             indexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);
            // 
            //             blamResourceStream.Position = Entry.Fixups[geodef.VertexBuffers.Count * 2 + i].Offset;
            // 
            //             for (var j = 0; j < indexCount; j++)
            //                 outIndexStream.WriteIndex(inIndexStream.ReadIndex());
            //         }
            // 
            //         sbsp.Geometry.Resource = new ResourceReference
            //         {
            //             Type = 5, // FIXME: hax
            //             DefinitionFixups = new List<ResourceDefinitionFixup>(),
            //             D3DObjectFixups = new List<D3DObjectFixup>(),
            //             Unknown68 = 1,
            //             Owner = edTag
            //         };
            // 
            //         edResourceStream.Position = 0;
            //         CacheContext.AddResource(sbsp.Geometry.Resource, ResourceLocation.Resources, edResourceStream);
            //     }
            // 
            //     var geodeserialization = new ResourceSerializationContext(sbsp.Geometry.Resource);
            // 
            //     var geoserializer = new TagSerializer(CacheVersion.HaloOnline106708);
            // 
            // 
            //     serializer.Serialize(geodeserialization, geodef);
            // }
            // else
            // {
            //     Console.WriteLine("Nothing in the cache! Create empty resource.");
            //     sbsp.Geometry = new RenderGeometry
            //     {
            //     };
            //     sbsp.Geometry.Resource = new ResourceReference
            //     {
            //         DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, 0),
            //         D3DObjectFixups = new List<D3DObjectFixup>(),
            //         DefinitionFixups = new List<ResourceDefinitionFixup>(),
            //         Unknown68 = 0,
            //         Type = 5,
            //         Index = -1,
            //         DefinitionData = new byte[0x30]
            //     };
            // }
            // 
            // 
            // Console.WriteLine("Finished porting Geometry 1!");
            #endregion

            #region CollisionBSP
            Console.WriteLine("Porting collision BSP...");
            var collisionFixups = new List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry>(); // has list of all groups and list of members, for members offsets

            var CollRawID = blamSbsp.ZoneAssetIndex3;
            var CollEntry = BlamCache.ResourceGestalt.DefinitionEntries[CollRawID & ushort.MaxValue];
            collisionFixups.Add(CollEntry);

            blamSbsp.CollisionBspResource = new ResourceReference
            {
                Index = newTag.Index, // WARNING
                DefinitionFixups = new List<ResourceDefinitionFixup>(),
                D3DObjectFixups = new List<D3DObjectFixup>(),
                Type = 0,
                Unknown68 = 1,
                OldLocationFlags = OldResourceLocationFlags.InResources
            };

            foreach (var fixup in CollEntry.Fixups)
            {
                var fixup2 = new ResourceDefinitionFixup
                {
                    DefinitionDataOffset = (uint)fixup.BlockOffset
                };
                if (fixup.FixupType == 4)
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Resource, fixup.Offset);
                }
                else
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Definition, fixup.Offset);
                }
                blamSbsp.CollisionBspResource.DefinitionFixups.Add(fixup2);

            }

            blamSbsp.CollisionBspResource.DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, CollEntry.DefinitionAddress);
            blamSbsp.CollisionBspResource.DefinitionData = BlamCache.ResourceGestalt.FixupData.Skip(CollEntry.Offset).Take(CollEntry.Size).ToArray();

            var CollresourceDataStream = new MemoryStream(BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex3));

            var Collstream = new MemoryStream(blamSbsp.CollisionBspResource.DefinitionData);
            var Collreader = new EndianReader(Collstream, EndianFormat.BigEndian);

            Collreader.BaseStream.Position = blamSbsp.CollisionBspResource.DefinitionAddress.Offset;
            Collreader.Format = EndianFormat.BigEndian;

            var Colldeserializer = new TagDeserializer(CacheVersion.Halo3ODST);
            var ColldataContext = new DataSerializationContext(Collreader, null);

            var Collwriter = new EndianWriter(Collstream, EndianFormat.BigEndian);

            // apply fixups
            foreach (var fixup in blamSbsp.CollisionBspResource.DefinitionFixups)
            {
                Collstream.Position = fixup.DefinitionDataOffset;
                Collwriter.Write(fixup.Address.Value);
            }

            Collstream.Position = blamSbsp.CollisionBspResource.DefinitionAddress.Offset;

            var Colldef = Colldeserializer.Deserialize<StructureBspTagResources>(ColldataContext);

            var Colldeserialization = new ResourceSerializationContext(blamSbsp.CollisionBspResource);

            foreach (var thing in Colldef.Compressions)
            {
                thing.Unknown5 = new ResourceBlockReference<StructureBspTagResources.Compression.Unknown4Block>();
                thing.Unknown6 = 0.0f;
            }


            serializer.Serialize(Colldeserialization, Colldef);

            // Erase all definition fixups because they get regenerated with the proper locations for HO when we reserialize
            blamSbsp.CollisionBspResource.DefinitionFixups = new List<ResourceDefinitionFixup>();

            // reserialize collision BSP into HO format
            var Collserialcontext = new ResourceSerializationContext(blamSbsp.CollisionBspResource);
            CacheContext.Serializer.Serialize(Collserialcontext, Colldef);

            var Collfixups = blamSbsp.CollisionBspResource.DefinitionFixups;

            for (var i = 0; i < blamSbsp.CollisionBspResource.DefinitionFixups.Count; i++)
            {
                if (blamSbsp.CollisionBspResource.DefinitionFixups[i].Address.Value == 0x0)
                {
                    blamSbsp.CollisionBspResource.DefinitionFixups.Remove(blamSbsp.CollisionBspResource.DefinitionFixups[i]);
                }
            }

            blamSbsp.CollisionBspResource.DefinitionFixups = Collfixups;
            blamSbsp.CollisionBspResource.Unknown68 = 1;

            var bspWriter = new EndianWriter(new MemoryStream(new byte[BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex3).Length]), EndianFormat.LittleEndian);
            var bspReader = new EndianReader(new MemoryStream(BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex3)), EndianFormat.BigEndian);

            // Collisions
            foreach (var coll in Colldef.CollisionBsps)
            {
                bspReader.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                for (var i = 0; i < coll.Bsp3dNodes.Count; i++)
                {
                    // read 3d nodes in H3 format
                    var temp1 = bspReader.ReadByte();
                    var temp2 = bspReader.ReadByte();
                    var temp3 = bspReader.ReadByte();
                    var temp4 = bspReader.ReadByte();
                    var temp5 = bspReader.ReadByte();
                    var temp6 = bspReader.ReadByte();
                    var temp7 = bspReader.ReadInt16();

                    // write them in HO format
                    bspWriter.Write(temp7);
                    bspWriter.Write(temp6);
                    bspWriter.Write(temp5);
                    bspWriter.Write(temp4);
                    bspWriter.Write(temp3);
                    bspWriter.Write(temp2);
                    bspWriter.Write(temp1);

                }

                bspReader.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                for (var i = 0; i < coll.Bsp2dNodes.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Planes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Planes.Address.Offset;
                for (var i = 0; i < coll.Planes.Count; i++)
                {

                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                }

                bspReader.BaseStream.Position = coll.Leaves.Address.Offset;
                bspWriter.BaseStream.Position = coll.Leaves.Address.Offset;
                for (var i = 0; i < coll.Leaves.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt32());
                }

                bspReader.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                for (var i = 0; i < coll.Bsp2dReferences.Count; i++)
                {

                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Surfaces.Address.Offset;
                bspWriter.BaseStream.Position = coll.Surfaces.Address.Offset;
                for (var i = 0; i < coll.Surfaces.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadByte());
                    bspWriter.Write(bspReader.ReadByte());
                }

                bspReader.BaseStream.Position = coll.Edges.Address.Offset;
                bspWriter.BaseStream.Position = coll.Edges.Address.Offset;
                for (var i = 0; i < coll.Edges.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Vertices.Address.Offset;
                bspWriter.BaseStream.Position = coll.Vertices.Address.Offset;
                for (var i = 0; i < coll.Vertices.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }
            }

            // Large collision BSPs
            // Collisions
            foreach (var coll in Colldef.LargeCollisionBsps)
            {
                bspReader.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                for (var i = 0; i < coll.Bsp3dNodes.Count; i++)
                {
                    // read 3d nodes in H3 format
                    var temp1 = bspReader.ReadByte();
                    var temp2 = bspReader.ReadByte();
                    var temp3 = bspReader.ReadByte();
                    var temp4 = bspReader.ReadByte();
                    var temp5 = bspReader.ReadByte();
                    var temp6 = bspReader.ReadByte();
                    var temp7 = bspReader.ReadInt16();

                    bspWriter.Write(temp7);
                    bspWriter.Write(temp6);
                    bspWriter.Write(temp5);
                    bspWriter.Write(temp4);
                    bspWriter.Write(temp3);
                    bspWriter.Write(temp2);
                    bspWriter.Write(temp1);
                }

                bspReader.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                for (var i = 0; i < coll.Bsp2dNodes.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Planes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Planes.Address.Offset;
                for (var i = 0; i < coll.Planes.Count; i++)
                {

                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                }

                bspReader.BaseStream.Position = coll.Leaves.Address.Offset;
                bspWriter.BaseStream.Position = coll.Leaves.Address.Offset;
                for (var i = 0; i < coll.Leaves.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt32());
                }

                bspReader.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                for (var i = 0; i < coll.Bsp2dReferences.Count; i++)
                {

                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Surfaces.Address.Offset;
                bspWriter.BaseStream.Position = coll.Surfaces.Address.Offset;
                for (var i = 0; i < coll.Surfaces.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadByte());
                    bspWriter.Write(bspReader.ReadByte());
                }

                bspReader.BaseStream.Position = coll.Edges.Address.Offset;
                bspWriter.BaseStream.Position = coll.Edges.Address.Offset;
                for (var i = 0; i < coll.Edges.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Vertices.Address.Offset;
                bspWriter.BaseStream.Position = coll.Vertices.Address.Offset;
                for (var i = 0; i < coll.Vertices.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }
            }

            // Compressions
            foreach (var comp in Colldef.Compressions)
            {
                bspReader.BaseStream.Position = comp.Bsp3dNodes.Address.Offset;
                bspWriter.BaseStream.Position = comp.Bsp3dNodes.Address.Offset;
                for (var i = 0; i < comp.Bsp3dNodes.Count; i++)
                {
                    // read 3d nodes in H3 format
                    var temp1 = bspReader.ReadByte();
                    var temp2 = bspReader.ReadByte();
                    var temp3 = bspReader.ReadByte();
                    var temp4 = bspReader.ReadByte();
                    var temp5 = bspReader.ReadByte();
                    var temp6 = bspReader.ReadByte();
                    var temp7 = bspReader.ReadInt16();

                    bspWriter.Write(temp7);
                    bspWriter.Write(temp6);
                    bspWriter.Write(temp5);
                    bspWriter.Write(temp4);
                    bspWriter.Write(temp3);
                    bspWriter.Write(temp2);
                    bspWriter.Write(temp1);
                }

                bspReader.BaseStream.Position = comp.Bsp2dNodes.Address.Offset;
                bspWriter.BaseStream.Position = comp.Bsp2dNodes.Address.Offset;
                for (var i = 0; i < comp.Bsp2dNodes.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = comp.Planes.Address.Offset;
                bspWriter.BaseStream.Position = comp.Planes.Address.Offset;
                for (var i = 0; i < comp.Planes.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                }

                bspReader.BaseStream.Position = comp.Leaves.Address.Offset;
                bspWriter.BaseStream.Position = comp.Leaves.Address.Offset;
                for (var i = 0; i < comp.Leaves.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt32());
                }

                bspReader.BaseStream.Position = comp.Bsp2dReferences.Address.Offset;
                bspWriter.BaseStream.Position = comp.Bsp2dReferences.Address.Offset;
                for (var i = 0; i < comp.Bsp2dReferences.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = comp.Surfaces.Address.Offset;
                bspWriter.BaseStream.Position = comp.Surfaces.Address.Offset;
                for (var i = 0; i < comp.Surfaces.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadByte());
                    bspWriter.Write(bspReader.ReadByte());
                }

                bspReader.BaseStream.Position = comp.Edges.Address.Offset;
                bspWriter.BaseStream.Position = comp.Edges.Address.Offset;
                for (var i = 0; i < comp.Edges.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = comp.Vertices.Address.Offset;
                bspWriter.BaseStream.Position = comp.Vertices.Address.Offset;
                for (var i = 0; i < comp.Vertices.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = comp.Unknown1.Address.Offset;
                bspWriter.BaseStream.Position = comp.Unknown1.Address.Offset;
                for (var i = 0; i < comp.Unknown1.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt32());
                }

                bspReader.BaseStream.Position = comp.Unknown2.Address.Offset;
                bspWriter.BaseStream.Position = comp.Unknown2.Address.Offset;
                for (var i = 0; i < comp.Unknown2.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt32());
                    bspWriter.Write(bspReader.ReadUInt32());
                }

                bspReader.BaseStream.Position = comp.Unknown3.Address.Offset;
                bspWriter.BaseStream.Position = comp.Unknown3.Address.Offset;
                for (var i = 0; i < comp.Unknown3.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt32());
                }

                foreach (var coll in comp.CollisionBsps)
                {
                    bspReader.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                    for (var i = 0; i < coll.Bsp3dNodes.Count; i++)
                    {
                        // read 3d nodes in H3 format
                        var temp1 = bspReader.ReadByte();
                        var temp2 = bspReader.ReadByte();
                        var temp3 = bspReader.ReadByte();
                        var temp4 = bspReader.ReadByte();
                        var temp5 = bspReader.ReadByte();
                        var temp6 = bspReader.ReadByte();
                        var temp7 = bspReader.ReadInt16();

                        bspWriter.Write(temp7);
                        bspWriter.Write(temp6);
                        bspWriter.Write(temp5);
                        bspWriter.Write(temp4);
                        bspWriter.Write(temp3);
                        bspWriter.Write(temp2);
                        bspWriter.Write(temp1);
                    }

                    bspReader.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                    for (var i = 0; i < coll.Bsp2dNodes.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                    }

                    bspReader.BaseStream.Position = coll.Planes.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Planes.Address.Offset;
                    for (var i = 0; i < coll.Planes.Count; i++)
                    {

                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                    }

                    bspReader.BaseStream.Position = coll.Leaves.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Leaves.Address.Offset;
                    for (var i = 0; i < coll.Leaves.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadUInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt32());
                    }

                    bspReader.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                    for (var i = 0; i < coll.Bsp2dReferences.Count; i++)
                    {

                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                    }

                    bspReader.BaseStream.Position = coll.Surfaces.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Surfaces.Address.Offset;
                    for (var i = 0; i < coll.Surfaces.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadByte());
                        bspWriter.Write(bspReader.ReadByte());
                    }

                    bspReader.BaseStream.Position = coll.Edges.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Edges.Address.Offset;
                    for (var i = 0; i < coll.Edges.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                    }

                    bspReader.BaseStream.Position = coll.Vertices.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Vertices.Address.Offset;
                    for (var i = 0; i < coll.Vertices.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                    }
                }

                foreach (var mopp in comp.CollisionMoppCodes)
                {
                    bspReader.BaseStream.Position = mopp.Data.Address.Offset;
                    bspWriter.BaseStream.Position = mopp.Data.Address.Offset;
                    for (var i = 0; i < mopp.Data.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadByte());
                    }
                }
            }

            bspWriter.BaseStream.Position = 0;
            CacheContext.AddResource(blamSbsp.CollisionBspResource, ResourceLocation.Resources, bspWriter.BaseStream);
            Console.WriteLine("Finished porting collision BSP!");

            #endregion

            #region Deserialize Lbsp to get the ResourceID. Search for Lbsp based on sbsp blam tagname.

            var RawID2 = 0;

            // sbspIndex
            // Deserialize sLdT to get the correct Lbsp resource.
            CacheFile.IndexItem blamTagsLdT = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "sLdT") // Lbsp name always matches the sbsp name. there's extra Lbsps with a longer name
                {
                    blamTagsLdT = tag;
                    break;
                }
            }

            var blamDeserializersLdT = new TagDeserializer(BlamCache.Version);
            var blamContextsLdT = new CacheSerializationContext(CacheContext, BlamCache, blamTagsLdT);
            var blamsLdT = blamDeserializersLdT.Deserialize<ScenarioLightmap>(blamContextsLdT);

            var blamDeserializerLbsp = new TagDeserializer(BlamCache.Version);
            var blamContextLbsp = new CacheSerializationContext(CacheContext, BlamCache, BlamCache.IndexItems.Find(i => i.ID == blamsLdT.LightmapDataReferences[sbspIndex].LightmapData.Index));
            var blamLbsp = blamDeserializerLbsp.Deserialize<ScenarioLightmapBspData>(blamContextLbsp);

            RawID2 = BitConverter.ToInt16(BitConverter.GetBytes(blamLbsp.Geometry.ZoneAssetIndex), 0);

            #endregion

            #region Geometry2
            Console.WriteLine("Porting Geometry 2/Main Render Geometry...");
            var geometryFixups2 = new List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry>(); // has list of all groups and list of members, for members offsets

            var Entry2 = BlamCache.ResourceGestalt.DefinitionEntries[RawID2 & ushort.MaxValue];
            geometryFixups2.Add(Entry2);

            blamSbsp.Geometry2.Resource = new ResourceReference
            {
                Type = 5, // FIXME: hax
                DefinitionFixups = new List<ResourceDefinitionFixup>(),
                D3DObjectFixups = new List<D3DObjectFixup>(),
                Unknown68 = 1
            };

            foreach (var fixup in Entry2.Fixups)
            {
                var fixup2 = new ResourceDefinitionFixup
                {
                    DefinitionDataOffset = (uint)fixup.BlockOffset
                };
                if (fixup.FixupType == 4)
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Resource, fixup.Offset);
                }
                else
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Definition, fixup.Offset);
                }
                blamSbsp.Geometry2.Resource.DefinitionFixups.Add(fixup2);
            }

            blamSbsp.Geometry2.Resource.DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, Entry2.DefinitionAddress);
            blamSbsp.Geometry2.Resource.DefinitionData = BlamCache.ResourceGestalt.FixupData.Skip(Entry2.Offset).Take(Entry2.Size).ToArray();

            var resourceDataStream2 = new MemoryStream(BlamCache.GetRawFromID(RawID2));

            var geostream2 = new MemoryStream(blamSbsp.Geometry2.Resource.DefinitionData);
            var georeader2 = new EndianReader(geostream2, EndianFormat.BigEndian);

            georeader2.BaseStream.Position = blamSbsp.Geometry2.Resource.DefinitionAddress.Offset;
            georeader2.Format = EndianFormat.BigEndian;

            var geodeserializer2 = new TagDeserializer(BlamCore.Cache.CacheVersion.Halo3ODST);
            var geodataContext2 = new DataSerializationContext(georeader2, null);

            var geowriter2 = new EndianWriter(geostream2, EndianFormat.BigEndian);
            foreach (var fixup in blamSbsp.Geometry2.Resource.DefinitionFixups)
            {
                geostream2.Position = fixup.DefinitionDataOffset;
                geowriter2.Write(fixup.Address.Value);
            }
            geostream2.Position = blamSbsp.Geometry2.Resource.DefinitionAddress.Offset;

            var geodef2 = geodeserializer2.Deserialize<RenderGeometryApiResourceDefinition>(geodataContext2);

            using (var blamResourceStream = new MemoryStream(BlamCache.GetRawFromID(RawID2)))
            using (var edResourceStream = new MemoryStream())
            {
                //
                // Convert Blam render_geometry_api_resource_definition
                //

                var inVertexStream = VertexStreamFactory.Create(BlamCache.Version, blamResourceStream);
                var outVertexStream = VertexStreamFactory.Create(CacheContext.Version, edResourceStream);

                for (var i = 0; i < geodef2.VertexBuffers.Count; i++)
                {
                    var vertexBuffer = geodef2.VertexBuffers[i].Definition;

                    var count = vertexBuffer.Count;

                    var startPos = (int)edResourceStream.Position;
                    vertexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                    blamResourceStream.Position = Entry2.Fixups[i].Offset;

                    // Console.WriteLine(vertexBuffer.Format.ToString());
                    switch (vertexBuffer.Format)
                    {
                        case VertexBufferFormat.World:
                            ConvertVertices(count, inVertexStream.ReadWorldVertex, v =>
                            {
                                v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
                                outVertexStream.WriteWorldVertex(v);
                            });
                            break;

                        case VertexBufferFormat.Rigid:
                            ConvertVertices(count, inVertexStream.ReadRigidVertex, v =>
                            {
                                v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
                                outVertexStream.WriteRigidVertex(v);
                            });
                            break;

                        case VertexBufferFormat.Skinned:
                            ConvertVertices(count, inVertexStream.ReadSkinnedVertex, v =>
                            {
                                v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
                                outVertexStream.WriteSkinnedVertex(v);
                            });
                            break;

                        case VertexBufferFormat.StaticPerPixel:
                            ConvertVertices(count, inVertexStream.ReadStaticPerPixelData, outVertexStream.WriteStaticPerPixelData);
                            break;

                        case VertexBufferFormat.StaticPerVertex:
                            ConvertVertices(count, inVertexStream.ReadStaticPerVertexData, outVertexStream.WriteStaticPerVertexData);
                            break;

                        case VertexBufferFormat.AmbientPrt:
                            ConvertVertices(count, inVertexStream.ReadAmbientPrtData, outVertexStream.WriteAmbientPrtData);
                            break;

                        case VertexBufferFormat.LinearPrt:
                            ConvertVertices(count, inVertexStream.ReadLinearPrtData, outVertexStream.WriteLinearPrtData);
                            break;

                        case VertexBufferFormat.QuadraticPrt:
                            ConvertVertices(count, inVertexStream.ReadQuadraticPrtData, outVertexStream.WriteQuadraticPrtData);
                            break;

                        case VertexBufferFormat.StaticPerVertexColor:
                            ConvertVertices(count, inVertexStream.ReadStaticPerVertexColorData, outVertexStream.WriteStaticPerVertexColorData);
                            break;

                        case VertexBufferFormat.Decorator:
                            ConvertVertices(count, inVertexStream.ReadDecoratorVertex, outVertexStream.WriteDecoratorVertex);
                            break;

                        case VertexBufferFormat.World2:
                            vertexBuffer.Format = VertexBufferFormat.World;
                            goto default;

                        default:
                            throw new NotSupportedException(vertexBuffer.Format.ToString());
                    }

                    vertexBuffer.Data.Size = (int)edResourceStream.Position - startPos;
                    vertexBuffer.VertexSize = (short)(vertexBuffer.Data.Size / vertexBuffer.Count);
                }

                for (var i = 0; i < geodef2.IndexBuffers.Count; i++)
                {
                    var indexBuffer = geodef2.IndexBuffers[i].Definition;

                    var indexCount = indexBuffer.Data.Size / 2;

                    var inIndexStream = new IndexBufferStream(
                        blamResourceStream,
                        IndexBufferFormat.UInt16,
                        EndianFormat.BigEndian);

                    var outIndexStream = new IndexBufferStream(
                        edResourceStream,
                        IndexBufferFormat.UInt16,
                        EndianFormat.LittleEndian);

                    var startPos = (int)edResourceStream.Position;
                    indexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                    blamResourceStream.Position = Entry2.Fixups[geodef2.VertexBuffers.Count * 2 + i].Offset;

                    for (var j = 0; j < indexCount; j++)
                        outIndexStream.WriteIndex(inIndexStream.ReadIndex());
                }

                blamSbsp.Geometry2.Resource = new ResourceReference
                {
                    Type = 5, // FIXME: hax
                    DefinitionFixups = new List<ResourceDefinitionFixup>(),
                    D3DObjectFixups = new List<D3DObjectFixup>(),
                    Unknown68 = 1,
                    Owner = newTag
                };

                edResourceStream.Position = 0;
                CacheContext.AddResource(blamSbsp.Geometry2.Resource, ResourceLocation.Resources, edResourceStream);
            }

            var geodeserialization2 = new ResourceSerializationContext(blamSbsp.Geometry2.Resource);

            var geoserializer2 = new TagSerializer(CacheVersion.HaloOnline106708);


            serializer.Serialize(geodeserialization2, geodef2);


            Console.WriteLine("Finished porting Geometry 2/Main Render Geometry!");

            #endregion

            #region BSP Tagblock Resource
            Console.WriteLine("Porting tagblock resource/4th resource...");

            blamSbsp.PathfindingResource = new ResourceReference
            {
                DefinitionFixups = new List<ResourceDefinitionFixup>(),
                D3DObjectFixups = new List<D3DObjectFixup>()
            };

            var tagblockFixups = new List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry>(); // has list of all groups and list of members, for members offsets

            var RawID4 = blamSbsp.ZoneAssetIndex4;
            var Entry4 = BlamCache.ResourceGestalt.DefinitionEntries[RawID4 & ushort.MaxValue];
            tagblockFixups.Add(Entry4);

            foreach (var fixup in Entry4.Fixups)
            {
                var fixup2 = new ResourceDefinitionFixup
                {
                    DefinitionDataOffset = (uint)fixup.BlockOffset
                };
                if (fixup.FixupType == 4)
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Resource, fixup.Offset);
                }
                else
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Definition, fixup.Offset);
                }
                blamSbsp.PathfindingResource.DefinitionFixups.Add(fixup2);
            }

            blamSbsp.PathfindingResource.DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, Entry4.DefinitionAddress);
            blamSbsp.PathfindingResource.DefinitionData = BlamCache.ResourceGestalt.FixupData.Skip(Entry4.Offset).Take(Entry4.Size).ToArray();

            // Need to add Index 4 into SBSP struct
            var tagblockDataStream = new MemoryStream(BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex4));

            var tagblockStream = new MemoryStream(blamSbsp.PathfindingResource.DefinitionData);
            var tagblockReader = new EndianReader(tagblockStream, EndianFormat.BigEndian);

            tagblockReader.BaseStream.Position = blamSbsp.PathfindingResource.DefinitionAddress.Offset;
            tagblockReader.Format = EndianFormat.BigEndian;

            var tagblockDeserializer = new TagDeserializer(BlamCore.Cache.CacheVersion.Halo3ODST);
            var tagblockDataContext = new DataSerializationContext(tagblockReader, null);

            var tagblockWriter = new EndianWriter(tagblockStream, EndianFormat.BigEndian);
            foreach (var fixup in blamSbsp.PathfindingResource.DefinitionFixups)
            {
                tagblockStream.Position = fixup.DefinitionDataOffset;
                tagblockWriter.Write(fixup.Address.Value);
            }
            tagblockStream.Position = blamSbsp.PathfindingResource.DefinitionAddress.Offset;

            var tagblockDef = tagblockDeserializer.Deserialize<SbspTagblockResource>(tagblockDataContext);

            var tagblockSerialization = new ResourceSerializationContext(blamSbsp.PathfindingResource);

            blamSbsp.PathfindingResource.Type = 5;
            blamSbsp.PathfindingResource.Unknown68 = 1;

            var tagblockResourceWriter = new EndianWriter(new MemoryStream(new byte[BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex4).Length]), EndianFormat.LittleEndian);
            var tagblockResourceReader = new EndianReader(new MemoryStream(BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex4)), EndianFormat.BigEndian);

            File.WriteAllBytes("test123", BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex4));

            // fix endian
            tagblockResourceReader.BaseStream.Position = tagblockDef.Unknown1s.Address.Offset;
            tagblockResourceWriter.BaseStream.Position = tagblockDef.Unknown1s.Address.Offset;
            for (var i = 0; i < tagblockDef.Unknown1s.Count; i++)
            {
                tagblockResourceWriter.Write(tagblockResourceReader.ReadUInt16());
                tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
            }

            tagblockResourceReader.BaseStream.Position = tagblockDef.UnknownRaw7ths.Address.Offset;
            tagblockResourceWriter.BaseStream.Position = tagblockDef.UnknownRaw7ths.Address.Offset;
            for (var i = 0; i < tagblockDef.UnknownRaw7ths.Count; i++)
            {
                tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
            }

            // Not really required since int8/byte are endian agnostic but do it because otherwise all of them would be 0x00
            tagblockResourceReader.BaseStream.Position = tagblockDef.UnknownRaw6ths.Address.Offset;
            tagblockResourceWriter.BaseStream.Position = tagblockDef.UnknownRaw6ths.Address.Offset;
            for (var i = 0; i < tagblockDef.UnknownRaw6ths.Count; i++)
            {
                tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
                tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
            }


            /*
            foreach (var data in tagblockDef.Pathfinding)
            {
                tagblockResourceReader.BaseStream.Position = data.sectors.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.sectors.Address.Offset;
                for (var i = 0; i < data.sectors.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
                }

                tagblockResourceReader.BaseStream.Position = data.link.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.link.Address.Offset;
                for (var i = 0; i < data.link.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                }

                tagblockResourceReader.BaseStream.Position = data.references.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.references.Address.Offset;
                for (var i = 0; i < data.references.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
                }

                tagblockResourceReader.BaseStream.Position = data.bsp2dnodes.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.bsp2dnodes.Address.Offset;
                for (var i = 0; i < data.bsp2dnodes.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                }

                tagblockResourceReader.BaseStream.Position = data.vertices.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.vertices.Address.Offset;
                for (var i = 0; i < data.vertices.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                }

                foreach(var reference in data.objectreferences)
                {
                    foreach(var unk in reference.unknown2)
                    {
                        // TODO: tagblockResourceReader.BaseStream.Position = unk.unknown3.Address.Offset;
                        // TODO: tagblockResourceWriter.BaseStream.Position = unk.unknown3.Address.Offset;
                        for (var i = 0; i < unk.unknown3.Count; i++)
                        {
                            bspWriter.Write(tagblockResourceReader.ReadInt32());
                        }
                    }
                }

                tagblockResourceReader.BaseStream.Position = data.pathfindinghints.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.pathfindinghints.Address.Offset;
                for (var i = 0; i < data.pathfindinghints.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                }

                tagblockResourceReader.BaseStream.Position = data.instancedgeometryreferences.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.instancedgeometryreferences.Address.Offset;
                for (var i = 0; i < data.instancedgeometryreferences.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                }

                tagblockResourceReader.BaseStream.Position = data.UnknownTagblock1.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.UnknownTagblock1.Address.Offset;
                for (var i = 0; i < data.UnknownTagblock1.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
                }

                foreach(var unk in data.UnknownTagblock2)
                {
                    // TODO: tagblockResourceReader.BaseStream.Position = unk.Unknown.Address.Offset;
                    // TODO: tagblockResourceWriter.BaseStream.Position = unk.Unknown.Address.Offset;
                    for (var i = 0; i < unk.Unknown.Count; i++)
                    {
                        tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
                    }
                }

            }
            */


            tagblockResourceWriter.BaseStream.Position = 0;
            CacheContext.AddResource(blamSbsp.PathfindingResource, ResourceLocation.Resources, tagblockResourceWriter.BaseStream);

            blamSbsp.PathfindingResource.Unknown68 = 1;
            blamSbsp.PathfindingResource.Type = 7;

            var tagblockSerializer = new TagSerializer(CacheVersion.HaloOnline106708);


            serializer.Serialize(tagblockSerialization, tagblockDef);

            Console.WriteLine("Finished porting tagblock resource/4th resource!");
            #endregion

            File.WriteAllBytes("model_output", BlamCache.GetRawFromID(RawID2));

            var fileStream = File.Create("original_endian");
            bspReader.BaseStream.Seek(0, SeekOrigin.Begin);
            bspReader.BaseStream.CopyTo(fileStream);
            fileStream.Close();

            var bspfileStream = File.Create("fixed_endian");
            bspWriter.BaseStream.Seek(0, SeekOrigin.Begin);
            bspWriter.BaseStream.CopyTo(bspfileStream);
            bspfileStream.Close();

            bspWriter.Flush();
            bspWriter.Close();

            blamSbsp.Geometry2.Resource.Unknown68 = 1;
            blamSbsp.CollisionBspResource.Unknown68 = 1;
            blamSbsp.PathfindingResource.Unknown68 = 1;

            foreach (var mat in blamSbsp.CollisionMaterials)
            {
                mat.RenderMethod = CacheContext.TagCache.Index[0x101f];
            }

            foreach (var mat in blamSbsp.Materials)
            {
                if (BlamCache.IndexItems.Find(i => i.ID == mat.RenderMethod.Index) != null)
                {
                    mat.RenderMethod = PortShader(BlamCache.IndexItems.Find(i => i.ID == mat.RenderMethod.Index).Filename);
                }
            }

            // for (int i = 0; i < sbsp.Decorators.Count; i++)
            //     sbsp.Decorators[i] = CacheContext.TagCache.Index[0x2ecd];

            foreach (var geo in blamSbsp.InstancedGeometryInstances)
            {
                foreach (var def in geo.CollisionDefinitions)
                {
                    def.BspIndex = 0;
                }
            }

            foreach (var cluster in blamSbsp.Clusters)
            {
                cluster.BackgroundSoundEnvironmentIndex = -1;
            }

            foreach (var thing in blamSbsp.UnknownSoundClustersA)
            {
                thing.BackgroundSoundEnvironmentIndex = -1;
            }

            foreach (var thing in blamSbsp.UnknownSoundClustersB)
            {
                thing.BackgroundSoundEnvironmentIndex = -1;
            }

            foreach (var thing in blamSbsp.UnknownSoundClustersC)
            {
                thing.BackgroundSoundEnvironmentIndex = -1;
            }

            // Add Sbsp tag ref to each cluster
            // for (int i = 0; i < sbsp.Clusters.Count; i++)
            // {
            //     sbsp.Clusters[i].Bsp = ArgumentParser.ParseTagSpecifier(CacheContext, "0x3489");
            // }

            //
            // Finalize new ElDorado scenario_structure_bsp tag
            //

            CacheContext.TagNames[newTag.Index] = blamTagName;

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.scenario_structure_bsp\" tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, blamSbsp);
            }

            //
            // Done!
            //

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.scenario_structure_bsp\" successfully!");

            return newTag;
        }

        private CachedTagInstance PortScenarioLightmap(string blamTagName, CachedTagInstance edTag = null)
        {
            var isNew = (edTag == null);

            //
            // Verify Blam scenario_structure_bsp tag
            //

            Console.Write("Verifying Blam scenario_lightmap tag...");

            CacheFile.IndexItem blamTag = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "sLdT" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam scenario_lightmap tag does not exist: " + blamTagName);
                return null;
            }

            Console.WriteLine("Done.");

            //
            // Verify ED scenario_structure_bsp tag
            //

            if (!isNew)
            {
                Console.Write("Verifying ElDorado scenario_structure_bsp tag index...");

                if (edTag.Group.Name != CacheContext.GetStringId("scenario_lightmap"))
                {
                    Console.WriteLine($"Specified tag index is not a scenario_structure_bsp: 0x{edTag.Index:X4}");
                    return null;
                }

                Console.WriteLine("done.");
            }

            //
            // Load Blam scenario_structure_bsp tag
            //

            Console.Write("Loading Blam scenario_lightmap tag...");

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var sLdT = blamDeserializer.Deserialize<ScenarioLightmap>(blamContext);

            Console.WriteLine("Done.");

            //
            // Update Blam scenario_structure_bsp tag definition
            //

            for (int j = 0; j < sLdT.LightmapDataReferences.Count; j++)
            {
                sLdT.LightmapDataReferences[j].LightmapData =
                    PortScenarioLightmapBSP(BlamCache.IndexItems.Find(i => i.ID == sLdT.LightmapDataReferences[j].LightmapData.Index).Filename);
            }

            //
            // Finalize new ElDorado scenario_structure_bsp tag
            //

            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating new ElDorado scenario_lightmap tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("sLdT")]);

                Console.WriteLine("Done.");
            }
            else
            {
                newTag = edTag;
            }

            CacheContext.TagNames[newTag.Index] = blamTagName;

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.scenario_lightmap\" tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, sLdT);
            }

            //
            // Done!
            //

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.scenario_lightmap\" successfully!");

            return newTag;
        }

        private CachedTagInstance PortScenarioLightmapBSP(string blamTagName, CachedTagInstance edTag = null)
        {
            var isNew = (edTag == null);

            //
            // Verify Blam scenario_structure_bsp tag
            //

            Console.Write("Verifying Blam scenario_lightmap_bsp_data tag...");

            CacheFile.IndexItem blamTag = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "Lbsp" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam scenario_lightmap_bsp_data tag does not exist: " + blamTagName);
                return null;
            }

            Console.WriteLine("Done.");

            //
            // Verify ED scenario_structure_bsp tag
            //

            if (!isNew)
            {
                Console.Write("Verifying ElDorado scenario_structure_bsp tag index...");

                if (edTag.Group.Name != CacheContext.GetStringId("scenario_lightmap_bsp_data"))
                {
                    Console.WriteLine($"Specified tag index is not a scenario_structure_bsp: 0x{edTag.Index:X4}");
                    return null;
                }

                Console.WriteLine("Done.");
            }

            //
            // Create new tag
            //

            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating new ElDorado scenario_lightmap tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("Lbsp")]);

                Console.WriteLine("Done.");
            }
            else
            {
                newTag = edTag;
            }

            //
            // Load Blam scenario_structure_bsp tag
            //

            Console.Write("Loading Blam scenario_lightmap tag...");

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var Lbsp = blamDeserializer.Deserialize<ScenarioLightmapBspData>(blamContext);

            Console.WriteLine("Done.");

            //
            // Load Blam resource fixups
            //

            var fixupEntry = BlamCache.ResourceGestalt.DefinitionEntries[Lbsp.Geometry.ZoneAssetIndex & ushort.MaxValue];

            var resourceDefinition = new RenderGeometryApiResourceDefinition
            {
                VertexBuffers = new List<D3DPointer<VertexBufferDefinition>>(),
                IndexBuffers = new List<D3DPointer<IndexBufferDefinition>>()
            };

            using (var fixupStream = new MemoryStream(BlamCache.ResourceGestalt.FixupData))
            using (var reader = new EndianReader(fixupStream, EndianFormat.BigEndian))
            {
                var dataContext = new DataSerializationContext(reader, null);

                reader.SeekTo(fixupEntry.Offset + (fixupEntry.Size - 24));

                var vertexBufferCount = reader.ReadInt32();
                reader.Skip(8);
                var indexBufferCount = reader.ReadInt32();

                reader.SeekTo(fixupEntry.Offset);

                for (var i = 0; i < vertexBufferCount; i++)
                {
                    resourceDefinition.VertexBuffers.Add(new D3DPointer<VertexBufferDefinition>
                    {
                        Definition = new VertexBufferDefinition
                        {
                            Count = reader.ReadInt32(),
                            Format = (VertexBufferFormat)reader.ReadInt16(),
                            VertexSize = reader.ReadInt16(),
                            Data = new ResourceDataReference
                            {
                                Size = reader.ReadInt32(),
                                Unused4 = reader.ReadInt32(),
                                Unused8 = reader.ReadInt32(),
                                Address = new ResourceAddress(ResourceAddressType.Memory, reader.ReadInt32()),
                                Unused10 = reader.ReadInt32()
                            }
                        }
                    });
                }



                reader.Skip(vertexBufferCount * 12);

                for (var i = 0; i < indexBufferCount; i++)
                {
                    resourceDefinition.IndexBuffers.Add(new D3DPointer<IndexBufferDefinition>
                    {
                        Definition = new IndexBufferDefinition
                        {
                            Type = (PrimitiveType)reader.ReadInt32(),
                            Data = new ResourceDataReference
                            {
                                Size = reader.ReadInt32(),
                                Unused4 = reader.ReadInt32(),
                                Unused8 = reader.ReadInt32(),
                                Address = new ResourceAddress(ResourceAddressType.Memory, reader.ReadInt32()),
                                Unused10 = reader.ReadInt32()
                            }
                        }
                    });
                }
            }

            //
            // Convert Blam data to ElDorado data
            //

            using (var edResourceStream = new MemoryStream())
            {
                using (var blamResourceStream = new MemoryStream(BlamCache.GetRawFromID(Lbsp.Geometry.ZoneAssetIndex)))
                {
                    //
                    // Convert Blam render_geometry_api_resource_definition
                    //

                    var inVertexStream = VertexStreamFactory.Create(BlamCache.Version, blamResourceStream);
                    var outVertexStream = VertexStreamFactory.Create(CacheContext.Version, edResourceStream);

                    for (var i = 0; i < resourceDefinition.VertexBuffers.Count; i++)
                    {
                        var vertexBuffer = resourceDefinition.VertexBuffers[i].Definition;

                        var count = vertexBuffer.Count;

                        var startPos = (int)edResourceStream.Position;
                        vertexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                        blamResourceStream.Position = fixupEntry.Fixups[i].Offset;

                        switch (vertexBuffer.Format)
                        {
                            case VertexBufferFormat.World:  // C100
                                ConvertVertices(count, inVertexStream.ReadWorldVertex, outVertexStream.WriteWorldVertex);
                                break;

                            case VertexBufferFormat.Rigid: // C100
                                ConvertVertices(count, inVertexStream.ReadRigidVertex, outVertexStream.WriteRigidVertex);
                                break;

                            case VertexBufferFormat.Skinned:
                                ConvertVertices(count, inVertexStream.ReadSkinnedVertex, outVertexStream.WriteSkinnedVertex);
                                break;

                            case VertexBufferFormat.StaticPerPixel: // C100
                                ConvertVertices(count, inVertexStream.ReadStaticPerPixelData, outVertexStream.WriteStaticPerPixelData);
                                break;

                            case VertexBufferFormat.StaticPerVertex: // C100
                                ConvertVertices(count, inVertexStream.ReadStaticPerVertexData, v =>
                                {
                                    v.Texcoord = v.Texcoord - 0x80808081;
                                    v.Texcoord2 = v.Texcoord2 - 0x80808081;
                                    v.Texcoord3 = v.Texcoord3 - 0x80808081;
                                    v.Texcoord4 = v.Texcoord4 - 0x80808081;
                                    v.Texcoord5 = v.Texcoord5 - 0x80808081;

                                    outVertexStream.WriteStaticPerVertexData(v);
                                });
                                break;

                            case VertexBufferFormat.AmbientPrt:  // C100
                                ConvertVertices(count, inVertexStream.ReadAmbientPrtData, outVertexStream.WriteAmbientPrtData);
                                break;

                            case VertexBufferFormat.LinearPrt:
                                ConvertVertices(count, inVertexStream.ReadLinearPrtData, outVertexStream.WriteLinearPrtData);
                                break;

                            case VertexBufferFormat.QuadraticPrt:
                                ConvertVertices(count, inVertexStream.ReadQuadraticPrtData, outVertexStream.WriteQuadraticPrtData);
                                break;

                            case VertexBufferFormat.StaticPerVertexColor:
                                ConvertVertices(count, inVertexStream.ReadStaticPerVertexColorData, outVertexStream.WriteStaticPerVertexColorData);
                                break;

                            case VertexBufferFormat.Decorator:
                                ConvertVertices(count, inVertexStream.ReadDecoratorVertex, outVertexStream.WriteDecoratorVertex);
                                break;

                            case VertexBufferFormat.World2:
                                vertexBuffer.Format = VertexBufferFormat.World;
                                goto case VertexBufferFormat.World;

                            default:
                                throw new NotSupportedException(vertexBuffer.Format.ToString());
                        }

                        vertexBuffer.Data.Size = (int)edResourceStream.Position - startPos;
                        vertexBuffer.VertexSize = (short)(vertexBuffer.Data.Size / vertexBuffer.Count);
                    }

                    for (var i = 0; i < resourceDefinition.IndexBuffers.Count; i++)
                    {
                        var indexBuffer = resourceDefinition.IndexBuffers[i].Definition;

                        var indexCount = indexBuffer.Data.Size / 2;

                        var inIndexStream = new IndexBufferStream(
                            blamResourceStream,
                            IndexBufferFormat.UInt16,
                            EndianFormat.BigEndian);

                        var outIndexStream = new IndexBufferStream(
                            edResourceStream,
                            IndexBufferFormat.UInt16,
                            EndianFormat.LittleEndian);

                        var startPos = (int)edResourceStream.Position;
                        indexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                        blamResourceStream.Position = fixupEntry.Fixups[resourceDefinition.VertexBuffers.Count * 2 + i].Offset;

                        for (var j = 0; j < indexCount; j++)
                            outIndexStream.WriteIndex(inIndexStream.ReadIndex());
                    }
                }

                //
                // Finalize the new ElDorado scenario_lightmap_bsp_data tag
                //

                CacheContext.TagNames[newTag.Index] = blamTagName;

                Lbsp.Geometry.Resource = new ResourceReference
                {
                    Type = 5, // FIXME: hax
                    DefinitionFixups = new List<ResourceDefinitionFixup>(),
                    D3DObjectFixups = new List<D3DObjectFixup>(),
                    Unknown68 = 1,
                    Owner = newTag // WARNING
                };

                Console.WriteLine("Writing resource data...");

                edResourceStream.Position = 0;

                var resourceContext = new ResourceSerializationContext(Lbsp.Geometry.Resource);
                CacheContext.Serializer.Serialize(resourceContext, resourceDefinition);
                CacheContext.AddResource(Lbsp.Geometry.Resource, ResourceLocation.Resources, edResourceStream);
            }

            //
            // Convert UnknownSection.Unknown byte[] endian
            //

            for (int i = 0; i < Lbsp.Geometry.UnknownSections.Count; i++) // different between H3 and ODST or HO
            {
                var dataref = Lbsp.Geometry.UnknownSections[i].Unknown; // different between H3 and ODST or HO

                LbspUnknownSection Section = new LbspUnknownSection();

                if (dataref.Length != 0)
                {
                    using (var outStream = new MemoryStream())
                    using (var outReader = new BinaryReader(outStream))
                    using (var outWriter = new EndianWriter(outStream, EndianFormat.LittleEndian))
                    using (var stream = new MemoryStream(dataref))
                    using (var reader = new EndianReader(stream, EndianFormat.BigEndian))
                    {
                        var dataContext = new DataSerializationContext(reader, null); // at block 2, reader buffer is 0x0's instead of data

                        Section.Headers = new List<LbspUnknownSection.LbspUnknownSectionHeader>();

                        Section.Headers.Add(CacheContext.Deserializer.Deserialize<LbspUnknownSection.LbspUnknownSectionHeader>(dataContext));

                        Section.VertexLists = new LbspUnknownSection.VertexList { Vertex = new List<byte>() };

                        var a = Section.Headers[0];
                        int vertex1size = a.BytesCount;

                        outWriter.Write(a.Unknown00);
                        outWriter.Write(a.Unknown01);
                        outWriter.Write(a.Unknown02);
                        outWriter.Write(a.Unknown03);
                        outWriter.Write(a.Unknown04);
                        outWriter.Write(a.Unknown05);
                        outWriter.Write(a.Unknown06);
                        outWriter.Write(a.Unknown07);
                        outWriter.Write(a.Unknown08);
                        outWriter.Write(a.Unknown09);
                        outWriter.Write(a.Unknown10);
                        outWriter.Write(a.Unknown11);
                        outWriter.Write(a.X);
                        outWriter.Write(a.Y);
                        outWriter.Write(a.Z);
                        outWriter.Write(a.Unknown15);
                        outWriter.Write(a.Unknown16);
                        outWriter.Write(a.BytesCount);
                        outWriter.Write(a.BytesCount2);

                        while (reader.BaseStream.Position < dataref.Length) // read the rest of dataref
                        {
                            try
                            {
                                if (Section.Headers.Count == 2) // remove "wrongfully" added ones
                                    Section.Headers.RemoveAt(1);

                                Section.Headers.Add(CacheContext.Deserializer.Deserialize<LbspUnknownSection.LbspUnknownSectionHeader>(dataContext));

                                if (Section.Headers[0].X == Section.Headers[1].X &
                                    Section.Headers[0].Y == Section.Headers[1].Y &
                                    Section.Headers[0].Z == Section.Headers[1].Z) // if some values match header1, continue
                                {
                                    a = Section.Headers[1];

                                    outWriter.Write(a.Unknown00);
                                    outWriter.Write(a.Unknown01);
                                    outWriter.Write(a.Unknown02);
                                    outWriter.Write(a.Unknown03);
                                    outWriter.Write(a.Unknown04);
                                    outWriter.Write(a.Unknown05);
                                    outWriter.Write(a.Unknown06);
                                    outWriter.Write(a.Unknown07);
                                    outWriter.Write(a.Unknown08);
                                    outWriter.Write(a.Unknown09);
                                    outWriter.Write(a.Unknown10);
                                    outWriter.Write(a.Unknown11);
                                    outWriter.Write(a.X);
                                    outWriter.Write(a.Y);
                                    outWriter.Write(a.Z);
                                    outWriter.Write(a.Unknown15);
                                    outWriter.Write(a.Unknown16);
                                    outWriter.Write(a.BytesCount);
                                    outWriter.Write(a.BytesCount2);

                                    while (reader.BaseStream.Position < dataref.Length)
                                    {
                                        Section.VertexLists.Vertex.Add(reader.ReadByte());
                                        outWriter.Write(Section.VertexLists.Vertex[Section.VertexLists.Vertex.Count - 1]);
                                    }
                                }
                                else // if read data doesn't match, go back and just read 4 bytes
                                {
                                    reader.BaseStream.Position = reader.BaseStream.Position - 0x2C; // if read data doesn't match, go back and serialize 

                                    Section.VertexLists.Vertex.Add(reader.ReadByte());
                                    outWriter.Write(Section.VertexLists.Vertex[Section.VertexLists.Vertex.Count - 1]);

                                    Section.VertexLists.Vertex.Add(reader.ReadByte());
                                    outWriter.Write(Section.VertexLists.Vertex[Section.VertexLists.Vertex.Count - 1]);

                                    Section.VertexLists.Vertex.Add(reader.ReadByte());
                                    outWriter.Write(Section.VertexLists.Vertex[Section.VertexLists.Vertex.Count - 1]);

                                    Section.VertexLists.Vertex.Add(reader.ReadByte());
                                    outWriter.Write(Section.VertexLists.Vertex[Section.VertexLists.Vertex.Count - 1]);
                                }
                            }
                            catch (Exception) { }
                        }
                        // Write back to tag
                        outStream.Position = 0;

                        Lbsp.Geometry.UnknownSections[i].Unknown = outReader.ReadBytes((int)outStream.Length);
                    }
                }
                else // some tagblocks are empty
                    continue;
            }

            //
            // Fix Unknown byte[]
            //

            var tagblocks = Lbsp.Geometry.Unknown2;
            for (int i = 0; i < tagblocks.Count; i++) // different between H3 and ODST or HO
            {
                var dataref = tagblocks[i].Unknown3; // different between H3 and ODST or HO

                if (dataref.Length != 0)
                {
                    using (var outStream = new MemoryStream())
                    using (var outReader = new BinaryReader(outStream))
                    using (var outWriter = new EndianWriter(outStream, EndianFormat.LittleEndian))
                    using (var stream = new MemoryStream(dataref))
                    using (var reader = new EndianReader(stream, EndianFormat.BigEndian))
                    {
                        var dataContext = new DataSerializationContext(reader, null); // at block 2, reader buffer is 0x0's instead of data

                        var ints = new List<UInt32>();

                        while (reader.BaseStream.Position < dataref.Length) // read the rest of dataref
                        {
                            try
                            {
                                outWriter.Write(reader.ReadUInt32());
                            }
                            catch (Exception) { }
                        }
                        // Write back to tag
                        outStream.Position = 0;

                        Lbsp.Geometry.Unknown2[i].Unknown3 = outReader.ReadBytes((int)outStream.Length);
                    }
                }
                else // some tagblocks are empty
                    continue;
            }

            //
            // Set bitm
            //

            Lbsp.PrimaryMap = CacheContext.GetTag(0x2E88);
            Lbsp.IntensityMap = CacheContext.GetTag(0x2E89);

            // blamMode.PrimaryMap = CacheContext.GetTag(0x0343);
            // blamMode.IntensityMap = CacheContext.GetTag(0x0343);

            for (int i = 0; i < Lbsp.Geometry.Meshes.Count; i++)
            {
                Lbsp.Geometry.Meshes[i].PrtType = PrtType.None;
                // blamMode.Geometry.Meshes[i].VertexBuffers[0] = 65535;
                // blamMode.Geometry.Meshes[i].VertexBuffers[1] = 65535;
                // blamMode.Geometry.Meshes[i].VertexBuffers[2] = 65535;
                // blamMode.Geometry.Meshes[i].VertexBuffers[3] = 65535;
                // blamMode.Geometry.Meshes[i].VertexBuffers[4] = 65535;
                // blamMode.Geometry.Meshes[i].VertexBuffers[5] = 65535;
                // blamMode.Geometry.Meshes[i].VertexBuffers[6] = 65535;
                // blamMode.Geometry.Meshes[i].VertexBuffers[7] = 65535;
            }

            // Fix lightning
            Lbsp.Shadows = 4;
            Lbsp.Midtones = 1;
            Lbsp.Highlights = 1;
            Lbsp.TopDownWhites = 1;
            Lbsp.TopDownBlacks = 1;

            //
            // Finalize new ElDorado scenario_structure_bsp tag
            //

            CacheContext.TagNames[newTag.Index] = blamTagName;

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.scenario_lightmap_bsp_data\" tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, Lbsp);
            }

            //
            // Done!
            //

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.scenario_lightmap_bsp_data\" successfully!");

            return newTag;
        }

        private CachedTagInstance PortShader(string blamTagName, CachedTagInstance edTag = null)
        {
            #region Main
            var isNew = (edTag == null);

            //
            // Verify Blam scenario_structure_bsp tag
            //

            Console.Write("Verifying Blam shader tag...");

            CacheFile.IndexItem blamTag = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ParentClass == "rm" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam shader tag does not exist: " + blamTagName);
                return null;
            }

            Console.WriteLine("Done.");

            //
            // Verify ED shader tag
            //

            if (!isNew)
            {
                Console.Write("Verifying ElDorado shader tag index...");

                if (edTag.Group.Name != CacheContext.GetStringId("shader"))
                {
                    Console.WriteLine($"Specified tag index is not a shader: 0x{edTag.Index:X4}");
                    return null;
                }

                Console.WriteLine("Done.");
            }

            //
            // Load Blam scenario_structure_bsp tag
            //

            Console.Write("Loading Blam shader tag...");

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var shader = blamDeserializer.Deserialize<Shader>(blamContext);

            Console.WriteLine("Done.");


            var shaderName = blamTagName;

            CacheFile.IndexItem item = null;

            Console.WriteLine("Verifying Blam shader tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if ((tag.ParentClass == "rm") && tag.Filename == shaderName)
                {
                    item = tag;
                    break;
                }
            }

            if (item == null)
            {
                Console.WriteLine("Blam shader tag does not exist: " + shaderName);
                return CacheContext.GetTag(0x101F);
            }

            CachedTagInstance newRmsh = CacheContext.GetTag(0x101F);
            #endregion
            try
            {
                var BlamShader = new BlamCore.Legacy.Halo3Beta.shader(BlamCache, item.Offset);

                var templateItem = BlamCache.IndexItems.Find(i =>
                    i.ID == BlamShader.Properties[0].TemplateTagID);

                var template = new BlamCore.Legacy.Halo3Beta.render_method_template(BlamCache, templateItem.Offset);

                //
                // Determine the Blam shader's base bitmap
                //

                var bitmapIndex = -1;
                var bitmapArgName = "";

                for (var i = 0; i < template.UsageBlocks.Count; i++)
                {
                    var entry = template.UsageBlocks[i];

                    if (entry.Usage.StartsWith("base_map") ||
                        entry.Usage.StartsWith("diffuse_map") ||
                        entry.Usage == "foam_texture")
                    {
                        bitmapIndex = i;
                        bitmapArgName = entry.Usage;
                        break;
                    }
                }

                //
                // Load and decode the Blam shader's base bitmap
                //

                var bitmItem = BlamCache.IndexItems.Find(i =>
                    i.ID == BlamShader.Properties[0].ShaderMaps[bitmapIndex].BitmapTagID);

                var bitm = new BlamCore.Legacy.Halo3Beta.bitmap(BlamCache, bitmItem.Offset);

                var submap = bitm.Bitmaps[0];

                byte[] raw;

                if (bitm.RawChunkBs.Count > 0)
                {
                    int rawID = bitm.RawChunkBs[submap.InterleavedIndex].RawID;
                    byte[] buffer = BlamCache.GetRawFromID(rawID);
                    raw = new byte[submap.RawSize];
                    Array.Copy(buffer, submap.Index2 * submap.RawSize, raw, 0, submap.RawSize);
                }
                else
                {
                    int rawID = bitm.RawChunkAs[0].RawID;
                    raw = BlamCache.GetRawFromID(rawID, submap.RawSize);
                }

                var vHeight = submap.VirtualHeight;
                var vWidth = submap.VirtualWidth;

                var ms = new MemoryStream();
                var bw = new BinaryWriter(ms);

                if (submap.Flags.Values[3])
                    raw = DxtDecoder.ConvertToLinearTexture(raw, vWidth, vHeight, submap.Format);

                if (submap.Format != BitmapFormat.A8R8G8B8)
                    for (int i = 0; i < raw.Length; i += 2)
                        Array.Reverse(raw, i, 2);
                else
                    for (int i = 0; i < (raw.Length); i += 4)
                        Array.Reverse(raw, i, 4);

                new DdsImage(submap).Write(bw);
                bw.Write(raw);

                raw = ms.ToArray();

                bw.Close();
                bw.Dispose();

                //
                // ElDorado Serialization
                //

                using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
                {
                    //
                    // Create the new ElDorado bitmap
                    //

                    var newBitm = CacheContext.TagCache.DuplicateTag(cacheStream, CacheContext.GetTag(0x101F));

                    var bitmap = new Bitmap
                    {
                        Flags = Bitmap.RuntimeFlags.UseResource,
                        Sequences = new List<Bitmap.Sequence>
                    {
                        new Bitmap.Sequence
                        {
                            Name = "",
                            FirstBitmapIndex = 0,
                            BitmapCount = 1
                        }
                    },
                        Images = new List<Bitmap.Image>
                    {
                        new Bitmap.Image
                        {
                            Signature = new Tag("bitm").Value,
                            Unknown28 = -1
                        }
                    },
                        Resources = new List<Bitmap.BitmapResource>
                    {
                        new Bitmap.BitmapResource()
                    }
                    };

                    using (var imageStream = new MemoryStream(raw))
                    {
                        var injector = new BitmapDdsInjector(CacheContext);
                        imageStream.Seek(0, SeekOrigin.Begin);
                        injector.InjectDds(CacheContext.Serializer, CacheContext.Deserializer, bitmap, 0, imageStream);
                    }

                    var context = new TagSerializationContext(cacheStream, CacheContext, newBitm);
                    CacheContext.Serializer.Serialize(context, bitmap);

                    //
                    // Create the new ElDorado shader
                    //

                    newRmsh = CacheContext.TagCache.DuplicateTag(cacheStream, CacheContext.GetTag(0x101F));
                    context = new TagSerializationContext(cacheStream, CacheContext, newRmsh);
                    shader = CacheContext.Deserializer.Deserialize<Shader>(context);

                    shader.ShaderProperties[0].ShaderMaps[0].Bitmap = newBitm;

                    CacheContext.Serializer.Serialize(context, shader);

                    Console.WriteLine("Done! New shader tag is 0x" + newRmsh.Index.ToString("X8"));
                }
            }
            catch (Exception)
            {
                using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
                    newRmsh = CacheContext.TagCache.DuplicateTag(cacheStream, CacheContext.GetTag(0x101F)); // if anything fails, just use shaders\invalid
            }

            //
            // Finalize new ElDorado shader tag
            //

            CacheContext.TagNames[newRmsh.Index] = blamTagName;

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newRmsh.Index]}.shader\" successfully!");

            return newRmsh;

        }

        private void ConvertVertices<T>(int count, Func<T> readVertex, Action<T> writeVertex)
        {
            for (var i = 0; i < count; i++)
                writeVertex(readVertex());
        }

        private CachedTagInstance ConvertTagReference(int index)
        {
            var instance = BlamCache.IndexItems.Find(i => i.ID == index);

            if (instance != null)
            {
                var chars = new char[] { ' ', ' ', ' ', ' ' };
                for (var i = 0; i < instance.ClassCode.Length; i++) chars[i] = instance.ClassCode[i];

                var tags = CacheContext.TagCache.Index.FindAllInGroup(new string(chars));

                foreach (var tag in tags)
                {
                    if (instance.Filename == CacheContext.TagNames[tag.Index])
                        return tag;
                }
            }

            return null;
        }

    }
}
