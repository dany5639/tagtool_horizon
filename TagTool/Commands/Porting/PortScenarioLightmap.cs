using System;
using System.Collections.Generic;
using BlamCore.Cache;
using BlamCore.Legacy.Base;
using BlamCore.Common;
using BlamCore.TagDefinitions;

namespace TagTool.Commands.Porting
{
    class PortScenarioLightmap : Command
    {
        private GameCacheContext CacheContext { get; }
        private CacheFile BlamCache { get; }

        public PortScenarioLightmap(GameCacheContext cacheContext, CacheFile blamCache)
            : base(CommandFlags.Inherit,

                  "PortScenarioLightmap",
                  "Ports a Blam scenario_lightmap_bsp_data tag to the current cache.",

                  "PortScenarioLightmap [New <Blam Tag>] | [<Blam Tag> <ElDorado Tag>]",

                  "Ports a Blam scenario_lightmap_bsp_data tag to the current cache.")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        private void ConvertVertices<T>(int count, Func<T> readVertex, Action<T> writeVertex)
        {
            for (var i = 0; i < count; i++)
                writeVertex(readVertex());
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count < 2 || args.Count > 3)
                return false;

            bool isNew = false;
            if (args[0].ToLower() == "new")
            {
                isNew = true;
                args.RemoveAt(0);
            }

            var initialStringIDCount = CacheContext.StringIdCache.Strings.Count;

            //
            // Verify Blam model tag
            //

            var blamTagName = args[0];

            CacheFile.IndexItem blamTag = null;

            Console.WriteLine("Verifying Blam scenario_lightmap tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "sLdT" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam scenario_lightmap tag does not exist: " + args[0]);
                return false;
            }

            //
            // Verify ED scenario_lightmap tag
            //

            CachedTagInstance edTag = null;

            if (!isNew)
            {
                Console.Write("Verifying ElDorado scenario_structure_bsp tag index...");

                edTag = ArgumentParser.ParseTagSpecifier(CacheContext, args[1]);

                if (edTag.Group.Name != CacheContext.GetStringId("scenario_lightmap"))
                {
                    Console.WriteLine($"Specified tag index is not a scenario_structure_bsp: 0x{edTag.Index:X4}");
                    return false;
                }

                Console.WriteLine("done.");
            }

            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating new ElDorado scenario_lightmap tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("sLdT")]);

                Console.WriteLine("done.");
            }
            else
            {
                newTag = edTag;
            }

            CacheContext.TagNames[newTag.Index] = blamTagName;

            //
            // Load Blam model tag
            //

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamHlmt = blamDeserializer.Deserialize<ScenarioLightmap>(blamContext);

            //
            // Update Tag Refs
            //

            foreach (var a in blamHlmt.LightmapDataReferences)
            {
                a.LightmapData = ConvertTagReference(a.LightmapData.Index);
            }
            
            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.scenario_lightmap\" tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, blamHlmt);
            }

            //
            // Save new string_ids
            //

            if (CacheContext.StringIdCache.Strings.Count != initialStringIDCount)
            {
                Console.Write("Saving string_ids...");

                using (var stringIdStream = CacheContext.OpenStringIdCacheReadWrite())
                    CacheContext.StringIdCache.Save(stringIdStream);

                Console.WriteLine("done.");
            }

            //
            // Done!
            //

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.scenario_lightmap\" successfully!");

            return true;
        }

        private CachedTagInstance ConvertTagReference(int index)
        {
            var instance = BlamCache.IndexItems.Find(i => i.ID == index);

            if (instance != null)
            {
                var chars = new char[] { ' ', ' ', ' ', ' ' };
                for (var i = 0; i < instance.ClassCode.Length; i++)
                    chars[i] = instance.ClassCode[i];

                var tags = CacheContext.TagCache.Index.FindAllInGroup(new string(chars));

                foreach (var tag in tags)
                {
                    if (instance.Filename == CacheContext.TagNames[tag.Index])
                        return tag;
                }
            }

            return null;
        }
    }
}