﻿using System;
using System.Collections.Generic;
using System.IO;
using BlamCore.Legacy.Base;
using BlamCore.Cache;
using BlamCore.TagDefinitions;
using BlamCore.Common;
using BlamCore.Geometry;
using BlamCore.TagResources;
using BlamCore.IO;
using BlamCore.Serialization;
using BlamCore.Bitmaps;

namespace TagTool.Commands.Porting
{
    class PortModelCommand : Command
    {
        public GameCacheContext CacheContext { get; }
        public CacheFile BlamCache { get; }

        public PortModelCommand(GameCacheContext cacheContext, CacheFile blamCache)
            : base(CommandFlags.None,

                  "PortModel",
                  "Ports a Blam model tag to current cache.",

                  "PortModel [New <Blam Tag>] | [<Blam Tag> <ElDorado Tag>]",

                  "Ports a Blam model tag to current cache.")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count < 2 || args.Count > 3)
                return false;

            bool isNew = false;
            if (args[0].ToLower() == "new")
            {
                isNew = true;
                args.RemoveAt(0);
            }

            var initialStringIDCount = CacheContext.StringIdCache.Strings.Count;

            //
            // Verify Blam model tag
            //

            var blamTagName = args[0];

            CacheFile.IndexItem blamTag = null;

            Console.WriteLine("Verifying Blam model tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "hlmt" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam model tag does not exist: " + args[0]);
                return false;
            }

            //
            // Verify ED model tag
            //

            CachedTagInstance edTag = null;
            Model edHlmt = null;

            if (!isNew)
            {
                Console.WriteLine("Verifying ElDorado model tag index...");

                edTag = ArgumentParser.ParseTagSpecifier(CacheContext, args[1]);

                if (edTag.Group.Name != CacheContext.GetStringId("model"))
                {
                    Console.WriteLine("Specified tag index is not a model: " + args[1]);
                    return false;
                }

                //
                // Load ElDorado model tag
                //

                using (var cacheStream = CacheContext.OpenTagCacheRead())
                {
                    var context = new TagSerializationContext(cacheStream, CacheContext, edTag);
                    edHlmt = CacheContext.Deserializer.Deserialize<Model>(context);
                }
            }

            //
            // Load Blam model tag
            //

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamHlmt = blamDeserializer.Deserialize<Model>(blamContext);

            blamHlmt.RenderModel = PortRenderModel(BlamCache.IndexItems.Find(i => i.ID == blamHlmt.RenderModel.Index).Filename, isNew ? null : edHlmt.RenderModel);
            blamHlmt.CollisionModel = PortCollisionModel(BlamCache.IndexItems.Find(i => i.ID == blamHlmt.CollisionModel.Index).Filename, isNew ? null : edHlmt.CollisionModel);
            blamHlmt.Animation = PortModelAnimationGraph(BlamCache.IndexItems.Find(i => i.ID == blamHlmt.Animation.Index).Filename, isNew ? null : edHlmt.Animation);
            blamHlmt.PhysicsModel = PortPhysicsModel(BlamCache.IndexItems.Find(i => i.ID == blamHlmt.PhysicsModel.Index).Filename, isNew ? null : edHlmt.PhysicsModel);
            blamHlmt.LodModel = null;

            foreach (var variant in blamHlmt.Variants)
            {
                var variantName = BlamCache.Strings.GetItemByID((int)variant.Name.Value);
                if (variantName == "<blank>") variantName = "";
                variant.Name = CacheContext.StringIdCache.Contains(variantName) ?
                    CacheContext.StringIdCache.GetStringId(variantName) :
                    CacheContext.StringIdCache.AddString(variantName);

                foreach (var region in variant.Regions)
                {
                    var regionName = BlamCache.Strings.GetItemByID((int)region.Name.Value);
                    if (regionName == "<blank>") regionName = "";
                    region.Name = CacheContext.StringIdCache.Contains(regionName) ?
                        CacheContext.StringIdCache.GetStringId(regionName) :
                        CacheContext.StringIdCache.AddString(regionName);

                    foreach (var permutation in region.Permutations)
                    {
                        var permutationName = BlamCache.Strings.GetItemByID((int)permutation.Name.Value);
                        if (permutationName == "<blank>") permutationName = "";
                        permutation.Name = CacheContext.StringIdCache.Contains(permutationName) ?
                            CacheContext.StringIdCache.GetStringId(permutationName) :
                            CacheContext.StringIdCache.AddString(permutationName);

                        foreach (var state in permutation.States)
                        {
                            var stateName = BlamCache.Strings.GetItemByID((int)state.Name.Value);
                            if (stateName == "<blank>") stateName = "";
                            state.Name = CacheContext.StringIdCache.Contains(stateName) ?
                                CacheContext.StringIdCache.GetStringId(stateName) :
                                CacheContext.StringIdCache.AddString(stateName);

                            state.LoopingEffect = null;

                            var stateLoopingEffectMarkerName = BlamCache.Strings.GetItemByID((int)state.LoopingEffectMarkerName.Value);
                            if (stateLoopingEffectMarkerName == "<blank>") stateLoopingEffectMarkerName = "";
                            state.LoopingEffectMarkerName = CacheContext.StringIdCache.Contains(stateLoopingEffectMarkerName) ?
                                CacheContext.StringIdCache.GetStringId(stateLoopingEffectMarkerName) :
                                CacheContext.StringIdCache.AddString(stateLoopingEffectMarkerName);
                        }
                    }
                }

                foreach (var obj in variant.Objects)
                {
                    var objParentMarker = BlamCache.Strings.GetItemByID((int)obj.ParentMarker.Value);
                    if (objParentMarker == "<blank>") objParentMarker = "";
                    obj.ParentMarker = CacheContext.StringIdCache.Contains(objParentMarker) ?
                        CacheContext.StringIdCache.GetStringId(objParentMarker) :
                        CacheContext.StringIdCache.AddString(objParentMarker);

                    var objChildMarker = BlamCache.Strings.GetItemByID((int)obj.ChildMarker.Value);
                    if (objChildMarker == "<blank>") objChildMarker = "";
                    obj.ChildMarker = CacheContext.StringIdCache.Contains(objChildMarker) ?
                        CacheContext.StringIdCache.GetStringId(objChildMarker) :
                        CacheContext.StringIdCache.AddString(objChildMarker);

                    var objChildVariant = BlamCache.Strings.GetItemByID((int)obj.ChildVariant.Value);
                    if (objChildVariant == "<blank>") objChildVariant = "";
                    obj.ChildVariant = CacheContext.StringIdCache.Contains(objChildVariant) ?
                        CacheContext.StringIdCache.GetStringId(objChildVariant) :
                        CacheContext.StringIdCache.AddString(objChildVariant);

                    obj.ChildObject = null;
                }
            }

            foreach (var instanceGroup in blamHlmt.InstanceGroups)
            {
                var instanceGroupName = BlamCache.Strings.GetItemByID((int)instanceGroup.Name.Value);
                if (instanceGroupName == "<blank>") instanceGroupName = "";
                instanceGroup.Name = CacheContext.StringIdCache.Contains(instanceGroupName) ?
                    CacheContext.StringIdCache.GetStringId(instanceGroupName) :
                    CacheContext.StringIdCache.AddString(instanceGroupName);

                foreach (var instanceMember in instanceGroup.InstanceMembers)
                {
                    var instanceMemberName = BlamCache.Strings.GetItemByID((int)instanceMember.InstanceName.Value);
                    if (instanceMemberName == "<blank>") instanceMemberName = "";
                    instanceMember.InstanceName = CacheContext.StringIdCache.Contains(instanceMemberName) ?
                        CacheContext.StringIdCache.GetStringId(instanceMemberName) :
                        CacheContext.StringIdCache.AddString(instanceMemberName);
                }
            }

            foreach (var material in blamHlmt.Materials)
            {
                var materialName = BlamCache.Strings.GetItemByID((int)material.Name.Value);
                if (materialName == "<blank>") materialName = "";
                material.Name = CacheContext.StringIdCache.Contains(materialName) ?
                    CacheContext.StringIdCache.GetStringId(materialName) :
                    CacheContext.StringIdCache.AddString(materialName);

                var materialMaterialName = BlamCache.Strings.GetItemByID((int)material.MaterialName.Value);
                if (materialMaterialName == "<blank>") materialMaterialName = "";
                material.MaterialName = CacheContext.StringIdCache.Contains(materialMaterialName) ?
                    CacheContext.StringIdCache.GetStringId(materialMaterialName) :
                    CacheContext.StringIdCache.AddString(materialMaterialName);
            }

            foreach (var newDamageInfo in blamHlmt.NewDamageInfo)
            {
                var newDamageInfoGlobalIndirectMaterialName = BlamCache.Strings.GetItemByID((int)newDamageInfo.GlobalIndirectMaterialName.Value);
                if (newDamageInfoGlobalIndirectMaterialName == "<blank>") newDamageInfoGlobalIndirectMaterialName = "";
                newDamageInfo.GlobalIndirectMaterialName = CacheContext.StringIdCache.Contains(newDamageInfoGlobalIndirectMaterialName) ?
                    CacheContext.StringIdCache.GetStringId(newDamageInfoGlobalIndirectMaterialName) :
                    CacheContext.StringIdCache.AddString(newDamageInfoGlobalIndirectMaterialName);

                var newDamageInfoGlobalShieldMaterialName = BlamCache.Strings.GetItemByID((int)newDamageInfo.GlobalShieldMaterialName.Value);
                if (newDamageInfoGlobalShieldMaterialName == "<blank>") newDamageInfoGlobalShieldMaterialName = "";
                newDamageInfo.GlobalShieldMaterialName = CacheContext.StringIdCache.Contains(newDamageInfoGlobalShieldMaterialName) ?
                    CacheContext.StringIdCache.GetStringId(newDamageInfoGlobalShieldMaterialName) :
                    CacheContext.StringIdCache.AddString(newDamageInfoGlobalShieldMaterialName);

                newDamageInfo.ShieldDamagedEffect = null;
                newDamageInfo.ShieldDepletedEffect = null;
                newDamageInfo.ShieldRechargingEffect = null;

                foreach (var damageSection in newDamageInfo.DamageSections)
                {
                    var damageSectionName = BlamCache.Strings.GetItemByID((int)damageSection.Name.Value);
                    if (damageSectionName == "<blank>") damageSectionName = "";
                    damageSection.Name = CacheContext.StringIdCache.Contains(damageSectionName) ?
                        CacheContext.StringIdCache.GetStringId(damageSectionName) :
                        CacheContext.StringIdCache.AddString(damageSectionName);

                    foreach (var instantResponse in damageSection.InstantResponses)
                    {
                        var instantResponseTrigger = BlamCache.Strings.GetItemByID((int)instantResponse.Trigger.Value);
                        if (instantResponseTrigger == "<blank>") instantResponseTrigger = "";
                        instantResponse.Trigger = CacheContext.StringIdCache.Contains(instantResponseTrigger) ?
                            CacheContext.StringIdCache.GetStringId(instantResponseTrigger) :
                            CacheContext.StringIdCache.AddString(instantResponseTrigger);

                        instantResponse.PrimaryTransitionEffect = null;
                        instantResponse.SecondaryTransitionEffect = null;
                        instantResponse.TransitionDamageEffect = null;

                        var instantResponseRegion = BlamCache.Strings.GetItemByID((int)instantResponse.Region.Value);
                        if (instantResponseRegion == "<blank>") instantResponseRegion = "";
                        instantResponse.Region = CacheContext.StringIdCache.Contains(instantResponseRegion) ?
                            CacheContext.StringIdCache.GetStringId(instantResponseRegion) :
                            CacheContext.StringIdCache.AddString(instantResponseRegion);

                        var instantResponseSecondaryRegion = BlamCache.Strings.GetItemByID((int)instantResponse.SecondaryRegion.Value);
                        if (instantResponseSecondaryRegion == "<blank>") instantResponseSecondaryRegion = "";
                        instantResponse.SecondaryRegion = CacheContext.StringIdCache.Contains(instantResponseSecondaryRegion) ?
                            CacheContext.StringIdCache.GetStringId(instantResponseSecondaryRegion) :
                            CacheContext.StringIdCache.AddString(instantResponseSecondaryRegion);

                        var instantResponseSpecialDamageCase = BlamCache.Strings.GetItemByID((int)instantResponse.SpecialDamageCase.Value);
                        if (instantResponseSpecialDamageCase == "<blank>") instantResponseSpecialDamageCase = "";
                        instantResponse.SpecialDamageCase = CacheContext.StringIdCache.Contains(instantResponseSpecialDamageCase) ?
                            CacheContext.StringIdCache.GetStringId(instantResponseSpecialDamageCase) :
                            CacheContext.StringIdCache.AddString(instantResponseSpecialDamageCase);

                        var instantResponseEffectMarkerName = BlamCache.Strings.GetItemByID((int)instantResponse.EffectMarkerName.Value);
                        if (instantResponseEffectMarkerName == "<blank>") instantResponseEffectMarkerName = "";
                        instantResponse.EffectMarkerName = CacheContext.StringIdCache.Contains(instantResponseEffectMarkerName) ?
                            CacheContext.StringIdCache.GetStringId(instantResponseEffectMarkerName) :
                            CacheContext.StringIdCache.AddString(instantResponseEffectMarkerName);

                        var instantResponseDamageEffectMarkerName = BlamCache.Strings.GetItemByID((int)instantResponse.DamageEffectMarkerName.Value);
                        if (instantResponseDamageEffectMarkerName == "<blank>") instantResponseDamageEffectMarkerName = "";
                        instantResponse.DamageEffectMarkerName = CacheContext.StringIdCache.Contains(instantResponseDamageEffectMarkerName) ?
                            CacheContext.StringIdCache.GetStringId(instantResponseDamageEffectMarkerName) :
                            CacheContext.StringIdCache.AddString(instantResponseDamageEffectMarkerName);

                        instantResponse.DelayEffect = null;

                        var instantResponseDelayEffectMarkerName = BlamCache.Strings.GetItemByID((int)instantResponse.DelayEffectMarkerName.Value);
                        if (instantResponseDelayEffectMarkerName == "<blank>") instantResponseDelayEffectMarkerName = "";
                        instantResponse.DelayEffectMarkerName = CacheContext.StringIdCache.Contains(instantResponseDelayEffectMarkerName) ?
                            CacheContext.StringIdCache.GetStringId(instantResponseDelayEffectMarkerName) :
                            CacheContext.StringIdCache.AddString(instantResponseDelayEffectMarkerName);

                        var instantResponseEjectingSeatLabel = BlamCache.Strings.GetItemByID((int)instantResponse.EjectingSeatLabel.Value);
                        if (instantResponseEjectingSeatLabel == "<blank>") instantResponseEjectingSeatLabel = "";
                        instantResponse.EjectingSeatLabel = CacheContext.StringIdCache.Contains(instantResponseEjectingSeatLabel) ?
                            CacheContext.StringIdCache.GetStringId(instantResponseEjectingSeatLabel) :
                            CacheContext.StringIdCache.AddString(instantResponseEjectingSeatLabel);

                        var instantResponseDestroyedChildObjectMarkerName = BlamCache.Strings.GetItemByID((int)instantResponse.DestroyedChildObjectMarkerName.Value);
                        if (instantResponseDestroyedChildObjectMarkerName == "<blank>") instantResponseDestroyedChildObjectMarkerName = "";
                        instantResponse.DestroyedChildObjectMarkerName = CacheContext.StringIdCache.Contains(instantResponseDestroyedChildObjectMarkerName) ?
                            CacheContext.StringIdCache.GetStringId(instantResponseDestroyedChildObjectMarkerName) :
                            CacheContext.StringIdCache.AddString(instantResponseDestroyedChildObjectMarkerName);

                    }

                    var damageSectionResurrectionRegionName = BlamCache.Strings.GetItemByID((int)damageSection.ResurrectionRegionName.Value);
                    if (damageSectionResurrectionRegionName == "<blank>") damageSectionResurrectionRegionName = "";
                    damageSection.ResurrectionRegionName = CacheContext.StringIdCache.Contains(damageSectionResurrectionRegionName) ?
                        CacheContext.StringIdCache.GetStringId(damageSectionResurrectionRegionName) :
                        CacheContext.StringIdCache.AddString(damageSectionResurrectionRegionName);
                }

                foreach (var damageSeat in newDamageInfo.DamageSeats)
                {
                    var damageSeatSeatLabel = BlamCache.Strings.GetItemByID((int)damageSeat.SeatLabel.Value);
                    if (damageSeatSeatLabel == "<blank>") damageSeatSeatLabel = "";
                    damageSeat.SeatLabel = CacheContext.StringIdCache.Contains(damageSeatSeatLabel) ?
                        CacheContext.StringIdCache.GetStringId(damageSeatSeatLabel) :
                        CacheContext.StringIdCache.AddString(damageSeatSeatLabel);

                    foreach (var unknown in damageSeat.Unknown)
                    {
                        var unknownNode = BlamCache.Strings.GetItemByID((int)unknown.Node.Value);
                        if (unknownNode == "<blank>") unknownNode = "";
                        unknown.Node = CacheContext.StringIdCache.Contains(unknownNode) ?
                            CacheContext.StringIdCache.GetStringId(unknownNode) :
                            CacheContext.StringIdCache.AddString(unknownNode);
                    }
                }

                foreach (var damageConstraint in newDamageInfo.DamageConstraints)
                {
                    var damageConstraintPhysicsModelConstraintName = BlamCache.Strings.GetItemByID((int)damageConstraint.PhysicsModelConstraintName.Value);
                    if (damageConstraintPhysicsModelConstraintName == "<blank>") damageConstraintPhysicsModelConstraintName = "";
                    damageConstraint.PhysicsModelConstraintName = CacheContext.StringIdCache.Contains(damageConstraintPhysicsModelConstraintName) ?
                        CacheContext.StringIdCache.GetStringId(damageConstraintPhysicsModelConstraintName) :
                        CacheContext.StringIdCache.AddString(damageConstraintPhysicsModelConstraintName);

                    var damageConstraintDamageConstraintName = BlamCache.Strings.GetItemByID((int)damageConstraint.DamageConstraintName.Value);
                    if (damageConstraintDamageConstraintName == "<blank>") damageConstraintDamageConstraintName = "";
                    damageConstraint.DamageConstraintName = CacheContext.StringIdCache.Contains(damageConstraintDamageConstraintName) ?
                        CacheContext.StringIdCache.GetStringId(damageConstraintDamageConstraintName) :
                        CacheContext.StringIdCache.AddString(damageConstraintDamageConstraintName);

                    var damageConstraintDamageConstraintGroupName = BlamCache.Strings.GetItemByID((int)damageConstraint.DamageConstraintGroupName.Value);
                    if (damageConstraintDamageConstraintGroupName == "<blank>") damageConstraintDamageConstraintGroupName = "";
                    damageConstraint.DamageConstraintGroupName = CacheContext.StringIdCache.Contains(damageConstraintDamageConstraintGroupName) ?
                        CacheContext.StringIdCache.GetStringId(damageConstraintDamageConstraintGroupName) :
                        CacheContext.StringIdCache.AddString(damageConstraintDamageConstraintGroupName);

                }
            }

            foreach (var target in blamHlmt.Targets)
            {
                var targetMarkerName = BlamCache.Strings.GetItemByID((int)target.MarkerName.Value);
                if (targetMarkerName == "<blank>") targetMarkerName = "";
                target.MarkerName = CacheContext.StringIdCache.Contains(targetMarkerName) ?
                    CacheContext.StringIdCache.GetStringId(targetMarkerName) :
                    CacheContext.StringIdCache.AddString(targetMarkerName);
            }

            foreach (var collisionRegion in blamHlmt.CollisionRegions)
            {
                var collisionRegionName = BlamCache.Strings.GetItemByID((int)collisionRegion.Name.Value);
                if (collisionRegionName == "<blank>") collisionRegionName = "";
                collisionRegion.Name = CacheContext.StringIdCache.Contains(collisionRegionName) ?
                    CacheContext.StringIdCache.GetStringId(collisionRegionName) :
                    CacheContext.StringIdCache.AddString(collisionRegionName);

                foreach (var permutation in collisionRegion.Permutations)
                {
                    var permutationName = BlamCache.Strings.GetItemByID((int)permutation.Name.Value);
                    if (permutationName == "<blank>") permutationName = "";
                    permutation.Name = CacheContext.StringIdCache.Contains(permutationName) ?
                        CacheContext.StringIdCache.GetStringId(permutationName) :
                        CacheContext.StringIdCache.AddString(permutationName);
                }
            }

            foreach (var node in blamHlmt.Nodes)
            {
                var nodeName = BlamCache.Strings.GetItemByID((int)node.Name.Value);
                if (nodeName == "<blank>") nodeName = "";
                node.Name = CacheContext.StringIdCache.Contains(nodeName) ?
                    CacheContext.StringIdCache.GetStringId(nodeName) :
                    CacheContext.StringIdCache.AddString(nodeName);
            }

            blamHlmt.PrimaryDialogue = null;
            blamHlmt.SecondaryDialogue = null;

            var blamHlmtDefaultDialogueEffect = BlamCache.Strings.GetItemByID((int)blamHlmt.DefaultDialogueEffect.Value);
            if (blamHlmtDefaultDialogueEffect == "<blank>") blamHlmtDefaultDialogueEffect = "";
            blamHlmt.DefaultDialogueEffect = CacheContext.StringIdCache.Contains(blamHlmtDefaultDialogueEffect) ?
                CacheContext.StringIdCache.GetStringId(blamHlmtDefaultDialogueEffect) :
                CacheContext.StringIdCache.AddString(blamHlmtDefaultDialogueEffect);

            foreach (var unknown2 in blamHlmt.Unknown6)
            {
                var unknown2Region = BlamCache.Strings.GetItemByID((int)unknown2.Region.Value);
                if (blamHlmtDefaultDialogueEffect == "<blank>") blamHlmtDefaultDialogueEffect = "";
                unknown2.Region = CacheContext.StringIdCache.Contains(unknown2Region) ?
                    CacheContext.StringIdCache.GetStringId(unknown2Region) :
                    CacheContext.StringIdCache.AddString(unknown2Region);

                var unknown2Permutation = BlamCache.Strings.GetItemByID((int)unknown2.Permutation.Value);
                if (blamHlmtDefaultDialogueEffect == "<blank>") blamHlmtDefaultDialogueEffect = "";
                unknown2.Permutation = CacheContext.StringIdCache.Contains(unknown2Permutation) ?
                    CacheContext.StringIdCache.GetStringId(unknown2Permutation) :
                    CacheContext.StringIdCache.AddString(unknown2Permutation);
            }

            foreach (var unknown3 in blamHlmt.Unknown7)
            {
                var unknown3Unknown = BlamCache.Strings.GetItemByID((int)unknown3.Unknown.Value);
                if (blamHlmtDefaultDialogueEffect == "<blank>") blamHlmtDefaultDialogueEffect = "";
                unknown3.Unknown = CacheContext.StringIdCache.Contains(unknown3Unknown) ?
                    CacheContext.StringIdCache.GetStringId(unknown3Unknown) :
                    CacheContext.StringIdCache.AddString(unknown3Unknown);
            }

            foreach (var unknown4 in blamHlmt.Unknown8)
            {
                var unknown4Marker = BlamCache.Strings.GetItemByID((int)unknown4.Marker.Value);
                if (blamHlmtDefaultDialogueEffect == "<blank>") blamHlmtDefaultDialogueEffect = "";
                unknown4.Marker = CacheContext.StringIdCache.Contains(unknown4Marker) ?
                    CacheContext.StringIdCache.GetStringId(unknown4Marker) :
                    CacheContext.StringIdCache.AddString(unknown4Marker);

                var unknown4Marker2 = BlamCache.Strings.GetItemByID((int)unknown4.Marker2.Value);
                if (blamHlmtDefaultDialogueEffect == "<blank>") blamHlmtDefaultDialogueEffect = "";
                unknown4.Marker2 = CacheContext.StringIdCache.Contains(unknown4Marker2) ?
                    CacheContext.StringIdCache.GetStringId(unknown4Marker2) :
                    CacheContext.StringIdCache.AddString(unknown4Marker2);
            }

            blamHlmt.ShieldImpactThirdPerson = null;
            blamHlmt.ShieldImpactFirstPerson = null;

            //
            // Finalize new ElDorado model tag
            //

            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating new ElDorado model tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("hlmt")]);
                
                Console.WriteLine("done.");
            }
            else
            {
                newTag = edTag;
            }

            CacheContext.TagNames[newTag.Index] = blamTagName;

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.model\" tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, blamHlmt);
            }

            //
            // Save new string_ids
            //

            if (CacheContext.StringIdCache.Strings.Count != initialStringIDCount)
            {
                Console.Write("Saving string_ids...");

                using (var stringIdStream = CacheContext.OpenStringIdCacheReadWrite())
                    CacheContext.StringIdCache.Save(stringIdStream);

                Console.WriteLine("done.");
            }

            //
            // Done!
            //

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.model\" successfully!");

            return true;
        }

        private CachedTagInstance PortCollisionModel(string blamTagName, CachedTagInstance edTag = null)
        {
            var isNew = (edTag == null);

            //
            // Verify Blam collision_model tag
            //

            CacheFile.IndexItem blamTag = null;

            Console.WriteLine("Verifying Blam collision_model tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "coll" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam collision_model tag does not exist: " + blamTagName);
                return null;
            }

            //
            // Verify ED collision_model tag
            //

            if (!isNew)
            {
                Console.Write("Verifying ElDorado collision_model tag index...");

                if (edTag.Group.Name != CacheContext.GetStringId("collision_model"))
                {
                    Console.WriteLine($"Specified tag index is not a collision_model: 0x{edTag.Index:X4}");
                    return null;
                }

                Console.WriteLine("done.");
            }

            //
            // Load Blam collision_model tag
            //

            Console.Write("Loading Blam collision_model tag...");

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamColl = blamDeserializer.Deserialize<CollisionModel>(blamContext);

            Console.WriteLine("done.");

            //
            // Update Blam collision_model tag definition
            //

            foreach (var material in blamColl.Materials)
            {
                var materialName = BlamCache.Strings.GetItemByID((int)material.Name.Value);
                if (materialName == "<blank>") materialName = "";
                material.Name = CacheContext.StringIdCache.Contains(materialName) ?
                    CacheContext.StringIdCache.GetStringId(materialName) :
                    CacheContext.StringIdCache.AddString(materialName);
            }

            foreach (var region in blamColl.Regions)
            {
                var regionName = BlamCache.Strings.GetItemByID((int)region.Name.Value);
                if (regionName == "<blank>") regionName = "";
                region.Name = CacheContext.StringIdCache.Contains(regionName) ?
                    CacheContext.StringIdCache.GetStringId(regionName) :
                    CacheContext.StringIdCache.AddString(regionName);

                foreach (var permutation in region.Permutations)
                {
                    var permutationName = BlamCache.Strings.GetItemByID((int)permutation.Name.Value);
                    if (permutationName == "<blank>") permutationName = "";
                    permutation.Name = CacheContext.StringIdCache.Contains(permutationName) ?
                        CacheContext.StringIdCache.GetStringId(permutationName) :
                        CacheContext.StringIdCache.AddString(permutationName);

                    foreach (var bsp in permutation.Bsps)
                    {
                        foreach (var bsp3dNode in bsp.Geometry.Bsp3dNodes)
                        {
                            bsp3dNode.Plane = bsp3dNode.Plane_H3;
                            bsp3dNode.FrontChildLower = bsp3dNode.FrontChildLower_H3;
                            bsp3dNode.FrontChildMid = bsp3dNode.FrontChildMid_H3;
                            bsp3dNode.FrontChildUpper = bsp3dNode.FrontChildUpper_H3;
                            bsp3dNode.BackChildLower = bsp3dNode.BackChildLower_H3;
                            bsp3dNode.BackChildMid = bsp3dNode.BackChildMid_H3;
                            bsp3dNode.BackChildUpper = bsp3dNode.BackChildUpper_H3;
                        }
                    }
                }
            }

            foreach (var node in blamColl.Nodes)
            {
                var nodeName = BlamCache.Strings.GetItemByID((int)node.Name.Value);
                if (nodeName == "<blank>") nodeName = "";
                node.Name = CacheContext.StringIdCache.Contains(nodeName) ?
                    CacheContext.StringIdCache.GetStringId(nodeName) :
                    CacheContext.StringIdCache.AddString(nodeName);
            }

            //
            // Finalize new ElDorado collision_model tag
            //

            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating new ElDorado collision_model tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("coll")]);

                Console.WriteLine("done.");
            }
            else
            {
                newTag = edTag;
            }

            CacheContext.TagNames[newTag.Index] = blamTagName;

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                Console.Write($"Writing tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, blamColl);

                Console.WriteLine("done.");
                Console.WriteLine();
            }

            //
            // Done!
            //

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.collision_model\" successfully!");

            return newTag;
        }

        private CachedTagInstance PortPhysicsModel(string blamTagName, CachedTagInstance edTag = null)
        {
            var isNew = (edTag == null);

            //
            // Verify Blam physics_model tag
            //
            
            Console.Write("Verifying Blam physics_model tag...");

            CacheFile.IndexItem blamTag = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "phmo" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam physics_model tag does not exist: " + blamTagName);
                return null;
            }

            Console.WriteLine("done.");

            //
            // Verify ED physics_model tag
            //
            
            if (!isNew)
            {
                Console.Write("Verifying ElDorado physics_model tag index...");
                
                if (edTag.Group.Name != CacheContext.GetStringId("physics_model"))
                {
                    Console.WriteLine($"Specified tag index is not a physics_model: 0x{edTag.Index:X4}");
                    return null;
                }

                Console.WriteLine("done.");
            }

            //
            // Load Blam physics_model tag
            //

            Console.Write("Loading Blam physics_model tag...");

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamPhmo = blamDeserializer.Deserialize<PhysicsModel>(blamContext);

            Console.WriteLine("done.");

            //
            // Update Blam physics_model tag definition
            //

            foreach (var motor in blamPhmo.DampedSpringMotors)
            {
                var motorName = BlamCache.Strings.GetItemByID((int)motor.Name.Value);
                if (motorName == "<blank>") motorName = "";
                motor.Name = CacheContext.StringIdCache.Contains(motorName) ?
                    CacheContext.StringIdCache.GetStringId(motorName) :
                    CacheContext.StringIdCache.AddString(motorName);
            }

            foreach (var motor in blamPhmo.PositionMotors)
            {
                var motorName = BlamCache.Strings.GetItemByID((int)motor.Name.Value);
                if (motorName == "<blank>") motorName = "";
                motor.Name = CacheContext.StringIdCache.Contains(motorName) ?
                    CacheContext.StringIdCache.GetStringId(motorName) :
                    CacheContext.StringIdCache.AddString(motorName);
            }

            foreach (var type in blamPhmo.PhantomTypes)
            {
                var typeMarkerName = BlamCache.Strings.GetItemByID((int)type.MarkerName.Value);
                if (typeMarkerName == "<blank>") typeMarkerName = "";
                type.MarkerName = CacheContext.StringIdCache.Contains(typeMarkerName) ?
                    CacheContext.StringIdCache.GetStringId(typeMarkerName) :
                    CacheContext.StringIdCache.AddString(typeMarkerName);

                var typeAlignmentMarkerName = BlamCache.Strings.GetItemByID((int)type.AlignmentMarkerName.Value);
                if (typeAlignmentMarkerName == "<blank>") typeAlignmentMarkerName = "";
                type.AlignmentMarkerName = CacheContext.StringIdCache.Contains(typeAlignmentMarkerName) ?
                    CacheContext.StringIdCache.GetStringId(typeAlignmentMarkerName) :
                    CacheContext.StringIdCache.AddString(typeAlignmentMarkerName);
            }

            foreach (var edge in blamPhmo.NodeEdges)
            {
                var nodeAMaterialName = BlamCache.Strings.GetItemByID((int)edge.NodeAMaterial.Value);
                if (nodeAMaterialName == "<blank>") nodeAMaterialName = "";
                edge.NodeAMaterial = CacheContext.StringIdCache.Contains(nodeAMaterialName) ?
                    CacheContext.StringIdCache.GetStringId(nodeAMaterialName) :
                    CacheContext.StringIdCache.AddString(nodeAMaterialName);

                var nodeBMaterialName = BlamCache.Strings.GetItemByID((int)edge.NodeBMaterial.Value);
                if (nodeBMaterialName == "<blank>") nodeBMaterialName = "";
                edge.NodeBMaterial = CacheContext.StringIdCache.Contains(nodeBMaterialName) ?
                    CacheContext.StringIdCache.GetStringId(nodeBMaterialName) :
                    CacheContext.StringIdCache.AddString(nodeBMaterialName);
            }

            foreach (var material in blamPhmo.Materials)
            {
                var name = BlamCache.Strings.GetItemByID((int)material.Name.Value);
                if (name == "<blank>") name = "";
                material.Name = CacheContext.StringIdCache.Contains(name) ?
                    CacheContext.StringIdCache.GetStringId(name) :
                    CacheContext.StringIdCache.AddString(name);

                var materialName = BlamCache.Strings.GetItemByID((int)material.MaterialName.Value);
                if (materialName == "<blank>") materialName = "";
                material.MaterialName = CacheContext.StringIdCache.Contains(materialName) ?
                    CacheContext.StringIdCache.GetStringId(materialName) :
                    CacheContext.StringIdCache.AddString(materialName);
            }

            foreach (var sphere in blamPhmo.Spheres)
            {
                var name = BlamCache.Strings.GetItemByID((int)sphere.Name.Value);
                if (name == "<blank>") name = "";
                sphere.Name = CacheContext.StringIdCache.Contains(name) ?
                    CacheContext.StringIdCache.GetStringId(name) :
                    CacheContext.StringIdCache.AddString(name);
            }

            foreach (var pill in blamPhmo.Pills)
            {
                var name = BlamCache.Strings.GetItemByID((int)pill.Name.Value);
                if (name == "<blank>") name = "";
                pill.Name = CacheContext.StringIdCache.Contains(name) ?
                    CacheContext.StringIdCache.GetStringId(name) :
                    CacheContext.StringIdCache.AddString(name);
            }

            foreach (var box in blamPhmo.Boxes)
            {
                var name = BlamCache.Strings.GetItemByID((int)box.Name.Value);
                if (name == "<blank>") name = "";
                box.Name = CacheContext.StringIdCache.Contains(name) ?
                    CacheContext.StringIdCache.GetStringId(name) :
                    CacheContext.StringIdCache.AddString(name);
            }

            foreach (var triangle in blamPhmo.Triangles)
            {
                var name = BlamCache.Strings.GetItemByID((int)triangle.Name.Value);
                if (name == "<blank>") name = "";
                triangle.Name = CacheContext.StringIdCache.Contains(name) ?
                    CacheContext.StringIdCache.GetStringId(name) :
                    CacheContext.StringIdCache.AddString(name);
            }

            foreach (var polyhedron in blamPhmo.Polyhedra)
            {
                var name = BlamCache.Strings.GetItemByID((int)polyhedron.Name.Value);
                if (name == "<blank>") name = "";
                polyhedron.Name = CacheContext.StringIdCache.Contains(name) ?
                    CacheContext.StringIdCache.GetStringId(name) :
                    CacheContext.StringIdCache.AddString(name);
            }

            foreach (var constraint in blamPhmo.HingeConstraints)
            {
                var name = BlamCache.Strings.GetItemByID((int)constraint.Name.Value);
                if (name == "<blank>") name = "";
                constraint.Name = CacheContext.StringIdCache.Contains(name) ?
                    CacheContext.StringIdCache.GetStringId(name) :
                    CacheContext.StringIdCache.AddString(name);
            }

            foreach (var constraint in blamPhmo.RagdollConstraints)
            {
                var name = BlamCache.Strings.GetItemByID((int)constraint.Name.Value);
                if (name == "<blank>") name = "";
                constraint.Name = CacheContext.StringIdCache.Contains(name) ?
                    CacheContext.StringIdCache.GetStringId(name) :
                    CacheContext.StringIdCache.AddString(name);
            }

            foreach (var region in blamPhmo.Regions)
            {
                var regionName = BlamCache.Strings.GetItemByID((int)region.Name.Value);
                if (regionName == "<blank>") regionName = "";
                region.Name = CacheContext.StringIdCache.Contains(regionName) ?
                    CacheContext.StringIdCache.GetStringId(regionName) :
                    CacheContext.StringIdCache.AddString(regionName);

                foreach (var permutation in region.Permutations)
                {
                    var permutationName = BlamCache.Strings.GetItemByID((int)permutation.Name.Value);
                    if (permutationName == "<blank>") permutationName = "";
                    permutation.Name = CacheContext.StringIdCache.Contains(permutationName) ?
                        CacheContext.StringIdCache.GetStringId(permutationName) :
                        CacheContext.StringIdCache.AddString(permutationName);
                }
            }

            foreach (var node in blamPhmo.Nodes)
            {
                var nodeName = BlamCache.Strings.GetItemByID((int)node.Name.Value);
                if (nodeName == "<blank>") nodeName = "";
                node.Name = CacheContext.StringIdCache.Contains(nodeName) ?
                    CacheContext.StringIdCache.GetStringId(nodeName) :
                    CacheContext.StringIdCache.AddString(nodeName);
            }

            foreach (var constraint in blamPhmo.LimitedHingeConstraints)
            {
                var name = BlamCache.Strings.GetItemByID((int)constraint.Name.Value);
                if (name == "<blank>") name = "";
                constraint.Name = CacheContext.StringIdCache.Contains(name) ?
                    CacheContext.StringIdCache.GetStringId(name) :
                    CacheContext.StringIdCache.AddString(name);
            }

            //
            // Finalize new ElDorado physics_model tag
            //

            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating new ElDorado physics_model tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("hlmt")]);

                Console.WriteLine("done.");
            }
            else
            {
                newTag = edTag;
            }

            CacheContext.TagNames[newTag.Index] = blamTagName;

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.physics_model\" tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, blamPhmo);
            }
            
            //
            // Done!
            //

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.physics_model\" successfully!");

            return newTag;
        }

        private CachedTagInstance PortRenderModel(string blamTagName, CachedTagInstance edTag = null)
        {
            var isNew = (edTag == null);

            //
            // Verify Blam render_model tag
            //

            Console.Write("Verifying Blam render_model tag...");

            CacheFile.IndexItem blamTag = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "mode" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam render_model tag does not exist: " + blamTagName);
                return null;
            }

            Console.WriteLine("done.");

            //
            // Verify the ED render_model tag
            //
            
            if (!isNew)
            {
                Console.Write("Verifying ED tag index...");
                
                if (edTag.Group.Name != CacheContext.GetStringId("render_model"))
                {
                    Console.WriteLine($"Specified tag index is not a render_model: 0x{edTag.Index:X4}");
                    return null;
                }

                Console.WriteLine("done.");
            }

            //
            // Deserialize the Blam render_model tag
            //

            Console.Write("Loading Blam render_model tag...");

            var blamDeserializer = new TagDeserializer(BlamCache.Version);

            RenderModel blamMode;

            try
            {
                var context = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
                blamMode = blamDeserializer.Deserialize<RenderModel>(context);
            }
            catch
            {
                Console.WriteLine("Failed to deserialize Blam render_model tag: " + blamTagName);
                return null;
            }

            Console.WriteLine("done.");

            //
            // Load Blam resource fixups
            //

            var fixupEntry = BlamCache.ResourceGestalt.DefinitionEntries[blamMode.Geometry.ZoneAssetIndex & ushort.MaxValue];

            var resourceDefinition = new RenderGeometryApiResourceDefinition
            {
                VertexBuffers = new List<D3DPointer<VertexBufferDefinition>>(),
                IndexBuffers = new List<D3DPointer<IndexBufferDefinition>>()
            };

            using (var fixupStream = new MemoryStream(BlamCache.ResourceGestalt.FixupData))
            using (var reader = new EndianReader(fixupStream, EndianFormat.BigEndian))
            {
                var dataContext = new DataSerializationContext(reader, null);

                reader.SeekTo(fixupEntry.Offset + (fixupEntry.Size - 24));

                var vertexBufferCount = reader.ReadInt32();
                reader.Skip(8);
                var indexBufferCount = reader.ReadInt32();

                Console.Write("Loading Blam render_model vertex fixups...");

                reader.SeekTo(fixupEntry.Offset);

                for (var i = 0; i < vertexBufferCount; i++)
                {
                    resourceDefinition.VertexBuffers.Add(new D3DPointer<VertexBufferDefinition>
                    {
                        Definition = new VertexBufferDefinition
                        {
                            Count = reader.ReadInt32(),
                            Format = (VertexBufferFormat)reader.ReadInt16(),
                            VertexSize = reader.ReadInt16(),
                            Data = new ResourceDataReference
                            {
                                Size = reader.ReadInt32(),
                                Unused4 = reader.ReadInt32(),
                                Unused8 = reader.ReadInt32(),
                                Address = new ResourceAddress(ResourceAddressType.Memory, reader.ReadInt32()),
                                Unused10 = reader.ReadInt32()
                            }
                        }
                    });
                }

                Console.WriteLine("done.");

                reader.Skip(vertexBufferCount * 12);

                Console.Write("Loading Blam render_model index buffer fixups...");

                for (var i = 0; i < indexBufferCount; i++)
                {
                    resourceDefinition.IndexBuffers.Add(new D3DPointer<IndexBufferDefinition>
                    {
                        Definition = new IndexBufferDefinition
                        {
                            Type = (PrimitiveType)reader.ReadInt32(),
                            Data = new ResourceDataReference
                            {
                                Size = reader.ReadInt32(),
                                Unused4 = reader.ReadInt32(),
                                Unused8 = reader.ReadInt32(),
                                Address = new ResourceAddress(ResourceAddressType.Memory, reader.ReadInt32()),
                                Unused10 = reader.ReadInt32()
                            }
                        }
                    });
                }

                Console.WriteLine("done.");
            }

            //
            // Convert Blam data to ElDorado data
            //

            ConvertStringId(ref blamMode.Name);

            foreach (var region in blamMode.Regions)
            {
                ConvertStringId(ref region.Name);

                foreach (var permutation in region.Permutations)
                {
                    ConvertStringId(ref permutation.Name);
                }
            }

            foreach (var instance in blamMode.Instances)
            {
                ConvertStringId(ref instance.Name);
            }

            foreach (var node in blamMode.Nodes)
            {
                ConvertStringId(ref node.Name);
            }

            foreach (var markerGroup in blamMode.MarkerGroups)
            {
                ConvertStringId(ref markerGroup.Name);
            }

            foreach (var material in blamMode.Materials)
            {
                material.RenderMethod = PortShader(BlamCache.IndexItems.Find(i => i.ID == material.RenderMethod.Index).Filename);
            }

            using (var edResourceStream = new MemoryStream())
            {
                using (var blamResourceStream = new MemoryStream(BlamCache.GetRawFromID(blamMode.Geometry.ZoneAssetIndex)))
                {
                    //
                    // Convert Blam render_geometry_api_resource_definition
                    //

                    Console.Write("Converting Blam render_model resource...");

                    var inVertexStream = VertexStreamFactory.Create(BlamCache.Version, blamResourceStream);
                    var outVertexStream = VertexStreamFactory.Create(CacheContext.Version, edResourceStream);

                    for (var i = 0; i < resourceDefinition.VertexBuffers.Count; i++)
                    {
                        var vertexBuffer = resourceDefinition.VertexBuffers[i].Definition;

                        var count = vertexBuffer.Count;

                        var startPos = (int)edResourceStream.Position;
                        vertexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                        blamResourceStream.Position = fixupEntry.Fixups[i].Offset;

                        switch (vertexBuffer.Format)
                        {
                            case VertexBufferFormat.World:
                                ConvertVertices(count, inVertexStream.ReadWorldVertex, outVertexStream.WriteWorldVertex);
                                break;

                            case VertexBufferFormat.Rigid:
                                ConvertVertices(count, inVertexStream.ReadRigidVertex, outVertexStream.WriteRigidVertex);
                                break;

                            case VertexBufferFormat.Skinned:
                                ConvertVertices(count, inVertexStream.ReadSkinnedVertex, outVertexStream.WriteSkinnedVertex);
                                break;

                            case VertexBufferFormat.StaticPerPixel:
                                ConvertVertices(count, inVertexStream.ReadStaticPerPixelData, outVertexStream.WriteStaticPerPixelData);
                                break;

                            case VertexBufferFormat.StaticPerVertex:
                                ConvertVertices(count, inVertexStream.ReadStaticPerVertexData, outVertexStream.WriteStaticPerVertexData);
                                break;

                            case VertexBufferFormat.AmbientPrt:
                                ConvertVertices(count, inVertexStream.ReadAmbientPrtData, outVertexStream.WriteAmbientPrtData);
                                break;

                            case VertexBufferFormat.LinearPrt:
                                ConvertVertices(count, inVertexStream.ReadLinearPrtData, outVertexStream.WriteLinearPrtData);
                                break;

                            case VertexBufferFormat.QuadraticPrt:
                                ConvertVertices(count, inVertexStream.ReadQuadraticPrtData, outVertexStream.WriteQuadraticPrtData);
                                break;

                            case VertexBufferFormat.StaticPerVertexColor:
                                ConvertVertices(count, inVertexStream.ReadStaticPerVertexColorData, outVertexStream.WriteStaticPerVertexColorData);
                                break;

                            case VertexBufferFormat.Decorator:
                                ConvertVertices(count, inVertexStream.ReadDecoratorVertex, outVertexStream.WriteDecoratorVertex);
                                break;

                            case VertexBufferFormat.World2:
                                vertexBuffer.Format = VertexBufferFormat.World;
                                goto case VertexBufferFormat.World;

                            default:
                                throw new NotSupportedException(vertexBuffer.Format.ToString());
                        }

                        vertexBuffer.Data.Size = (int)edResourceStream.Position - startPos;
                        vertexBuffer.VertexSize = (short)(vertexBuffer.Data.Size / vertexBuffer.Count);
                    }

                    for (var i = 0; i < resourceDefinition.IndexBuffers.Count; i++)
                    {
                        var indexBuffer = resourceDefinition.IndexBuffers[i].Definition;

                        var indexCount = indexBuffer.Data.Size / 2;

                        var inIndexStream = new IndexBufferStream(
                            blamResourceStream,
                            IndexBufferFormat.UInt16,
                            EndianFormat.BigEndian);

                        var outIndexStream = new IndexBufferStream(
                            edResourceStream,
                            IndexBufferFormat.UInt16,
                            EndianFormat.LittleEndian);

                        var startPos = (int)edResourceStream.Position;
                        indexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                        blamResourceStream.Position = fixupEntry.Fixups[resourceDefinition.VertexBuffers.Count * 2 + i].Offset;

                        for (var j = 0; j < indexCount; j++)
                            outIndexStream.WriteIndex(inIndexStream.ReadIndex());
                    }

                    Console.WriteLine("done.");
                }

                //
                // Finalize the new ElDorado render_model tag
                //

                CachedTagInstance newTag;

                if (isNew)
                {
                    Console.Write("Allocating the new ElDorado render_model tag...");

                    using (var stream = CacheContext.OpenTagCacheReadWrite())
                        newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("mode")]);

                    Console.WriteLine("done.");
                }
                else
                {
                    newTag = edTag;
                }

                CacheContext.TagNames[newTag.Index] = blamTagName;

                blamMode.Geometry.Resource = new ResourceReference
                {
                    Type = 5, // FIXME: hax
                    DefinitionFixups = new List<ResourceDefinitionFixup>(),
                    D3DObjectFixups = new List<D3DObjectFixup>(),
                    Unknown68 = 1,
                    Owner = newTag
                };

                Console.WriteLine("Writing resource data...");

                edResourceStream.Position = 0;

                var resourceContext = new ResourceSerializationContext(blamMode.Geometry.Resource);
                CacheContext.Serializer.Serialize(resourceContext, resourceDefinition);
                CacheContext.AddResource(blamMode.Geometry.Resource, ResourceLocation.Resources, edResourceStream);

                using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
                {
                    Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.render_model\" tag data...");

                    var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                    CacheContext.Serializer.Serialize(context, blamMode);
                }
                
                //
                // Done!
                //

                Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.render_model\" successfully!");

                return newTag;
            }
        }

        private CachedTagInstance PortModelAnimationGraph(string blamTagName, CachedTagInstance edTag = null)
        {
            var isNew = (edTag == null);

            //
            // Verify the Blam model_animation_graph tag
            //
            
            CacheFile.IndexItem blamTag = null;

            Console.Write("Verifying Blam model_animation_graph tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "jmad" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine($"Blam model_animation_graph tag does not exist: {blamTagName}");
                return null;
            }

            Console.WriteLine("done.");

            //
            // Verify the ElDorado model_animation_graph tag
            //
            
            if (!isNew)
            {
                Console.Write("Verifying ElDorado model_animation_graph tag index...");
                
                if (edTag.Group.Name != CacheContext.GetStringId("model_animation_graph"))
                {
                    Console.WriteLine($"Specified tag index is not a model_animation_graph: 0x{edTag.Index:X4}");
                    return null;
                }

                Console.WriteLine("done.");
            }

            //
            // Load the Blam model_animation_graph tag
            //

            Console.Write("Loading Blam model_animation_graph tag...");

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamJmad = blamDeserializer.Deserialize<ModelAnimationGraph>(blamContext);

            Console.WriteLine("done.");

            //
            // Fix data in Blam model_animation_graph tag
            //

            blamJmad.ParentAnimationGraph = null;

            foreach (var skeletonNode in blamJmad.SkeletonNodes)
            {
                var stringId = BlamCache.Strings.GetItemByID((int)skeletonNode.Name.Value);

                skeletonNode.Name = CacheContext.StringIdCache.Contains(stringId) ?
                    CacheContext.StringIdCache.GetStringId(stringId) :
                    CacheContext.StringIdCache.AddString(stringId);
            }

            foreach (var reference in blamJmad.SoundReferences)
            {
                reference.Reference = null;
            }

            foreach (var reference in blamJmad.EffectReferences)
            {
                reference.Reference = null;
            }

            foreach (var blendScreen in blamJmad.BlendScreens)
            {
                var stringId = BlamCache.Strings.GetItemByID((int)blendScreen.Label.Value);

                blendScreen.Label = CacheContext.StringIdCache.Contains(stringId) ?
                    CacheContext.StringIdCache.GetStringId(stringId) :
                    CacheContext.StringIdCache.AddString(stringId);
            }

            foreach (var leg in blamJmad.Legs)
            {
                var stringId = BlamCache.Strings.GetItemByID((int)leg.FootMarker.Value);

                leg.FootMarker = CacheContext.StringIdCache.Contains(stringId) ?
                    CacheContext.StringIdCache.GetStringId(stringId) :
                    CacheContext.StringIdCache.AddString(stringId);

                var stringId2 = BlamCache.Strings.GetItemByID((int)leg.AnkleMarker.Value);

                leg.AnkleMarker = CacheContext.StringIdCache.Contains(stringId2) ?
                    CacheContext.StringIdCache.GetStringId(stringId2) :
                    CacheContext.StringIdCache.AddString(stringId2);
            }

            foreach (var animation in blamJmad.Animations)
            {
                var stringId = BlamCache.Strings.GetItemByID((int)animation.Name.Value);

                animation.Name = CacheContext.StringIdCache.Contains(stringId) ?
                    CacheContext.StringIdCache.GetStringId(stringId) :
                    CacheContext.StringIdCache.AddString(stringId);
            }

            foreach (var mode in blamJmad.Modes)
            {
                var stringId = BlamCache.Strings.GetItemByID((int)mode.Label.Value);

                mode.Label = CacheContext.StringIdCache.Contains(stringId) ?
                    CacheContext.StringIdCache.GetStringId(stringId) :
                    CacheContext.StringIdCache.AddString(stringId);

                foreach (var weaponClass in mode.WeaponClass)
                {
                    stringId = BlamCache.Strings.GetItemByID((int)weaponClass.Label.Value);

                    weaponClass.Label = CacheContext.StringIdCache.Contains(stringId) ?
                        CacheContext.StringIdCache.GetStringId(stringId) :
                        CacheContext.StringIdCache.AddString(stringId);

                    if (weaponClass.SyncActionGroups != null)
                        foreach (var syncActionGroup in weaponClass.SyncActionGroups)
                            if (syncActionGroup.SyncActions != null)
                                foreach (var syncAction in syncActionGroup.SyncActions)
                                    if (syncAction.OtherParticipants != null)
                                        foreach (var otherParticipant in syncAction.OtherParticipants)
                                            otherParticipant.ObjectType = null;

                    foreach (var weaponType in weaponClass.WeaponType)
                    {
                        stringId = BlamCache.Strings.GetItemByID((int)weaponType.Label.Value);

                        weaponType.Label = CacheContext.StringIdCache.Contains(stringId) ?
                            CacheContext.StringIdCache.GetStringId(stringId) :
                            CacheContext.StringIdCache.AddString(stringId);

                        foreach (var action in weaponType.Actions)
                        {
                            stringId = BlamCache.Strings.GetItemByID((int)action.Label.Value);

                            action.Label = CacheContext.StringIdCache.Contains(stringId) ?
                                CacheContext.StringIdCache.GetStringId(stringId) :
                                CacheContext.StringIdCache.AddString(stringId);
                        }

                        foreach (var overlay in weaponType.Overlays)
                        {
                            stringId = BlamCache.Strings.GetItemByID((int)overlay.Label.Value);

                            overlay.Label = CacheContext.StringIdCache.Contains(stringId) ?
                                CacheContext.StringIdCache.GetStringId(stringId) :
                                CacheContext.StringIdCache.AddString(stringId);
                        }
                    }
                    foreach (var weaponIk in weaponClass.WeaponIk)
                    {
                        stringId = BlamCache.Strings.GetItemByID((int)weaponIk.Marker.Value);

                        weaponIk.Marker = CacheContext.StringIdCache.Contains(stringId) ?
                            CacheContext.StringIdCache.GetStringId(stringId) :
                            CacheContext.StringIdCache.AddString(stringId);

                        var stringId2 = BlamCache.Strings.GetItemByID((int)weaponIk.AttachToMarker.Value);

                        weaponIk.AttachToMarker = CacheContext.StringIdCache.Contains(stringId2) ?
                            CacheContext.StringIdCache.GetStringId(stringId2) :
                            CacheContext.StringIdCache.AddString(stringId2);
                    }
                }
            }

            foreach (var vehicleSuspension in blamJmad.VehicleSuspension)
            {
                var stringId = BlamCache.Strings.GetItemByID((int)vehicleSuspension.Label.Value);

                vehicleSuspension.Label = CacheContext.StringIdCache.Contains(stringId) ?
                    CacheContext.StringIdCache.GetStringId(stringId) :
                    CacheContext.StringIdCache.AddString(stringId);

                var stringId2 = BlamCache.Strings.GetItemByID((int)vehicleSuspension.MarkerName.Value);

                vehicleSuspension.MarkerName = CacheContext.StringIdCache.Contains(stringId2) ?
                    CacheContext.StringIdCache.GetStringId(stringId2) :
                    CacheContext.StringIdCache.AddString(stringId2);

                var stringId3 = BlamCache.Strings.GetItemByID((int)vehicleSuspension.RegionName.Value);

                vehicleSuspension.RegionName = CacheContext.StringIdCache.Contains(stringId3) ?
                    CacheContext.StringIdCache.GetStringId(stringId3) :
                    CacheContext.StringIdCache.AddString(stringId3);
            }

            foreach (var objectOverlay in blamJmad.ObjectOverlays)
            {
                var stringId = BlamCache.Strings.GetItemByID((int)objectOverlay.Label.Value);

                objectOverlay.Label = CacheContext.StringIdCache.Contains(stringId) ?
                    CacheContext.StringIdCache.GetStringId(stringId) :
                    CacheContext.StringIdCache.AddString(stringId);

                var stringId2 = BlamCache.Strings.GetItemByID((int)objectOverlay.Function.Value);

                objectOverlay.Function = CacheContext.StringIdCache.Contains(stringId2) ?
                    CacheContext.StringIdCache.GetStringId(stringId2) :
                    CacheContext.StringIdCache.AddString(stringId2);
            }

            foreach (var inheritance in blamJmad.InheritanceList)
            {
                inheritance.InheritedGraph = null;
            }

            foreach (var weaponList in blamJmad.WeaponList)
            {
                var stringId = BlamCache.Strings.GetItemByID((int)weaponList.WeaponName.Value);

                weaponList.WeaponName = CacheContext.StringIdCache.Contains(stringId) ?
                    CacheContext.StringIdCache.GetStringId(stringId) :
                    CacheContext.StringIdCache.AddString(stringId);

                var stringId2 = BlamCache.Strings.GetItemByID((int)weaponList.WeaponClass.Value);

                weaponList.WeaponClass = CacheContext.StringIdCache.Contains(stringId2) ?
                    CacheContext.StringIdCache.GetStringId(stringId2) :
                    CacheContext.StringIdCache.AddString(stringId2);
            }
            
            Console.Write("Loading Blam model_animation_graph resource fixups...");

            var deserializer = new TagDeserializer(BlamCache.Version);

            var resourceGroups = new List<ModelAnimationTagResource>(); // has list of all groups and list of members, for members framecount
            var resourcesFixups = new List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry>(); // has list of all groups and list of members, for members offsets

            using (var reader = new EndianReader(new MemoryStream(BlamCache.ResourceGestalt.FixupData), EndianFormat.BigEndian))
            {
                foreach (var resource in blamJmad.ResourceGroups)
                {
                    var RawID = resource.ZoneAssetDatumIndex;
                    var Entry = BlamCache.ResourceGestalt.DefinitionEntries[RawID & ushort.MaxValue];
                    resourcesFixups.Add(Entry);
                    resourceGroups.Add(new ModelAnimationTagResource());
                    resourceGroups[blamJmad.ResourceGroups.IndexOf(resource)].GroupMembers = new List<ModelAnimationTagResource.GroupMember>();

                    reader.SeekTo(Entry.Offset);
                    var dataContext = new DataSerializationContext(reader, null);

                    for (int memberIndex = 0; memberIndex < resource.MemberCount; memberIndex++)
                    {
                        var member = deserializer.Deserialize<ModelAnimationTagResource.GroupMember>(dataContext);

                        resourceGroups[blamJmad.ResourceGroups.IndexOf(resource)].GroupMembers.Add(new ModelAnimationTagResource.GroupMember());
                        resourceGroups[blamJmad.ResourceGroups.IndexOf(resource)].GroupMembers[memberIndex] = member;
                    }
                }
            }

            Console.WriteLine("done.");

            var endianFormat = EndianFormat.BigEndian; // DEBUG: BigEndian for HO format. LittleEndian to extract and compare to H3 raw.
            
            Console.Write("Converting animations...");

            foreach (var resourceGroup in blamJmad.ResourceGroups)
            {
                var resourceGroupIndex = blamJmad.ResourceGroups.IndexOf(resourceGroup);

                using (var newStream = new MemoryStream())
                using (var newResourceGroupStream = new BinaryWriter(newStream))
                using (var resourceDataStream = new MemoryStream(BlamCache.GetRawFromID(resourceGroup.ZoneAssetDatumIndex)))
                using (var reader = new EndianReader(resourceDataStream, endianFormat))
                {
                    var dataContext = new DataSerializationContext(reader, null);

                    ModelAnimationTagResource.GroupMember.AnimationHeaderType1 Type1;

                    foreach (var member in resourceGroups[resourceGroupIndex].GroupMembers)
                    {
                        ModelAnimationTagResource.GroupMember rawMember = new ModelAnimationTagResource.GroupMember();

                        //
                        // Main; Get Member Info
                        //

                        int frameCount = member.FrameCount;
                        int resourceIndex = resourceGroupIndex;
                        int memberIndex = resourceGroups[resourceGroupIndex].GroupMembers.IndexOf(member);
                        int memberOffset = resourcesFixups[resourceIndex].Fixups[memberIndex].Offset;
                        short overlayOffset = member.OverlayOffset;
                        uint footerOffset = member.FlagsOffset;
                        var footerOffsetActual = memberOffset + overlayOffset + footerOffset;
                        var overlayOffsetActual = memberOffset + overlayOffset;

                        reader.BaseStream.Position = memberOffset;
                        
                        ModelAnimationTagResource.GroupMember.AnimationHeaderGlobal headerOverlayGlobal = new ModelAnimationTagResource.GroupMember.AnimationHeaderGlobal();

                        var headerBasePosition = reader.BaseStream.Position;

                        var headerGlobal = CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.AnimationHeaderGlobal>(dataContext); // read simple header to know its type
                        reader.BaseStream.Position -= 0x4; // there must be a better non cheap cheesy way

                        //
                        // Type 1
                        //

                        Type1 = new ModelAnimationTagResource.GroupMember.AnimationHeaderType1();
                        if (headerGlobal.HeaderStructType == ModelAnimationTagResource.GroupMemberAnimationType.Type1)
                        {
                            dataContext = new DataSerializationContext(reader, null);

                            Type1 = CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.AnimationHeaderType1>(dataContext);

                            newResourceGroupStream.Write((sbyte)Type1.HeaderStructType);
                            newResourceGroupStream.Write((sbyte)Type1.RotationNodeCount);
                            newResourceGroupStream.Write((sbyte)Type1.PositionNodeCount);
                            newResourceGroupStream.Write((sbyte)Type1.SpecularNodeCount);
                            newResourceGroupStream.Write(0x796E6144); // signature, doesn't seem to affect the file anyway.
                            newResourceGroupStream.Write(0x39333635); // signature
                            newResourceGroupStream.Write((uint)Type1.PositionFramesOffset);
                            newResourceGroupStream.Write((uint)Type1.SpecularFramesOffset);
                            newResourceGroupStream.Write((uint)Type1.RotationFramesSize);
                            newResourceGroupStream.Write((uint)Type1.PositionFramesSize);
                            newResourceGroupStream.Write((uint)Type1.SpecularFramesSize);

                            //
                            // Type1 Static Frames
                            //

                            List<ModelAnimationTagResource.GroupMember.RotationFrame> StaticRotations = new List<ModelAnimationTagResource.GroupMember.RotationFrame>();
                            List<ModelAnimationTagResource.GroupMember.PositionFrame> StaticPositions = new List<ModelAnimationTagResource.GroupMember.PositionFrame>();
                            List<ModelAnimationTagResource.GroupMember.SpecularFrame> StaticSpeculars = new List<ModelAnimationTagResource.GroupMember.SpecularFrame>();

                            int staticFrameCount = 1;
                            for (int nodeIndex = 0; nodeIndex < Type1.RotationNodeCount; nodeIndex++)
                            {
                                var rotationFrames = new List<ModelAnimationTagResource.GroupMember.RotationFrame>();
                                for (int frameIndex = 0; frameIndex < staticFrameCount; frameIndex++)
                                {
                                    StaticRotations.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.RotationFrame>(dataContext));
                                    newResourceGroupStream.Write(StaticRotations[StaticRotations.Count - 1].X);
                                    newResourceGroupStream.Write(StaticRotations[StaticRotations.Count - 1].Y);
                                    newResourceGroupStream.Write(StaticRotations[StaticRotations.Count - 1].Z);
                                    newResourceGroupStream.Write(StaticRotations[StaticRotations.Count - 1].W);
                                }
                            }

                            for (int nodeIndex = 0; nodeIndex < Type1.PositionNodeCount; nodeIndex++)
                            {
                                var positionFrames = new List<ModelAnimationTagResource.GroupMember.PositionFrame>();
                                for (int frameIndex = 0; frameIndex < staticFrameCount; frameIndex++)
                                {
                                    StaticPositions.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.PositionFrame>(dataContext));
                                    newResourceGroupStream.Write(StaticPositions[StaticPositions.Count - 1].X);
                                    newResourceGroupStream.Write(StaticPositions[StaticPositions.Count - 1].Y);
                                    newResourceGroupStream.Write(StaticPositions[StaticPositions.Count - 1].Z);
                                }
                            }

                            for (int nodeIndex = 0; nodeIndex < Type1.SpecularNodeCount; nodeIndex++)
                            {
                                var specularFrames = new List<ModelAnimationTagResource.GroupMember.SpecularFrame>();
                                for (int frameIndex = 0; frameIndex < staticFrameCount; frameIndex++)
                                {
                                    StaticSpeculars.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext));
                                    newResourceGroupStream.Write(StaticSpeculars[StaticSpeculars.Count - 1].X);
                                }
                            }
                        }

                        //
                        // Read header 2 overlay type
                        //

                        reader.BaseStream.Position = memberOffset + overlayOffset;
                        dataContext = new DataSerializationContext(reader, null);
                        headerGlobal = CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.AnimationHeaderGlobal>(dataContext);
                        reader.BaseStream.Position -= 0x4; // there must be a better non cheap cheesy way

                        switch (headerGlobal.HeaderStructType)
                        {
                            case ModelAnimationTagResource.GroupMemberAnimationType.Type3:
                                {
                                    var Type3 = CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.AnimationHeaderType1>(dataContext);

                                    newResourceGroupStream.Write((sbyte)Type3.HeaderStructType);
                                    newResourceGroupStream.Write((sbyte)Type3.RotationNodeCount);
                                    newResourceGroupStream.Write((sbyte)Type3.PositionNodeCount);
                                    newResourceGroupStream.Write((sbyte)Type3.SpecularNodeCount);
                                    newResourceGroupStream.Write((uint)Type3.Unknown0);
                                    newResourceGroupStream.Write((uint)Type3.Unknown1);
                                    newResourceGroupStream.Write((uint)Type3.PositionFramesOffset);
                                    newResourceGroupStream.Write((uint)Type3.SpecularFramesOffset);
                                    newResourceGroupStream.Write((uint)Type3.RotationFramesSize);
                                    newResourceGroupStream.Write((uint)Type3.PositionFramesSize);
                                    newResourceGroupStream.Write((uint)Type3.SpecularFramesSize);

                                    //
                                    // Type3 Animated Frames
                                    //

                                    List<ModelAnimationTagResource.GroupMember.RotationFrame> OverlayRotations = new List<ModelAnimationTagResource.GroupMember.RotationFrame>();
                                    List<ModelAnimationTagResource.GroupMember.PositionFrame> OverlayPositions = new List<ModelAnimationTagResource.GroupMember.PositionFrame>();
                                    List<ModelAnimationTagResource.GroupMember.SpecularFrame> OverlaySpeculars = new List<ModelAnimationTagResource.GroupMember.SpecularFrame>();

                                    for (int nodeIndex = 0; nodeIndex < Type3.RotationNodeCount; nodeIndex++)
                                    {
                                        var rotationFrames = new List<ModelAnimationTagResource.GroupMember.RotationFrame>();
                                        for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
                                        {
                                            OverlayRotations.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.RotationFrame>(dataContext));
                                            newResourceGroupStream.Write(OverlayRotations[OverlayRotations.Count - 1].X);
                                            newResourceGroupStream.Write(OverlayRotations[OverlayRotations.Count - 1].Y);
                                            newResourceGroupStream.Write(OverlayRotations[OverlayRotations.Count - 1].Z);
                                            newResourceGroupStream.Write(OverlayRotations[OverlayRotations.Count - 1].W);
                                        }
                                    }

                                    for (int nodeIndex = 0; nodeIndex < Type3.PositionNodeCount; nodeIndex++)
                                    {
                                        var positionFrames = new List<ModelAnimationTagResource.GroupMember.PositionFrame>();
                                        for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
                                        {
                                            OverlayPositions.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.PositionFrame>(dataContext));
                                            newResourceGroupStream.Write(OverlayPositions[OverlayPositions.Count - 1].X);
                                            newResourceGroupStream.Write(OverlayPositions[OverlayPositions.Count - 1].Y);
                                            newResourceGroupStream.Write(OverlayPositions[OverlayPositions.Count - 1].Z);
                                        }
                                    }

                                    for (int nodeIndex = 0; nodeIndex < Type3.SpecularNodeCount; nodeIndex++)
                                    {
                                        var specularFrames = new List<ModelAnimationTagResource.GroupMember.SpecularFrame>();
                                        for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
                                        {
                                            OverlaySpeculars.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext));
                                            newResourceGroupStream.Write(OverlaySpeculars[OverlaySpeculars.Count - 1].X);
                                        }
                                    }
                                    break;
                                }

                            case ModelAnimationTagResource.GroupMemberAnimationType.Type4:
                                {
                                    var Type4 = CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.AnimationHeaderOverlay>(dataContext);

                                    newResourceGroupStream.Write((sbyte)Type4.HeaderStructType);
                                    newResourceGroupStream.Write((sbyte)Type4.RotationNodeCount);
                                    newResourceGroupStream.Write((sbyte)Type4.PositionNodeCount);
                                    newResourceGroupStream.Write((sbyte)Type4.SpecularNodeCount);
                                    newResourceGroupStream.Write((uint)Type4.Unknown0);
                                    newResourceGroupStream.Write((uint)Type4.Unknown1);
                                    newResourceGroupStream.Write((uint)Type4.PositionFramesCountPerNodeOffset);
                                    newResourceGroupStream.Write((uint)Type4.RealFrameMarkerOffset);
                                    newResourceGroupStream.Write((uint)Type4.RealFrameMarkerOffset1);
                                    newResourceGroupStream.Write((uint)Type4.RotationsRealFrameMarkersEndOffset);
                                    newResourceGroupStream.Write((uint)Type4.PositionsRealFrameMarkersEndOffset);
                                    newResourceGroupStream.Write((uint)Type4.RotationFramesOffset);
                                    newResourceGroupStream.Write((uint)Type4.PositionFramesOffset);
                                    newResourceGroupStream.Write((uint)Type4.SpecularFramesOffset);
                                    newResourceGroupStream.Write((uint)Type4.UselessPadding);

                                    // Since there's an inconsistent frame count per node, calculations need to be done to know which node has how many frames, 
                                    //      Using ModelAnimationTagResource.GroupMember.PositionFramesCountPerNode, ModelAnimationTagResource.GroupMember.RealFrameMarker

                                    var PositionFramesCountPerNode = new List<ModelAnimationTagResource.GroupMember.PositionFramesCountPerNode>();
                                    var RealFrameMarker = new List<ModelAnimationTagResource.GroupMember.RealFrameMarker>();
                                    var OverlayRotations = new List<ModelAnimationTagResource.GroupMember.RotationFrame>();
                                    var OverlayPositions = new List<ModelAnimationTagResource.GroupMember.PositionFrame>();
                                    var OverlaySpeculars = new List<ModelAnimationTagResource.GroupMember.SpecularFrame>();

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type4.RealFrameMarkerOffset) // until the next block of whatever
                                    {
                                        PositionFramesCountPerNode.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.PositionFramesCountPerNode>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type4.RotationFramesOffset)
                                    {
                                        RealFrameMarker.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.RealFrameMarker>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type4.PositionFramesOffset)
                                    {
                                        OverlayRotations.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.RotationFrame>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type4.SpecularFramesOffset)
                                    {
                                        OverlayPositions.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.PositionFrame>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < footerOffset)
                                    {
                                        OverlaySpeculars.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext));
                                    }

                                    foreach (var a in PositionFramesCountPerNode)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                    }

                                    foreach (var a in RealFrameMarker)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                        newResourceGroupStream.Write(a.Y);
                                        newResourceGroupStream.Write(a.Z);
                                        newResourceGroupStream.Write(a.W);
                                    }

                                    foreach (var a in OverlayRotations)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                        newResourceGroupStream.Write(a.Y);
                                        newResourceGroupStream.Write(a.Z);
                                        newResourceGroupStream.Write(a.W);
                                    }

                                    foreach (var a in OverlayPositions)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                        newResourceGroupStream.Write(a.Y);
                                        newResourceGroupStream.Write(a.Z);
                                    }

                                    foreach (var a in OverlaySpeculars)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                    }
                                    break;
                                }

                            case ModelAnimationTagResource.GroupMemberAnimationType.Type6:
                                {
                                    var Type6 = CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.AnimationHeaderOverlay>(dataContext);

                                    newResourceGroupStream.Write((sbyte)Type6.HeaderStructType);
                                    newResourceGroupStream.Write((sbyte)Type6.RotationNodeCount);
                                    newResourceGroupStream.Write((sbyte)Type6.PositionNodeCount);
                                    newResourceGroupStream.Write((sbyte)Type6.SpecularNodeCount);
                                    newResourceGroupStream.Write((uint)Type6.Unknown0);
                                    newResourceGroupStream.Write((uint)Type6.Unknown1);
                                    newResourceGroupStream.Write((uint)Type6.PositionFramesCountPerNodeOffset);
                                    newResourceGroupStream.Write((uint)Type6.RealFrameMarkerOffset);
                                    newResourceGroupStream.Write((uint)Type6.RealFrameMarkerOffset1);
                                    newResourceGroupStream.Write((uint)Type6.RotationsRealFrameMarkersEndOffset);
                                    newResourceGroupStream.Write((uint)Type6.PositionsRealFrameMarkersEndOffset);
                                    newResourceGroupStream.Write((uint)Type6.RotationFramesOffset);
                                    newResourceGroupStream.Write((uint)Type6.PositionFramesOffset);
                                    newResourceGroupStream.Write((uint)Type6.SpecularFramesOffset);
                                    newResourceGroupStream.Write((uint)Type6.UselessPadding);

                                    var PositionFramesCountPerNode = new List<ModelAnimationTagResource.GroupMember.PositionFramesCountPerNode>();
                                    var RealFrameMarker = new List<ModelAnimationTagResource.GroupMember.RealFrameMarker>();
                                    var OverlayRotations = new List<ModelAnimationTagResource.GroupMember.RotationFrame>();
                                    var OverlayPositions = new List<ModelAnimationTagResource.GroupMember.PositionFrame>();
                                    var OverlaySpeculars = new List<ModelAnimationTagResource.GroupMember.SpecularFrame>();

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type6.RealFrameMarkerOffset) // until the next block of whatever
                                    {
                                        PositionFramesCountPerNode.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.PositionFramesCountPerNode>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type6.RotationFramesOffset)
                                    {
                                        RealFrameMarker.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.RealFrameMarker>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type6.PositionFramesOffset)
                                    {
                                        OverlayRotations.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.RotationFrame>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type6.SpecularFramesOffset)
                                    {
                                        OverlayPositions.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.PositionFrame>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < footerOffset)
                                    {
                                        OverlaySpeculars.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext));
                                    }

                                    foreach (var a in PositionFramesCountPerNode)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                    }

                                    foreach (var a in RealFrameMarker)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                        newResourceGroupStream.Write(a.Y);
                                        newResourceGroupStream.Write(a.Z);
                                        newResourceGroupStream.Write(a.W);
                                    }

                                    foreach (var a in OverlayRotations)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                        newResourceGroupStream.Write(a.Y);
                                        newResourceGroupStream.Write(a.Z);
                                        newResourceGroupStream.Write(a.W);
                                    }

                                    foreach (var a in OverlayPositions)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                        newResourceGroupStream.Write(a.Y);
                                        newResourceGroupStream.Write(a.Z);
                                    }

                                    foreach (var a in OverlaySpeculars)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                    }
                                    break;
                                }

                            case ModelAnimationTagResource.GroupMemberAnimationType.Type7:
                                {
                                    var Type7 = CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.AnimationHeaderOverlay>(dataContext);

                                    newResourceGroupStream.Write((sbyte)Type7.HeaderStructType);
                                    newResourceGroupStream.Write((sbyte)Type7.RotationNodeCount);
                                    newResourceGroupStream.Write((sbyte)Type7.PositionNodeCount);
                                    newResourceGroupStream.Write((sbyte)Type7.SpecularNodeCount);
                                    newResourceGroupStream.Write((uint)Type7.Unknown0);
                                    newResourceGroupStream.Write((uint)Type7.Unknown1);
                                    newResourceGroupStream.Write((uint)Type7.PositionFramesCountPerNodeOffset);
                                    newResourceGroupStream.Write((uint)Type7.RealFrameMarkerOffset);
                                    newResourceGroupStream.Write((uint)Type7.RealFrameMarkerOffset1);
                                    newResourceGroupStream.Write((uint)Type7.RotationsRealFrameMarkersEndOffset);
                                    newResourceGroupStream.Write((uint)Type7.PositionsRealFrameMarkersEndOffset);
                                    newResourceGroupStream.Write((uint)Type7.RotationFramesOffset);
                                    newResourceGroupStream.Write((uint)Type7.PositionFramesOffset);
                                    newResourceGroupStream.Write((uint)Type7.SpecularFramesOffset);
                                    newResourceGroupStream.Write((uint)Type7.UselessPadding);

                                    var PositionFramesCountPerNode = new List<ModelAnimationTagResource.GroupMember.PositionFramesCountPerNode>();
                                    var RealFrameMarker = new List<ModelAnimationTagResource.GroupMember.RealFrameMarker>();
                                    var OverlayRotations = new List<ModelAnimationTagResource.GroupMember.RotationFrame>();
                                    var OverlayPositions = new List<ModelAnimationTagResource.GroupMember.PositionFrame>();
                                    var OverlaySpeculars = new List<ModelAnimationTagResource.GroupMember.SpecularFrame>();

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type7.RealFrameMarkerOffset) // until the next block of whatever
                                    {
                                        PositionFramesCountPerNode.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.PositionFramesCountPerNode>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type7.RotationFramesOffset)
                                    {
                                        RealFrameMarker.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.RealFrameMarker>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type7.PositionFramesOffset)
                                    {
                                        OverlayRotations.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.RotationFrame>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < memberOffset + overlayOffset + Type7.SpecularFramesOffset)
                                    {
                                        OverlayPositions.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.PositionFrame>(dataContext));
                                    }

                                    while (reader.BaseStream.Position < footerOffset)
                                    {
                                        OverlaySpeculars.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext));
                                    }

                                    foreach (var a in PositionFramesCountPerNode)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                    }

                                    foreach (var a in RealFrameMarker)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                        newResourceGroupStream.Write(a.Y);
                                        newResourceGroupStream.Write(a.Z);
                                        newResourceGroupStream.Write(a.W);
                                    }

                                    foreach (var a in OverlayRotations)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                        newResourceGroupStream.Write(a.Y);
                                        newResourceGroupStream.Write(a.Z);
                                        newResourceGroupStream.Write(a.W);
                                    }

                                    foreach (var a in OverlayPositions)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                        newResourceGroupStream.Write(a.Y);
                                        newResourceGroupStream.Write(a.Z);
                                    }

                                    foreach (var a in OverlaySpeculars)
                                    {
                                        newResourceGroupStream.Write(a.X);
                                    }
                                    break;
                                }

                            case ModelAnimationTagResource.GroupMemberAnimationType.Type8:
                                {
                                    var Type8 = CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.AnimationHeaderType8>(dataContext);

                                    newResourceGroupStream.Write((sbyte)Type8.HeaderStructType);
                                    newResourceGroupStream.Write((sbyte)Type8.RotationNodeCount);
                                    newResourceGroupStream.Write((sbyte)Type8.PositionNodeCount);
                                    newResourceGroupStream.Write((sbyte)Type8.SpecularNodeCount);
                                    newResourceGroupStream.Write((uint)Type8.Unknown0);
                                    newResourceGroupStream.Write((uint)Type8.Unknown1);
                                    newResourceGroupStream.Write((uint)Type8.PositionFramesOffset);
                                    newResourceGroupStream.Write((uint)Type8.SpecularFramesOffset);
                                    newResourceGroupStream.Write((uint)Type8.FrameCountPerNode);
                                    newResourceGroupStream.Write((uint)Type8.Unknown2);
                                    newResourceGroupStream.Write((uint)Type8.Unknown3);

                                    var OverlayRotationsFloat = new List<ModelAnimationTagResource.GroupMember.RotationFrameFloat>();
                                    var OverlayPositions = new List<ModelAnimationTagResource.GroupMember.PositionFrame>();
                                    var OverlaySpeculars = new List<ModelAnimationTagResource.GroupMember.SpecularFrame>();

                                    for (int nodeIndex = 0; nodeIndex < Type8.RotationNodeCount; nodeIndex++)
                                    {
                                        var rotationFrames = new List<ModelAnimationTagResource.GroupMember.RotationFrame>();
                                        for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
                                        {
                                            OverlayRotationsFloat.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.RotationFrameFloat>(dataContext));
                                            newResourceGroupStream.Write(OverlayRotationsFloat[OverlayRotationsFloat.Count - 1].X);
                                            newResourceGroupStream.Write(OverlayRotationsFloat[OverlayRotationsFloat.Count - 1].Y);
                                            newResourceGroupStream.Write(OverlayRotationsFloat[OverlayRotationsFloat.Count - 1].Z);
                                            newResourceGroupStream.Write(OverlayRotationsFloat[OverlayRotationsFloat.Count - 1].W);
                                        }
                                    }

                                    for (int nodeIndex = 0; nodeIndex < Type8.PositionNodeCount; nodeIndex++)
                                    {
                                        var positionFrames = new List<ModelAnimationTagResource.GroupMember.PositionFrame>();
                                        for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
                                        {
                                            OverlayPositions.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.PositionFrame>(dataContext));
                                            newResourceGroupStream.Write(OverlayPositions[OverlayPositions.Count - 1].X);
                                            newResourceGroupStream.Write(OverlayPositions[OverlayPositions.Count - 1].Y);
                                            newResourceGroupStream.Write(OverlayPositions[OverlayPositions.Count - 1].Z);
                                        }
                                    }

                                    for (int nodeIndex = 0; nodeIndex < Type8.SpecularNodeCount; nodeIndex++)
                                    {
                                        var specularFrames = new List<ModelAnimationTagResource.GroupMember.SpecularFrame>();
                                        for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
                                        {
                                            OverlaySpeculars.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext));
                                            newResourceGroupStream.Write(OverlaySpeculars[OverlaySpeculars.Count - 1].X);
                                        }
                                    }
                                    break;
                                }
                        }

                        //
                        // Footer main
                        //

                        var animation = blamJmad.Animations.Find(x =>
                        x.ResourceGroupIndex == resourceGroupIndex &
                        x.ResourceGroupMemberIndex == resourceGroups[resourceGroupIndex].GroupMembers.IndexOf(member));

                        reader.BaseStream.Position = memberOffset + overlayOffset + footerOffset;

                        try
                        {
                            var footer = new List<ModelAnimationTagResource.GroupMember.SpecularFrame>();

                            if (animation.Type == ModelAnimationGraph.FrameType.Base)
                            {
                                if (member.NodeCount > 31)
                                {
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // static rot
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // static rot

                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // static pos
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // static pos

                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // static spec
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // static spec

                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim rot
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim rot

                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim pos
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim pos

                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim spec
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim spec
                                }
                                else
                                {
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // static rot
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // static pos
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // static spec

                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim rot
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim pos
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim spec
                                }
                            }
                            else // Overlay animation, non Base.
                            {
                                if (member.NodeCount > 31)
                                {
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim rot
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim rot

                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim pos
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim pos

                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim spec
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim spec
                                }
                                else
                                {
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim rot
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim pos
                                    footer.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.SpecularFrame>(dataContext)); // anim spec
                                }
                            }
                            foreach (var a in footer)
                            {
                                newResourceGroupStream.Write(a.X);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        //
                        // FrameInfoType DxDy or DxDyDyaw frames
                        //

                        var frameInfoDxDyFrames = new List<ModelAnimationTagResource.GroupMember.FrameInfoDxDy>();
                        var frameInfoDxDyDyawFrames = new List<ModelAnimationTagResource.GroupMember.FrameInfoDxDyDyaw>();

                        if (animation.FrameInfoType == ModelAnimationGraph.FrameMovementDataType.DxDy)
                        {
                            for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
                            {
                                frameInfoDxDyFrames.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.FrameInfoDxDy>(dataContext));
                            }

                            if (headerGlobal.PositionNodeCount > 1)
                            {
                                for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
                                {
                                    frameInfoDxDyDyawFrames.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.FrameInfoDxDyDyaw>(dataContext));
                                }
                            }
                        }
                        else if (animation.FrameInfoType == ModelAnimationGraph.FrameMovementDataType.DxDyDyaw)
                        {
                            frameInfoDxDyDyawFrames = new List<ModelAnimationTagResource.GroupMember.FrameInfoDxDyDyaw>();

                            for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
                            {
                                frameInfoDxDyDyawFrames.Add(CacheContext.Deserializer.Deserialize<ModelAnimationTagResource.GroupMember.FrameInfoDxDyDyaw>(dataContext));
                            }
                        }
                        foreach (var a in frameInfoDxDyFrames)
                        {
                            newResourceGroupStream.Write(a.X);
                            newResourceGroupStream.Write(a.Y);
                        }
                        foreach (var a in frameInfoDxDyDyawFrames)
                        {
                            newResourceGroupStream.Write(a.X);
                            newResourceGroupStream.Write(a.Y);
                            newResourceGroupStream.Write(a.Z);
                        }

                        //
                        // Padding
                        //

                        try
                        {
                            while (reader.BaseStream.Position % 0x10 != 0x0)
                            {
                                reader.ReadUInt32();
                                newResourceGroupStream.Write(0x00000000);
                            }
                        }
                        catch (Exception) { }
                    }

                    //
                    // Add new ElDorado model_animation_graph resource
                    //

                    newResourceGroupStream.BaseStream.Position = 0;

                    using (newResourceGroupStream)
                    {
                        InjectAnimation(
                            CacheContext.Serializer, CacheContext.Deserializer,
                            blamJmad, resourceGroupIndex,
                            newStream,
                            resourceGroups[resourceGroupIndex],
                            resourcesFixups);
                    }

                    Console.Write("."); // Loading bar.
                }

                // There's an extra 0x4 bytes at the end for some reason which need to be removed
                var definition = new byte[blamJmad.ResourceGroups[resourceGroupIndex].Resource.DefinitionData.Length - 0x4];
                

                for (int j = 0; j < definition.Length; j++)
                {
                    definition[j] = blamJmad.ResourceGroups[resourceGroupIndex].Resource.DefinitionData[j];
                }

                blamJmad.ResourceGroups[resourceGroupIndex].Resource.DefinitionData = definition;
            }

            //
            // Finalize the new ElDorado model_animation_graph tag
            //

            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating the new ElDorado model_animation_graph tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("jmad")]);

                Console.WriteLine("done.");
            }
            else
            {
                newTag = edTag;
            }

            CacheContext.TagNames[newTag.Index] = blamTagName;

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                Console.Write($"Writing \"{CacheContext.TagNames[newTag.Index]}.model_animation_graph\" tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, blamJmad);

                Console.WriteLine("done.");
            }
            
            //
            // Done!
            //

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.model_animation_graph\" successfully!");

            return newTag;
        }

        private CachedTagInstance PortShader(string blamTagName, CachedTagInstance edTag = null)
        {
            var isNew = (edTag == null);

            //
            // Verify and load the Blam shader
            //

            CacheFile.IndexItem blamTag = null;

            Console.WriteLine("Verifying Blam shader tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if ((tag.ParentClass == "rm") && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam shader tag does not exist: " + blamTagName);
                return null;
            }

            //
            // Verify ED shader tag
            //
            
            if (!isNew)
            {
                Console.WriteLine("Verifying ElDorado shader tag index...");
                
                if (edTag.IsInGroup("rm  "))
                {
                    Console.WriteLine($"Specified tag index is not a shader: 0x{edTag.Index:X4}");
                    return null;
                }
            }

            //
            // Load the Blam shader tag
            //

            var blamShader = new BlamCore.Legacy.Halo3Beta.shader(BlamCache, blamTag.Offset);

            var templateItem = BlamCache.IndexItems.Find(i =>
                i.ID == blamShader.Properties[0].TemplateTagID);

            var template = new BlamCore.Legacy.Halo3Beta.render_method_template(BlamCache, templateItem.Offset);

            //
            // Determine the Blam shader's base bitmap
            //

            var bitmapIndex = -1;
            var bitmapArgName = "";

            for (var i = 0; i < template.UsageBlocks.Count; i++)
            {
                var entry = template.UsageBlocks[i];

                if (entry.Usage.StartsWith("base_map") ||
                    entry.Usage.StartsWith("diffuse_map") ||
                    entry.Usage == "foam_texture")
                {
                    bitmapIndex = i;
                    bitmapArgName = entry.Usage;
                    break;
                }
            }

            //
            // Load and decode the Blam shader's base bitmap
            //

            if (bitmapIndex == -1)
                return CacheContext.GetTag(0x101F);

            var bitmItem = BlamCache.IndexItems.Find(i =>
                i.ID == blamShader.Properties[0].ShaderMaps[bitmapIndex].BitmapTagID);

            var bitm = new BlamCore.Legacy.Halo3Beta.bitmap(BlamCache, bitmItem.Offset);

            var submap = bitm.Bitmaps[0];

            byte[] raw;

            if (bitm.RawChunkBs.Count > 0)
            {
                int rawID = bitm.RawChunkBs[submap.InterleavedIndex].RawID;
                byte[] buffer = BlamCache.GetRawFromID(rawID);
                raw = new byte[submap.RawSize];
                Array.Copy(buffer, submap.Index2 * submap.RawSize, raw, 0, submap.RawSize);
            }
            else
            {
                int rawID = bitm.RawChunkAs[0].RawID;
                raw = BlamCache.GetRawFromID(rawID, submap.RawSize);
            }

            var vHeight = submap.VirtualHeight;
            var vWidth = submap.VirtualWidth;

            var ms = new MemoryStream();
            var bw = new BinaryWriter(ms);

            if (submap.Flags.Values[3])
                raw = DxtDecoder.ConvertToLinearTexture(raw, vWidth, vHeight, submap.Format);

            if (submap.Format != BitmapFormat.A8R8G8B8)
                for (int i = 0; i < raw.Length; i += 2)
                    Array.Reverse(raw, i, 2);
            else
                for (int i = 0; i < (raw.Length); i += 4)
                    Array.Reverse(raw, i, 4);

            new DdsImage(submap).Write(bw);
            bw.Write(raw);

            raw = ms.ToArray();

            bw.Close();
            bw.Dispose();

            //
            // ElDorado Serialization
            //

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                //
                // Create the new ElDorado bitmap
                //

                var newBitm = CacheContext.TagCache.DuplicateTag(cacheStream, CacheContext.GetTag(0x101F));

                var bitmap = new Bitmap
                {
                    Flags = Bitmap.RuntimeFlags.UseResource,
                    Sequences = new List<Bitmap.Sequence>
                    {
                        new Bitmap.Sequence
                        {
                            Name = "",
                            FirstBitmapIndex = 0,
                            BitmapCount = 1
                        }
                    },
                    Images = new List<Bitmap.Image>
                    {
                        new Bitmap.Image
                        {
                            Signature = new Tag("bitm").Value,
                            Unknown28 = -1
                        }
                    },
                    Resources = new List<Bitmap.BitmapResource>
                    {
                        new Bitmap.BitmapResource()
                    }
                };

                using (var imageStream = new MemoryStream(raw))
                {
                    var injector = new BitmapDdsInjector(CacheContext);
                    imageStream.Seek(0, SeekOrigin.Begin);
                    injector.InjectDds(CacheContext.Serializer, CacheContext.Deserializer, bitmap, 0, imageStream);
                }

                var context = new TagSerializationContext(cacheStream, CacheContext, newBitm);
                CacheContext.Serializer.Serialize(context, bitmap);

                //
                // Finalize new ElDorado physics_model tag
                //

                CachedTagInstance newTag;

                if (isNew)
                {
                    Console.Write($"Allocating new ElDorado {blamTag.ClassName} tag...");

                    // adjutant code hack
                    var chars = new List<char>(blamTag.ClassCode);
                    while (chars.Count < 4) chars.Add(' ');

                    newTag = CacheContext.TagCache.DuplicateTag(cacheStream, CacheContext.GetTag(0x101F));

                    Console.WriteLine("done.");
                }
                else
                {
                    newTag = edTag;
                }

                CacheContext.TagNames[newTag.Index] = blamTagName;
                
                //
                // Create the new ElDorado shader
                //

                Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.{blamTag.ClassName}\" tag data...");
                
                context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                var shader = CacheContext.Deserializer.Deserialize<Shader>(context);
                shader.ShaderProperties[0].ShaderMaps[0].Bitmap = newBitm;
                CacheContext.Serializer.Serialize(context, shader);

                //
                // Done!
                //

                Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.{blamTag.ClassName}\" successfully!");

                return newTag;
            }
        }

        private void ConvertStringId(ref StringId stringId)
        {
            var value = BlamCache.Strings.GetItemByID((int)stringId.Value);

            if (!CacheContext.StringIdCache.Contains(value))
                stringId = CacheContext.StringIdCache.AddString(value);
            else
                stringId = CacheContext.GetStringId(value);
        }

        private void ConvertVertices<T>(int count, Func<T> readVertex, Action<T> writeVertex)
        {
            for (var i = 0; i < count; i++)
                writeVertex(readVertex());
        }

        private void InjectAnimation(TagSerializer serializer, TagDeserializer deserializer, ModelAnimationGraph animation, int groupIndex, Stream animationStream, ModelAnimationTagResource resourceGroups, List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry> resourcesFixups)
        {
            ResourceSerializationContext resourceContext;
            ModelAnimationTagResource definition;

            var resource = new ResourceReference
            {
                DefinitionFixups = new List<ResourceDefinitionFixup>(),
                D3DObjectFixups = new List<D3DObjectFixup>(),
                Type = 4,
                Unknown68 = 1,
                OldLocationFlags = OldResourceLocationFlags.InResources
            };

            definition = new ModelAnimationTagResource();

            definition.GroupMembers = new List<ModelAnimationTagResource.GroupMember>();

            for (var i = 0; i < resourceGroups.GroupMembers.Count; i++)
            {
                var groupMember = resourceGroups.GroupMembers[i];

                definition.GroupMembers.Add(new ModelAnimationTagResource.GroupMember
                {
                    Checksum = groupMember.Checksum,
                    FrameCount = groupMember.FrameCount,
                    NodeCount = groupMember.NodeCount,
                    MovementDataType = groupMember.MovementDataType, // weird that sbyte doesn't work
                    BaseHeader = groupMember.BaseHeader,
                    OverlayHeader = groupMember.OverlayHeader,
                    Unknown1 = groupMember.Unknown1,
                    Unknown2 = groupMember.Unknown2,
                    OverlayOffset = groupMember.OverlayOffset,
                    Unknown3 = groupMember.Unknown3,
                    Unknown4 = groupMember.Unknown4,
                    FlagsOffset = groupMember.FlagsOffset,
                    AnimationData = new ResourceDataReference(
                        groupMember.AnimationData.Size,
                        new ResourceAddress(ResourceAddressType.Resource, resourcesFixups[groupIndex].Fixups[i].Offset))
                });
            }

            CacheContext.AddResource(resource, ResourceLocation.Resources, animationStream);

            animation.ResourceGroups[groupIndex].Resource = resource;

            resourceContext = new ResourceSerializationContext(resource);

            serializer.Serialize(resourceContext, definition);
        }
    }
}