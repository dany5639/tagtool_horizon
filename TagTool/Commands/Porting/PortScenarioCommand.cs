﻿using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Legacy.Base;
using BlamCore.TagDefinitions;
using System;
using System.Collections.Generic;

namespace TagTool.Commands.Porting
{
    class PortScenarioCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private CacheFile BlamCache { get; }

        public PortScenarioCommand(GameCacheContext cacheContext, CacheFile blamCache) :
            base(CommandFlags.None,
                
                "PortScenario",
                "Ports a Blam scenario tag to the current cache. WIP",
                
                "PortScenario [New <Blam Tag>] | [<Blam Tag> <ElDorado Tag>]",
                
                "Ports a Blam scenario tag to the current cache. WIP")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count < 2 || args.Count > 3)
                return false;

            bool isNew = false;
            if (args[0].ToLower() == "new")
            {
                isNew = true;
                args.RemoveAt(0);
            }

            var initialStringIDCount = CacheContext.StringIdCache.Strings.Count;

            //
            // Verify the Blam scenario tag
            //

            var blamTagName = args[0];

            CacheFile.IndexItem blamTag = null;

            Console.WriteLine("Verifying Blam scenario tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "scnr" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam scenario tag does not exist: " + args[0]);
                return false;
            }

            //
            // Verify the ED scenario tag
            //

            CachedTagInstance edTag = null;

            if (!isNew)
            {
                Console.WriteLine("Verifying ElDorado scenario tag index...");

                edTag = ArgumentParser.ParseTagSpecifier(CacheContext, args[1]);

                if (edTag.Group.Name != CacheContext.GetStringId("scenario"))
                {
                    Console.WriteLine("Specified tag index is not a scenario: " + args[1]);
                    return false;
                }
            }

            //
            // Load the Blam scenario tag
            //

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamScenario = blamDeserializer.Deserialize<Scenario>(blamContext);

            //
            // Convert tags and string ids
            //

            #region Convert tags and stringids
            foreach (var structureBsp in blamScenario.StructureBsps)
            {
                structureBsp.StructureBsp = ConvertTagReference(structureBsp.StructureBsp.Index);
                structureBsp.Design = ConvertTagReference(structureBsp.Design.Index);
                structureBsp.Lighting = ConvertTagReference(structureBsp.Lighting.Index);
                structureBsp.Cubemap = ConvertTagReference(structureBsp.Cubemap.Index);
                structureBsp.Wind = ConvertTagReference(structureBsp.Wind.Index);
            }

            blamScenario.Unknown = ConvertTagReference(blamScenario.Unknown.Index);

            foreach (var reference in blamScenario.SkyReferences)
            {
                reference.SkyObject = ConvertTagReference(reference.SkyObject.Index);
            }

            foreach (var zoneSet in blamScenario.ZoneSets)
            {
                ConvertStringId(ref zoneSet.Name);
            }

            foreach (var bspAtlas in blamScenario.BspAtlas)
            {
                ConvertStringId(ref bspAtlas.Name);
            }

            foreach (var campaignPlayer in blamScenario.CampaignPlayers)
            {
                ConvertStringId(ref campaignPlayer.PlayerRepresentationName);
            }

            foreach (var placement in blamScenario.Scenery)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

                ConvertStringId(ref placement.Variant);
            }

            foreach (var palette in blamScenario.SceneryPalette)
            {
                palette.Scenery = ConvertTagReference(palette.Scenery.Index);
            }

            foreach (var placement in blamScenario.Bipeds)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

                ConvertStringId(ref placement.Variant);
            }

            foreach (var palette in blamScenario.BipedPalette)
            {
                palette.Biped = ConvertTagReference(palette.Biped.Index);
            }

            foreach (var placement in blamScenario.Vehicles)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

                ConvertStringId(ref placement.Variant);

            }

            foreach (var palette in blamScenario.VehiclePalette)
            {
                palette.Vehicle = ConvertTagReference(palette.Vehicle.Index);
            }

            foreach (var placement in blamScenario.Equipment)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

            }

            foreach (var palette in blamScenario.EquipmentPalette)
            {
                palette.Equipment = ConvertTagReference(palette.Equipment.Index);
            }

            foreach (var placement in blamScenario.Weapons)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

                ConvertStringId(ref placement.Variant);

            }

            foreach (var palette in blamScenario.WeaponPalette)
            {
                palette.Weapon = ConvertTagReference(palette.Weapon.Index);
            }

            foreach (var placement in blamScenario.Machines)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

                ConvertStringId(ref placement.Variant);

            }

            foreach (var palette in blamScenario.MachinePalette)
            {
                palette.Machine = ConvertTagReference(palette.Machine.Index);
            }

            foreach (var placement in blamScenario.Terminals)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

                ConvertStringId(ref placement.Variant);

            }

            foreach (var palette in blamScenario.TerminalPalette)
            {
                palette.Terminal = ConvertTagReference(palette.Terminal.Index);
            }

            foreach (var placement in blamScenario.AlternateRealityDevices)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

                ConvertStringId(ref placement.Variant);

            }

            foreach (var palette in blamScenario.AlternateRealityDevicePalette)
            {
                palette.ArgDevice = ConvertTagReference(palette.ArgDevice.Index);
            }

            foreach (var placement in blamScenario.Controls)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

                ConvertStringId(ref placement.Variant);
            }

            foreach (var palette in blamScenario.ControlPalette)
            {
                palette.Control = ConvertTagReference(palette.Control.Index);
            }

            foreach (var placement in blamScenario.SoundScenery)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);
            }

            foreach (var palette in blamScenario.SoundSceneryPalette)
            {
                palette.SoundScenery = ConvertTagReference(palette.SoundScenery.Index);
            }

            foreach (var placement in blamScenario.Giants)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

                ConvertStringId(ref placement.Variant);

            }

            foreach (var palette in blamScenario.GiantPalette)
            {
                palette.Giant = ConvertTagReference(palette.Giant.Index);
            }

            foreach (var placement in blamScenario.EffectScenery)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);


                ConvertStringId(ref placement.Unknown5);
            }

            foreach (var palette in blamScenario.EffectSceneryPalette)
            {
                palette.EffectScenery = ConvertTagReference(palette.EffectScenery.Index);
            }

            foreach (var placement in blamScenario.LightVolumes)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);
            }

            foreach (var palette in blamScenario.LightVolumePalette)
            {
                palette.LightVolume = ConvertTagReference(palette.LightVolume.Index);
            }

            foreach (var sandboxObject in blamScenario.SandboxVehicles)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                ConvertStringId(ref sandboxObject.Name);
            }

            foreach (var sandboxObject in blamScenario.SandboxWeapons)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                ConvertStringId(ref sandboxObject.Name);
            }

            foreach (var sandboxObject in blamScenario.SandboxEquipment)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                ConvertStringId(ref sandboxObject.Name);
            }

            foreach (var sandboxObject in blamScenario.SandboxScenery)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                ConvertStringId(ref sandboxObject.Name);
            }

            foreach (var sandboxObject in blamScenario.SandboxTeleporters)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                ConvertStringId(ref sandboxObject.Name);
            }

            foreach (var sandboxObject in blamScenario.SandboxGoalObjects)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                ConvertStringId(ref sandboxObject.Name);
            }

            foreach (var sandboxObject in blamScenario.SandboxSpawning)
            {
                sandboxObject.Object = ConvertTagReference(sandboxObject.Object.Index);

                ConvertStringId(ref sandboxObject.Name);
            }

            foreach (var softCeiling in blamScenario.SoftCeilings)
            {
                ConvertStringId(ref softCeiling.Name);

            }

            foreach (var profile in blamScenario.PlayerStartingProfile)
            {
                profile.PrimaryWeapon = ConvertTagReference(profile.PrimaryWeapon.Index);
                profile.SecondaryWeapon = ConvertTagReference(profile.SecondaryWeapon.Index);
            }

            foreach (var triggerVolume in blamScenario.TriggerVolumes)
            {
                ConvertStringId(ref triggerVolume.Name);

                ConvertStringId(ref triggerVolume.NodeName);
            }

            foreach (var squad in blamScenario.Squads)
            {
                ConvertStringId(ref squad.ModuleId);

                foreach (var formation in squad.SpawnFormations)
                {
                    ConvertStringId(ref formation.Name);

                    ConvertStringId(ref formation.FormationType);

                    ConvertStringId(ref formation.InitialState);

                    foreach (var point in formation.Points)
                    {
                        ConvertStringId(ref point.ActivityName);
                    }
                }

                foreach (var spawnPoint in squad.SpawnPoints)
                {
                    ConvertStringId(ref spawnPoint.Name);

                    ConvertStringId(ref spawnPoint.ActorVariantName);

                    ConvertStringId(ref spawnPoint.VehicleVariantName);

                    ConvertStringId(ref spawnPoint.ActivityName);

                    foreach (var point in spawnPoint.Points)
                    {
                        ConvertStringId(ref point.ActivityName);
                    }
                }

                foreach (var cell in squad.DesignerCells)
                {
                    ConvertStringId(ref cell.Name);

                    ConvertStringId(ref cell.VehicleVariant);
                }

                foreach (var cell in squad.TemplatedCells)
                {
                    ConvertStringId(ref cell.Name);

                    ConvertStringId(ref cell.VehicleVariant);
                }
            }

            foreach (var element in blamScenario.Unknown84)
            {
                ConvertStringId(ref element.Unknown);
            }

            for (var i = 0; i < blamScenario.DecalPalette.Count; i++)
                blamScenario.DecalPalette[i] = ConvertTagReference(blamScenario.DecalPalette[i].Index);

            for (var i = 0; i < blamScenario.StylePalette.Count; i++)
                blamScenario.StylePalette[i] = ConvertTagReference(blamScenario.StylePalette[i].Index);

            foreach (var squad in blamScenario.Squads)
            {
                squad.SquadTemplate = ConvertTagReference(squad.SquadTemplate.Index);
            }

            for (var i = 0; i < blamScenario.CharacterPalette.Count; i++)
                blamScenario.CharacterPalette[i] = ConvertTagReference(blamScenario.CharacterPalette[i].Index);

            foreach (var datum in blamScenario.AiPathfindingData)
            {
                foreach (var element in datum.Unknown9)
                {
                    ConvertStringId(ref element.Unknown);
                }
            }

            for (var i = 0; i < blamScenario.ScriptReferences.Count; i++)
                blamScenario.ScriptReferences[i] = ConvertTagReference(blamScenario.ScriptReferences[i].Index);

            foreach (var flag in blamScenario.CutsceneFlags)
            {
                ConvertStringId(ref flag.Name);
            }

            foreach (var title in blamScenario.CutsceneTitles)
            {
                ConvertStringId(ref title.Name);
            }

            blamScenario.CustomObjectNameStrings = ConvertTagReference(blamScenario.CustomObjectNameStrings.Index);
            blamScenario.ChapterTitleStrings = ConvertTagReference(blamScenario.ChapterTitleStrings.Index);

            foreach (var resource in blamScenario.ScenarioResources)
            {
                for (var i = 0; i < resource.ScriptSource.Count; i++)
                    resource.ScriptSource[i] = ConvertTagReference(resource.ScriptSource[i].Index);

                for (var i = 0; i < resource.AiResources.Count; i++)
                    resource.AiResources[i] = ConvertTagReference(resource.AiResources[i].Index);

                foreach (var reference in resource.References)
                {
                    reference.SceneryResource = ConvertTagReference(reference.SceneryResource.Index);

                    for (var i = 0; i < reference.OtherScenery.Count; i++)
                        reference.OtherScenery[i] = ConvertTagReference(reference.OtherScenery[i].Index);

                    reference.BipedsResource = ConvertTagReference(reference.BipedsResource.Index);

                    for (var i = 0; i < reference.OtherBipeds.Count; i++)
                        reference.OtherBipeds[i] = ConvertTagReference(reference.OtherBipeds[i].Index);

                    reference.VehiclesResource = ConvertTagReference(reference.VehiclesResource.Index);
                    reference.EquipmentResource = ConvertTagReference(reference.EquipmentResource.Index);
                    reference.WeaponsResource = ConvertTagReference(reference.WeaponsResource.Index);
                    reference.SoundSceneryResource = ConvertTagReference(reference.SoundSceneryResource.Index);
                    reference.LightsResource = ConvertTagReference(reference.LightsResource.Index);

                    reference.DevicesResource = ConvertTagReference(reference.DevicesResource.Index);

                    for (var i = 0; i < reference.OtherDevices.Count; i++)
                        reference.OtherDevices[i] = ConvertTagReference(reference.OtherDevices[i].Index);

                    reference.EffectSceneryResource = ConvertTagReference(reference.EffectSceneryResource.Index);

                    reference.DecalsResource = ConvertTagReference(reference.DecalsResource.Index);

                    for (var i = 0; i < reference.OtherDecals.Count; i++)
                        reference.OtherDecals[i] = ConvertTagReference(reference.OtherDecals[i].Index);

                    reference.CinematicsResource = ConvertTagReference(reference.CinematicsResource.Index);
                    reference.TriggerVolumesResource = ConvertTagReference(reference.TriggerVolumesResource.Index);
                    reference.ClusterDataResource = ConvertTagReference(reference.ClusterDataResource.Index);
                    reference.CommentsResource = ConvertTagReference(reference.CommentsResource.Index);
                    reference.CreatureResource = ConvertTagReference(reference.CreatureResource.Index);
                    reference.StructureLightingResource = ConvertTagReference(reference.StructureLightingResource.Index);

                    reference.DecoratorsResource = ConvertTagReference(reference.DecoratorsResource.Index);

                    for (var i = 0; i < reference.OtherDecorators.Count; i++)
                        reference.OtherDecorators[i] = ConvertTagReference(reference.OtherDecorators[i].Index);

                    reference.SkyReferencesResource = ConvertTagReference(reference.SkyReferencesResource.Index);
                    reference.CubemapResource = ConvertTagReference(reference.CubemapResource.Index);
                }
            }

            foreach (var mapping in blamScenario.UnitSeatsMapping)
            {
                mapping.Unit = ConvertTagReference(mapping.Unit.Index);
            }

            foreach (var palette in blamScenario.BackgroundSoundEnvironmentPalette)
            {
                ConvertStringId(ref palette.Name);

                palette.SoundEnvironment = ConvertTagReference(palette.SoundEnvironment.Index);
                palette.BackgroundSound = ConvertTagReference(palette.BackgroundSound.Index);
                palette.InsideClusterSound = ConvertTagReference(palette.InsideClusterSound.Index);
            }

            foreach (var fog in blamScenario.Fog)
            {
                ConvertStringId(ref fog.Name);

            }

            foreach (var cameraFx in blamScenario.CameraFx)
            {
                ConvertStringId(ref cameraFx.Name);

                cameraFx.CameraFx = ConvertTagReference(cameraFx.CameraFx.Index);
            }

            foreach (var data in blamScenario.ScenarioClusterData)
            {
                data.Bsp = ConvertTagReference(data.Bsp.Index);
            }

            foreach (var datum in blamScenario.SpawnData)
            {
                foreach (var spawnZone in datum.StaticRespawnZones)
                {
                    ConvertStringId(ref spawnZone.Name);
                }

                foreach (var spawnZone in datum.StaticInitialSpawnZones)
                {
                    ConvertStringId(ref spawnZone.Name);

                }
            }

            blamScenario.SoundEffectsCollection = ConvertTagReference(blamScenario.SoundEffectsCollection.Index);

            foreach (var placement in blamScenario.Crates)
            {
                ConvertStringId(ref placement.UniqueName);

                ConvertStringId(ref placement.ChildName);

                ConvertStringId(ref placement.Unknown5);

                ConvertStringId(ref placement.Variant);

            }

            foreach (var palette in blamScenario.CratePalette)
                palette.Crate = ConvertTagReference(palette.Crate.Index);

            for (var i = 0; i < blamScenario.FlockPalette.Count; i++)
                blamScenario.FlockPalette[i] = ConvertTagReference(blamScenario.FlockPalette[i].Index);

            foreach (var flock in blamScenario.Flocks)
            {
                ConvertStringId(ref flock.Name);

            }

            blamScenario.SubtitleStrings = ConvertTagReference(blamScenario.SubtitleStrings.Index);

            foreach (var palette in blamScenario.CreaturePalette)
                palette.Creature = ConvertTagReference(palette.Creature.Index);

            blamScenario.TerritoryLocationNameStrings = ConvertTagReference(blamScenario.TerritoryLocationNameStrings.Index);

            for (var i = 0; i < blamScenario.MissionDialogue.Count; i++)
                blamScenario.MissionDialogue[i] = ConvertTagReference(blamScenario.MissionDialogue[i].Index);

            blamScenario.ObjectiveStrings = ConvertTagReference(blamScenario.ObjectiveStrings.Index);

            foreach (var interpolator in blamScenario.Interpolators)
            {
                ConvertStringId(ref interpolator.Name);

                ConvertStringId(ref interpolator.AcceleratorName);

                ConvertStringId(ref interpolator.MultiplierName);
            }

            foreach (var simulation in blamScenario.SimulationDefinitionTable)
                simulation.Tag = ConvertTagReference(simulation.Tag.Index);

            blamScenario.DefaultCameraFx = ConvertTagReference(blamScenario.DefaultCameraFx.Index);
            blamScenario.DefaultScreenFx = ConvertTagReference(blamScenario.DefaultScreenFx.Index);
            blamScenario.SkyParameters = ConvertTagReference(blamScenario.SkyParameters.Index);
            blamScenario.GlobalLighing = ConvertTagReference(blamScenario.GlobalLighing.Index);
            blamScenario.Lightmap = ConvertTagReference(blamScenario.Lightmap.Index);
            blamScenario.PerformanceThrottles = ConvertTagReference(blamScenario.PerformanceThrottles.Index);

            foreach (var objective in blamScenario.AiObjectives)
            {
                ConvertStringId(ref objective.Name);

                foreach (var role in objective.Roles)
                {
                    ConvertStringId(ref role.Task);
                }
            }

            foreach (var zoneSet in blamScenario.DesignerZoneSets)
            {
                ConvertStringId(ref zoneSet.Name);
            }

            for (var i = 0; i < blamScenario.Cinematics.Count; i++)
                blamScenario.Cinematics[i] = ConvertTagReference(blamScenario.Cinematics[i].Index);

            foreach (var lighting in blamScenario.CinematicLighting)
            {
                ConvertStringId(ref lighting.Name);

                lighting.CinematicLight = ConvertTagReference(lighting.CinematicLight.Index);
            }

            foreach (var element in blamScenario.Unknown147)
            {
                ConvertStringId(ref element.Unknown4);
            }

            blamScenario.MissionVisionModeEffect = ConvertTagReference(blamScenario.MissionVisionModeEffect.Index);
            blamScenario.MissionVisionMode = ConvertTagReference(blamScenario.MissionVisionMode.Index);
            #endregion

            //
            // Finalize the new ElDorado scenario tag
            //

            #region Set some default tags used from mainmenu 0x27C3
            for (int i = 0; i < blamScenario.SkyReferences.Count; i++)
            {
                blamScenario.SkyReferences[i].SkyObject = CacheContext.GetTag(0x27C6);
            }

            blamScenario.SkyParameters = CacheContext.GetTag(0x27F7); 
            blamScenario.GlobalLighing = CacheContext.GetTag(0x27F8);

            //
            // Null scripts as they do not convert
            //

            blamScenario.ScriptExpressions = new List<BlamCore.Scripting.ScriptExpression>();
            blamScenario.ScriptingData = new List<Scenario.ScriptingDatum>();
            blamScenario.ScriptReferences = new List<CachedTagInstance>();
            blamScenario.Scripts = new List<BlamCore.Scripting.Script>();
            blamScenario.ScriptStrings = new byte[0];
            blamScenario.Globals = new List<BlamCore.Scripting.ScriptGlobal>();

            blamScenario.MapType = MapTypeValue.Multiplayer; 
            blamScenario.MapSubType = MapSubTypeValue.None; // scenario definition needs to be changed, it's different in HO to ODST

            foreach (var objectNames in blamScenario.ObjectNames) // failed attempt to fix None not working
            {
                if (objectNames.ObjectTypeOld == GameObjectTypeHalo3ODST.None)
                    objectNames.ObjectTypeNew = GameObjectTypeHaloOnline.None;
            }

#endregion

            #region Add spawnpoint
            blamScenario.SceneryPalette = new List<Scenario.SceneryPaletteBlock>();
            blamScenario.Scenery = new List<Scenario.SceneryBlock>();

            CachedTagInstance mp_spawnpoint = CacheContext.GetTag(0x2E90);  // default spawnpoint; neutral; default 0x2E90

            foreach (var tag2 in CacheContext.TagCache.Index.FindAllInGroup("scen"))
            {
                if (CacheContext.TagNames[tag2.Index].Contains("mp_spawn_point"))
                {
                    mp_spawnpoint = tag2;
                    break;
                }
            }

            blamScenario.SceneryPalette.Add(new Scenario.SceneryPaletteBlock { Scenery = mp_spawnpoint });

            float spawnpointX = 18f;
            float spawnpointY = 0f;
            float spawnpointZ = 0f;

            float spawnpointI = 0f;
            float spawnpointJ = 0f;
            float spawnpointK = 0f;

            var scenerySpawnpointDefault = new Scenario.SceneryBlock
            {
                PaletteIndex = (short)blamScenario.SceneryPalette.IndexOf(blamScenario.SceneryPalette.Find(x => x.Scenery == mp_spawnpoint)),
                NameIndex = 0,
                PlacementFlags = Scenario.ObjectPlacementFlags.None,
                Position = new RealPoint3d(spawnpointX, spawnpointY, spawnpointZ),
                Rotation = new RealEulerAngles3d(Angle.FromDegrees(spawnpointI), Angle.FromDegrees(spawnpointJ), Angle.FromDegrees(spawnpointK)),
                UniqueIdIndex = 49096,
                UniqueIdSalt = 8,
                ObjectTypeOld = GameObjectTypeHalo3ODST.Scenery,
                ObjectTypeNew = GameObjectTypeHaloOnline.Scenery,
                Source = Scenario.SceneryBlock.SourceValue.Editor,
                Unknown3 = -1,
                ParentNameIndex = -1,
                AllowedZoneSets = 1,
                Unknown12 = -1,
                Team = Scenario.SceneryBlock.TeamValue.Neutral,
                Unknown18 = 0
            };

            blamScenario.Scenery.Add(scenerySpawnpointDefault);

            Console.WriteLine("Added Spawnpoints");
            #endregion

            #region Add Camera for use as mainmenu map. Change map type to Mainmenu
            float cameraX = 13.95461f;
            float cameraY = 4.51311f;
            float cameraZ = 0.6763648f;
            float cameraI = 139.1801f;
            float cameraJ = 14.51703f;
            float cameraK = -12.21716f;
            blamScenario.CutsceneCameraPoints = new List<Scenario.CutsceneCameraPoint>();
            blamScenario.CutsceneCameraPoints.Add(new Scenario.CutsceneCameraPoint
            {
                Name = "main_menu_cam_point_0",
                Position = new RealPoint3d(cameraX, cameraY, cameraZ),
                Orientation = new RealEulerAngles3d(
                    Angle.FromDegrees(cameraI),
                    Angle.FromDegrees(cameraJ),
                    Angle.FromDegrees(cameraK)
                )
            });
            #endregion

            #region Nuke other crap that causes it to crash
            blamScenario.ChapterTitleStrings = null;
            blamScenario.CustomObjectNameStrings = null;
            blamScenario.DefaultCameraFx = null;
            blamScenario.DefaultScreenFx = null;
            blamScenario.MissionVisionMode = null;
            blamScenario.MissionVisionModeEffect = null;
            blamScenario.MissionVisionModeTheaterEffect = null;
            blamScenario.ObjectiveStrings = null;
            blamScenario.PerformanceThrottles = null;
            blamScenario.ScenarioPda = null;
            blamScenario.SoundEffectsCollection = null;
            blamScenario.SubtitleStrings = null;
            blamScenario.TerritoryLocationNameStrings = null;
            blamScenario.Unknown133 = null;
            blamScenario.Unknown156 = null;
            blamScenario.Unknown = null;

            blamScenario.CharacterPalette = new List<CachedTagInstance>();
            blamScenario.Cinematics = new List<CachedTagInstance>();
            blamScenario.FlockPalette = new List<CachedTagInstance>();
            blamScenario.MissionDialogue = new List<CachedTagInstance>();
            blamScenario.Unknown155 = new List<CachedTagInstance>();

            blamScenario.LocalNorth = new Angle();
            blamScenario.AlternateRealityDevices = new List<Scenario.AlternateRealityDevice>();
            blamScenario.AlternateRealityDevicePalette = new List<Scenario.AlternateRealityDevicePaletteBlock>();
            blamScenario.Bipeds = new List<Scenario.Biped>();
            blamScenario.BipedPalette = new List<Scenario.BipedPaletteBlock>();
            blamScenario.BspAtlas = new List<Scenario.BspAtlasBlock>();
            blamScenario.CameraFx = new List<Scenario.CameraFxBlock>();
            blamScenario.CampaignPlayers = new List<Scenario.CampaignPlayer>();
            blamScenario.CinematicLighting = new List<Scenario.CinematicLightingBlock>();
            blamScenario.Controls = new List<Scenario.Control>();
            blamScenario.ControlPalette = new List<Scenario.ControlPaletteBlock>();
            blamScenario.Crates = new List<Scenario.Crate>();
            blamScenario.CratePalette = new List<Scenario.CratePaletteBlock>();
            blamScenario.CreaturePalette = new List<Scenario.CreaturePaletteBlock>();
            blamScenario.CutsceneFlags = new List<Scenario.CutsceneFlag>();
            blamScenario.CutsceneTitles = new List<Scenario.CutsceneTitle>();
            blamScenario.DeviceGroups = new List<Scenario.DeviceGroup>();
            blamScenario.EffectSceneryPalette = new List<Scenario.EffectSceneryBlock2>();
            blamScenario.EffectScenery = new List<Scenario.EffectSceneryBlock>();
            blamScenario.Equipment = new List<Scenario.EquipmentBlock>();
            blamScenario.EquipmentPalette = new List<Scenario.EquipmentPaletteBlock>();
            blamScenario.Flocks = new List<Scenario.Flock>();
            blamScenario.Fog = new List<Scenario.FogBlock>();
            blamScenario.Giants = new List<Scenario.Giant>();
            blamScenario.GiantPalette = new List<Scenario.GiantPaletteBlock>();
            blamScenario.Interpolators = new List<Scenario.Interpolator>();
            blamScenario.LightVolumes = new List<Scenario.LightVolume>();
            blamScenario.LightVolumePalette = new List<Scenario.LightVolumesPaletteBlock>();
            blamScenario.Machines = new List<Scenario.Machine>();
            blamScenario.MachinePalette = new List<Scenario.MachinePaletteBlock>();
            blamScenario.ScenarioSafeTriggers = new List<Scenario.ScenarioSafeTrigger>();
            blamScenario.SoundScenery = new List<Scenario.SoundSceneryBlock>();
            blamScenario.SoundSceneryPalette = new List<Scenario.SoundSceneryPaletteBlock>();
            blamScenario.Squads = new List<Scenario.Squad>();
            blamScenario.SquadGroups = new List<Scenario.SquadGroup>();
            blamScenario.StylePalette = new List<CachedTagInstance>();
            blamScenario.Terminals = new List<Scenario.Terminal>();
            blamScenario.TerminalPalette = new List<Scenario.TerminalPaletteBlock>();
            blamScenario.Vehicles = new List<Scenario.Vehicle>();
            blamScenario.VehiclePalette = new List<Scenario.VehiclePaletteBlock>();
            blamScenario.Weapons = new List<Scenario.Weapon>();
            blamScenario.WeaponPalette = new List<Scenario.WeaponPaletteBlock>();

            blamScenario.AiObjectives = new List<Scenario.AiObjective>();
            blamScenario.AiPathfindingData = new List<Scenario.AiPathfindingDatum>();
            blamScenario.ObjectNames = new List<Scenario.ObjectName>();
            blamScenario.ScenarioMetagame = new List<Scenario.ScenarioMetagameBlock>();
            blamScenario.UnitSeatsMapping = new List<Scenario.UnitSeatsMappingBlock>();
            blamScenario.Zones = new List<Scenario.Zone>();

            blamScenario.EditorFolders = new List<Scenario.EditorFolder>();
            // blamScenario.ScenarioResources = new List<Scenario.ScenarioResource>();

            // blamScenario.PlayerStartingLocations = new List<Scenario.PlayerStartingLocation>();
            // blamScenario.PlayerStartingProfile = new List<Scenario.PlayerStartingProfileBlock>();
            blamScenario.SimulationDefinitionTable = new List<Scenario.SimulationDefinitionTableBlock>();
            blamScenario.SpawnData = new List<Scenario.SpawnDatum>();

            blamScenario.Unknown84 = new List<Scenario.UnknownBlock2>();
            blamScenario.Unknown109 = new List<Scenario.UnknownBlock3>();
            blamScenario.Unknown134 = new List<Scenario.UnknownBlock4>();
            blamScenario.Unknown135 = new List<Scenario.UnknownBlock5>();
            blamScenario.Unknown143 = new List<Scenario.UnknownBlock7>();
            blamScenario.Unknown147 = new List<Scenario.UnknownBlock8>();
            blamScenario.Unknown32 = new List<Scenario.UnknownBlock>();
            blamScenario.Unknown44 = new List<Scenario.UnknownBlock>();
            // scnr.Unknown142 = new List<Scenario.UnknownBlock6>(); // REQUIRED

            blamScenario.ScenarioKillTriggers = new List<Scenario.ScenarioKillTrigger>();
            blamScenario.TriggerVolumes = new List<Scenario.TriggerVolume>();
            blamScenario.SoftCeilings = new List<Scenario.SoftCeiling>();
            
            // scnr.ZonesetSwitchTriggerVolumes = new List<Scenario.ZoneSetSwitchTriggerVolume>();
            // scnr.DesignerZoneSets = new List<Scenario.DesignerZoneSet>();
            // scnr.ScenarioClusterData = new List<Scenario.ScenarioClusterDatum>();
            // scnr.StructureBsps = new List<Scenario.StructureBspBlock>(); // REQUIRED
            // scnr.ZoneSets = new List<Scenario.ZoneSet>(); // REQUIRED
            // scnr.ZoneSetAudibility = new List<Scenario.ZoneSetAudibilityBlock>(); // REQUIRED
            // scnr.ZoneSetPotentiallyVisibleSets = new List<Scenario.ZoneSetPotentiallyVisibleSet>(); // REQUIRED

            blamScenario.SandboxEquipment = new List<Scenario.SandboxObject>();
            blamScenario.SandboxGoalObjects = new List<Scenario.SandboxObject>();
            blamScenario.SandboxScenery = new List<Scenario.SandboxObject>();
            blamScenario.SandboxSpawning = new List<Scenario.SandboxObject>();
            blamScenario.SandboxTeleporters = new List<Scenario.SandboxObject>();
            blamScenario.SandboxVehicles = new List<Scenario.SandboxObject>();
            blamScenario.SandboxWeapons = new List<Scenario.SandboxObject>();

            blamScenario.Globals = new List<BlamCore.Scripting.ScriptGlobal>();
            blamScenario.Scripts = new List<BlamCore.Scripting.Script>();
            blamScenario.ScriptReferences = new List<CachedTagInstance>();
            blamScenario.ScriptExpressions = new List<BlamCore.Scripting.ScriptExpression>();
            blamScenario.ScriptingData = new List<Scenario.ScriptingDatum>();
            blamScenario.EditorScenarioData = new byte[0];
            blamScenario.ObjectSalts = new int[32];

            blamScenario.Unknown0 = 0;
            blamScenario.Unknown100 = 0;
            blamScenario.Unknown101 = 0;
            blamScenario.Unknown102 = 0;
            blamScenario.Unknown103 = 0;
            blamScenario.Unknown104 = 0;
            blamScenario.Unknown105 = 0;
            blamScenario.Unknown106 = 0;
            blamScenario.Unknown107 = 0;
            blamScenario.Unknown108 = 0;
            blamScenario.Unknown10 = 0;
            blamScenario.Unknown110 = 0;
            blamScenario.Unknown111 = 0;
            blamScenario.Unknown112 = 0;
            blamScenario.Unknown113 = 0;
            blamScenario.Unknown114 = 0;
            blamScenario.Unknown115 = 0;
            blamScenario.Unknown116 = 0;
            blamScenario.Unknown117 = 0;
            blamScenario.Unknown118 = 0;
            blamScenario.Unknown119 = 0;
            blamScenario.Unknown11 = 0;
            blamScenario.Unknown120 = 0;
            blamScenario.Unknown121 = 0;
            blamScenario.Unknown122 = 0;
            blamScenario.Unknown123 = 0;
            blamScenario.Unknown124 = 0;
            blamScenario.Unknown125 = 0;
            blamScenario.Unknown126 = 0;
            blamScenario.Unknown127 = 0;
            blamScenario.Unknown128 = 0;
            blamScenario.Unknown129 = 0;
            blamScenario.Unknown12 = 0;
            blamScenario.Unknown130 = 0;
            blamScenario.Unknown131 = 0;
            blamScenario.Unknown132 = 0;
            blamScenario.Unknown136 = 0;
            blamScenario.Unknown137 = 0;
            blamScenario.Unknown138 = 0;
            blamScenario.Unknown139 = 0;
            blamScenario.Unknown13 = 0;
            blamScenario.Unknown140 = 0;
            blamScenario.Unknown141 = 0;
            blamScenario.Unknown144 = 0;
            blamScenario.Unknown145 = 0;
            blamScenario.Unknown146 = 0;
            blamScenario.Unknown14 = 0;
            blamScenario.Unknown150 = 0;
            blamScenario.Unknown151 = 0;
            blamScenario.Unknown152 = 0;
            blamScenario.Unknown15 = 0;
            blamScenario.Unknown16 = 0;
            blamScenario.Unknown17 = 0;
            blamScenario.Unknown1 = 0;
            blamScenario.Unknown23 = 0;
            blamScenario.Unknown24 = 0;
            blamScenario.Unknown25 = 0;
            blamScenario.Unknown26 = 0;
            blamScenario.Unknown27 = 0;
            blamScenario.Unknown28 = 0;
            blamScenario.Unknown2 = 0;
            blamScenario.Unknown35 = 0;
            blamScenario.Unknown36 = 0;
            blamScenario.Unknown37 = 0;
            blamScenario.Unknown38 = 0;
            blamScenario.Unknown39 = 0;
            blamScenario.Unknown40 = 0;
            blamScenario.Unknown41 = 0;
            blamScenario.Unknown42 = 0;
            blamScenario.Unknown43 = 0;
            blamScenario.Unknown45 = 0;
            blamScenario.Unknown46 = 0;
            blamScenario.Unknown47 = 0;
            blamScenario.Unknown48 = 0;
            blamScenario.Unknown49 = 0;
            blamScenario.Unknown50 = 0;
            blamScenario.Unknown51 = 0;
            blamScenario.Unknown52 = 0;
            blamScenario.Unknown53 = 0;
            blamScenario.Unknown54 = 0;
            blamScenario.Unknown55 = 0;
            blamScenario.Unknown56 = 0;
            blamScenario.Unknown57 = 0;
            blamScenario.Unknown58 = 0;
            blamScenario.Unknown59 = 0;
            blamScenario.Unknown60 = 0;
            blamScenario.Unknown61 = 0;
            blamScenario.Unknown62 = 0;
            blamScenario.Unknown63 = 0;
            blamScenario.Unknown64 = 0;
            blamScenario.Unknown65 = 0;
            blamScenario.Unknown66 = 0;
            blamScenario.Unknown67 = 0;
            blamScenario.Unknown68 = 0;
            blamScenario.Unknown69 = 0;
            blamScenario.Unknown70 = 0;
            blamScenario.Unknown71 = 0;
            blamScenario.Unknown72 = 0;
            blamScenario.Unknown73 = 0;
            blamScenario.Unknown74 = 0;
            blamScenario.Unknown75 = 0;
            blamScenario.Unknown76 = 0;
            blamScenario.Unknown77 = 0;
            blamScenario.Unknown78 = 0;
            blamScenario.Unknown79 = 0;
            blamScenario.Unknown80 = 0;
            blamScenario.Unknown81 = 0;
            blamScenario.Unknown82 = 0;
            blamScenario.Unknown83 = 0;
            blamScenario.Unknown85 = 0;
            blamScenario.Unknown86 = 0;
            blamScenario.Unknown87 = 0;
            blamScenario.Unknown88 = 0;
            blamScenario.Unknown89 = 0;
            blamScenario.Unknown90 = 0;
            blamScenario.Unknown91 = 0;
            blamScenario.Unknown92 = 0;
            blamScenario.Unknown93 = 0;
            blamScenario.Unknown94 = 0;
            blamScenario.Unknown95 = 0;
            blamScenario.Unknown96 = 0;
            blamScenario.Unknown97 = 0;
            blamScenario.Unknown98 = 0;
            blamScenario.Unknown99 = 0;
            blamScenario.Unknown9 = 0;
            #endregion
            

            CachedTagInstance newTag;

            if (isNew)
            {
                Console.Write("Allocating the new ElDorado scenario tag...");

                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("scnr")]);

                Console.WriteLine("done.");
            }
            else
            {
                newTag = edTag;
            }

            CacheContext.TagNames[newTag.Index] = blamTagName;

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                Console.WriteLine($"Writing {CacheContext.TagNames[newTag.Index]}.scenario tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, blamScenario);
            }

            //
            // Save new string_ids
            //

            if (CacheContext.StringIdCache.Strings.Count != initialStringIDCount)
            {
                Console.Write("Saving string_ids...");

                using (var stringIdStream = CacheContext.OpenStringIdCacheReadWrite())
                    CacheContext.StringIdCache.Save(stringIdStream);

                Console.WriteLine("done.");
            }

            //
            // Done!
            //

            Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.scenario\" successfully!");

            return true;
        }

        private CachedTagInstance ConvertTagReference(int index)
        {
            var instance = BlamCache.IndexItems.Find(i => i.ID == index);

            if (instance != null)
            {
                var chars = new char[] { ' ', ' ', ' ', ' ' };
                for (var i = 0; i < instance.ClassCode.Length; i++)
                    chars[i] = instance.ClassCode[i];

                var tags = CacheContext.TagCache.Index.FindAllInGroup(new string(chars));

                foreach (var tag in tags)
                {
                    if (instance.Filename == CacheContext.TagNames[tag.Index])
                        return tag;
                }
            }

            return null;
        }

        private void ConvertStringId(ref StringId stringId)
        {
            var value = BlamCache.Strings.GetItemByID((int)stringId.Value);

            if (!CacheContext.StringIdCache.Contains(value))
                stringId = CacheContext.StringIdCache.AddString(value);
            else
                stringId = CacheContext.GetStringId(value);
        }

    }
}
