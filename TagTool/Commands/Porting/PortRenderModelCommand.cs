﻿using System;
using System.Collections.Generic;
using System.IO;
using BlamCore.Cache;
using BlamCore.Legacy.Base;
using BlamCore.Common;
using BlamCore.Geometry;
using BlamCore.TagDefinitions;
using BlamCore.TagResources;
using BlamCore.IO;
using BlamCore.Serialization;

namespace TagTool.Commands.Porting
{
    class PortRenderModelCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private CacheFile BlamCache { get; }

        public PortRenderModelCommand(GameCacheContext cacheContext, CacheFile blamCache)
            : base(CommandFlags.Inherit,

                  "PortRenderModel",
                  "Ports a Blam render_model tag to the current cache.",

                  "PortRenderModel [New <Blam Tag>] | [<Blam Tag> <ElDorado Tag>]",

                  "Ports a Blam render_model tag to the current cache.")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        private void ConvertStringId(ref StringId stringId)
        {
            var value = BlamCache.Strings.GetItemByID((int)stringId.Value);

            if (!CacheContext.StringIdCache.Contains(value))
                stringId = CacheContext.StringIdCache.AddString(value);
            else
                stringId = CacheContext.GetStringId(value);
        }

        private void ConvertVertices<T>(int count, Func<T> readVertex, Action<T> writeVertex)
        {
            for (var i = 0; i < count; i++)
                writeVertex(readVertex());
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count < 2 || args.Count > 3)
                return false;

            bool isNew = false;
            if (args[0].ToLower() == "new")
            {
                isNew = true;
                args.RemoveAt(0);
            }

            var initialStringIDCount = CacheContext.StringIdCache.Strings.Count;

            //
            // Verify the Blam render_model tag
            //

            var blamTagName = args[0];

            CacheFile.IndexItem blamTag = null;

            Console.WriteLine("Verifying Blam tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "mode" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam tag does not exist: " + args[0]);
                return false;
            }

            //
            // Verify the ED render_model tag
            //

            CachedTagInstance edTag = null;

            if (!isNew)
            {
                Console.WriteLine("Verifying ED tag index...");

                edTag = ArgumentParser.ParseTagSpecifier(CacheContext, args[1]);

                if (edTag.Group.Name != CacheContext.GetStringId("render_model"))
                {
                    Console.WriteLine("Specified tag index is not a render_model: " + args[1]);
                    return false;
                }
            }

            //
            // Deserialize the Blam render_model tag
            //

            var blamDeserializer = new TagDeserializer(BlamCache.Version);

            RenderModel blamMode;

            try
            {
                var context = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
                blamMode = blamDeserializer.Deserialize<RenderModel>(context);
            }
            catch
            {
                Console.WriteLine("Failed to deserialize Blam render_model tag: " + blamTagName);
                return true;
            }

            //
            // Load Blam resource fixups
            //

            var definitionEntry = BlamCache.ResourceGestalt.DefinitionEntries[blamMode.Geometry.ZoneAssetIndex & ushort.MaxValue];

            var resourceDefinition = new RenderGeometryApiResourceDefinition
            {
                VertexBuffers = new List<D3DPointer<VertexBufferDefinition>>(),
                IndexBuffers = new List<D3DPointer<IndexBufferDefinition>>()
            };

            using (var definitionStream = new MemoryStream(BlamCache.ResourceGestalt.FixupData))
            using (var definitionReader = new EndianReader(definitionStream, EndianFormat.BigEndian))
            {
                var dataContext = new DataSerializationContext(definitionReader, null);

                definitionReader.SeekTo(definitionEntry.Offset + (definitionEntry.Size - 24));

                var vertexBufferCount = definitionReader.ReadInt32();
                definitionReader.Skip(8);
                var indexBufferCount = definitionReader.ReadInt32();

                definitionReader.SeekTo(definitionEntry.Offset);

                for (var i = 0; i < vertexBufferCount; i++)
                {
                    resourceDefinition.VertexBuffers.Add(new D3DPointer<VertexBufferDefinition>
                    {
                        Definition = new VertexBufferDefinition
                        {
                            Count = definitionReader.ReadInt32(),
                            Format = (VertexBufferFormat)definitionReader.ReadInt16(),
                            VertexSize = definitionReader.ReadInt16(),
                            Data = new ResourceDataReference
                            {
                                Size = definitionReader.ReadInt32(),
                                Unused4 = definitionReader.ReadInt32(),
                                Unused8 = definitionReader.ReadInt32(),
                                Address = new ResourceAddress(ResourceAddressType.Memory, definitionReader.ReadInt32()),
                                Unused10 = definitionReader.ReadInt32()
                            }
                        }
                    });
                }

                definitionReader.Skip(vertexBufferCount * 12);

                for (var i = 0; i < indexBufferCount; i++)
                {
                    resourceDefinition.IndexBuffers.Add(new D3DPointer<IndexBufferDefinition>
                    {
                        Definition = new IndexBufferDefinition
                        {
                            Type = (PrimitiveType)definitionReader.ReadInt32(),
                            Data = new ResourceDataReference
                            {
                                Size = definitionReader.ReadInt32(),
                                Unused4 = definitionReader.ReadInt32(),
                                Unused8 = definitionReader.ReadInt32(),
                                Address = new ResourceAddress(ResourceAddressType.Memory, definitionReader.ReadInt32()),
                                Unused10 = definitionReader.ReadInt32()
                            }
                        }
                    });
                }
            }

            //
            // Convert Blam data to ElDorado data
            //

            ConvertStringId(ref blamMode.Name);

            foreach (var region in blamMode.Regions)
            {
                ConvertStringId(ref region.Name);

                foreach (var permutation in region.Permutations)
                {
                    ConvertStringId(ref permutation.Name);
                }
            }

            foreach (var instance in blamMode.Instances)
            {
                ConvertStringId(ref instance.Name);
            }

            foreach (var node in blamMode.Nodes)
            {
                ConvertStringId(ref node.Name);
            }

            foreach (var markerGroup in blamMode.MarkerGroups)
            {
                ConvertStringId(ref markerGroup.Name);
            }

            foreach (var material in blamMode.Materials)
            {
                material.RenderMethod = CacheContext.GetTag(0x101F);
            }

            using (var edResourceStream = new MemoryStream())
            {
                using (var blamResourceStream = new MemoryStream(BlamCache.GetRawFromID(blamMode.Geometry.ZoneAssetIndex)))
                {
                    //
                    // Convert Blam render_geometry_api_resource_definition
                    //

                    var inVertexStream = VertexStreamFactory.Create(BlamCache.Version, blamResourceStream);
                    var outVertexStream = VertexStreamFactory.Create(CacheContext.Version, edResourceStream);

                    for (var i = 0; i < resourceDefinition.VertexBuffers.Count; i++)
                    {
                        var vertexBuffer = resourceDefinition.VertexBuffers[i].Definition;

                        var count = vertexBuffer.Count;

                        var startPos = (int)edResourceStream.Position;
                        vertexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                        blamResourceStream.Position = definitionEntry.Fixups[i].Offset;

                        switch (vertexBuffer.Format)
                        {
                            case VertexBufferFormat.World:
                                ConvertVertices(count, inVertexStream.ReadWorldVertex, outVertexStream.WriteWorldVertex);
                                break;

                            case VertexBufferFormat.Rigid:
                                ConvertVertices(count, inVertexStream.ReadRigidVertex, outVertexStream.WriteRigidVertex);
                                break;

                            case VertexBufferFormat.Skinned:
                                ConvertVertices(count, inVertexStream.ReadSkinnedVertex, outVertexStream.WriteSkinnedVertex);
                                break;

                            case VertexBufferFormat.StaticPerPixel:
                                ConvertVertices(count, inVertexStream.ReadStaticPerPixelData, outVertexStream.WriteStaticPerPixelData);
                                break;

                            case VertexBufferFormat.StaticPerVertex:
                                ConvertVertices(count, inVertexStream.ReadStaticPerVertexData, outVertexStream.WriteStaticPerVertexData);
                                break;

                            case VertexBufferFormat.AmbientPrt:
                                ConvertVertices(count, inVertexStream.ReadAmbientPrtData, outVertexStream.WriteAmbientPrtData);
                                break;

                            case VertexBufferFormat.LinearPrt:
                                ConvertVertices(count, inVertexStream.ReadLinearPrtData, outVertexStream.WriteLinearPrtData);
                                break;

                            case VertexBufferFormat.QuadraticPrt:
                                ConvertVertices(count, inVertexStream.ReadQuadraticPrtData, outVertexStream.WriteQuadraticPrtData);
                                break;

                            case VertexBufferFormat.StaticPerVertexColor:
                                ConvertVertices(count, inVertexStream.ReadStaticPerVertexColorData, outVertexStream.WriteStaticPerVertexColorData);
                                break;

                            case VertexBufferFormat.Decorator:
                                ConvertVertices(count, inVertexStream.ReadDecoratorVertex, outVertexStream.WriteDecoratorVertex);
                                break;

                            case VertexBufferFormat.World2:
                                vertexBuffer.Format = VertexBufferFormat.World;
                                goto case VertexBufferFormat.World;

                            default:
                                throw new NotSupportedException(vertexBuffer.Format.ToString());
                        }

                        vertexBuffer.Data.Size = (int)edResourceStream.Position - startPos;
                        vertexBuffer.VertexSize = (short)(vertexBuffer.Data.Size / vertexBuffer.Count);
                    }

                    for (var i = 0; i < resourceDefinition.IndexBuffers.Count; i++)
                    {
                        var indexBuffer = resourceDefinition.IndexBuffers[i].Definition;

                        var indexCount = indexBuffer.Data.Size / 2;

                        var inIndexStream = new IndexBufferStream(
                            blamResourceStream,
                            IndexBufferFormat.UInt16,
                            EndianFormat.BigEndian);

                        var outIndexStream = new IndexBufferStream(
                            edResourceStream,
                            IndexBufferFormat.UInt16,
                            EndianFormat.LittleEndian);

                        var startPos = (int)edResourceStream.Position;
                        indexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                        blamResourceStream.Position = definitionEntry.Fixups[resourceDefinition.VertexBuffers.Count * 2 + i].Offset;

                        for (var j = 0; j < indexCount; j++)
                            outIndexStream.WriteIndex(inIndexStream.ReadIndex());
                    }
                }

                //
                // Finalize the new ElDorado render_model tag
                //

                CachedTagInstance newTag;

                if (isNew)
                {
                    Console.Write("Allocating the new ElDorado render_model tag...");

                    using (var stream = CacheContext.OpenTagCacheReadWrite())
                        newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("mode")]);

                    Console.WriteLine("done.");
                }
                else
                {
                    newTag = edTag;
                }

                CacheContext.TagNames[newTag.Index] = blamTagName;

                blamMode.Geometry.Resource = new ResourceReference
                {
                    Type = 5, // FIXME: hax
                    DefinitionFixups = new List<ResourceDefinitionFixup>(),
                    D3DObjectFixups = new List<D3DObjectFixup>(),
                    Unknown68 = 1,
                    Owner = newTag
                };

                Console.WriteLine("Writing resource data...");

                edResourceStream.Position = 0;

                var resourceContext = new ResourceSerializationContext(blamMode.Geometry.Resource);
                CacheContext.Serializer.Serialize(resourceContext, resourceDefinition);
                CacheContext.AddResource(blamMode.Geometry.Resource, ResourceLocation.Resources, edResourceStream);

                using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
                {
                    Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.render_model\" tag data...");

                    var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                    CacheContext.Serializer.Serialize(context, blamMode);
                }

                //
                // Save new string_ids
                //

                if (CacheContext.StringIdCache.Strings.Count != initialStringIDCount)
                {
                    Console.Write("Saving string_ids...");

                    using (var stringIdStream = CacheContext.OpenStringIdCacheReadWrite())
                        CacheContext.StringIdCache.Save(stringIdStream);

                    Console.WriteLine("done.");
                }

                //
                // Done!
                //

                Console.WriteLine($"Ported \"{CacheContext.TagNames[newTag.Index]}.render_model\" successfully!");

            }

            return true;
        }
    }
}