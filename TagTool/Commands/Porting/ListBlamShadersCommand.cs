﻿using System;
using System.Collections.Generic;
using System.IO;
using BlamCore.Cache;
using BlamCore.Legacy.Base;
using BlamCore.Common;
using BlamCore.Geometry;
using BlamCore.TagDefinitions;
using BlamCore.TagResources;
using BlamCore.IO;
using BlamCore.Serialization;

namespace TagTool.Commands.Porting
{
    class ListBlamShadersCommand : Command
    {
        public GameCacheContext CacheContext { get; }
        public CacheFile BlamCache { get; }

        public ListBlamShadersCommand(GameCacheContext cacheContext, CacheFile blamCache)
            : base(CommandFlags.None,

                  "ListBlamShaders",
                  "List all shaders of a specified Blam mode or sbsp tag.",

                  "ListBlamShaders <Blam Tag>",

                  "List all shaders of a specified Blam mode or sbsp tag.")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count != 1)
                return false;

            var renderModelName = args[0];

            CacheFile.IndexItem blamTag = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if ((tag.ClassCode == "mode" || 
                    tag.ClassCode == "sbsp" ) && 
                    tag.Filename.Contains(args[0]))
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                Console.WriteLine("Blam tag does not exist: " + args[0]);
                return false;
            }

            //
            // Deserialize the Blam render_model tag
            //

            var blamDeserializer = new TagDeserializer(BlamCache.Version);

            if (blamTag.ClassCode == "mode")
            {
                RenderModel renderModel;

                try
                {
                    var context = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
                    renderModel = blamDeserializer.Deserialize<RenderModel>(context);
                }
                catch
                {
                    Console.WriteLine("Failed to deserialize Blam render_model tag: " + renderModelName);
                    return true;
                }

                for (int i = 0; i < renderModel.Materials.Count; i++)
                {
                    var bitmItem = BlamCache.IndexItems.Find(j =>
                        j.ID == renderModel.Materials[i].RenderMethod.Index);
                    Console.WriteLine("{0:D2} {1}", i, bitmItem);
                }
            }
            else if (blamTag.ClassCode == "sbsp")
            {
                ScenarioStructureBsp sbsp;

                try
                {
                    var context = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
                    sbsp = blamDeserializer.Deserialize<ScenarioStructureBsp>(context);
                }
                catch
                {
                    Console.WriteLine("Failed to deserialize Blam render_model tag: " + renderModelName);
                    return true;
                }

                for (int i = 0; i < sbsp.Materials.Count; i++)
                {
                    var bitmItem = BlamCache.IndexItems.Find(j =>
                        j.ID == sbsp.Materials[i].RenderMethod.Index);
                    Console.WriteLine("{0:D2} {1}", i, bitmItem);
                }
            }


            return true;
        }
    }
}
/*
// rmsh: list every shader's Unknown tagblock values (shader signature).
Console.WriteLine("");
foreach (var tag in BlamCache.IndexItems)
{
    if (tag.ClassCode == "rmsh")
    {
        var blamDeserializer = new TagDeserializer(BlamCache.Version);
        var blamContext = new CacheSerializationContext(CacheContext, BlamCache, tag);
        var blamShader = blamDeserializer.Deserialize<Shader>(blamContext);

        Console.Write("{0:X4},", tag.Filename);
        for (int i = 0; i < blamShader.Unknown.Count; i++)
        {
            string unknown = "";
            unknown = unknown + "_" + blamShader.Unknown[i].Unknown.ToString();
            Console.Write(unknown);
        }
        Console.WriteLine("");
    }
}

*/
