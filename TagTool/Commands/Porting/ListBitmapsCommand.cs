using System;
using System.Collections.Generic;
using BlamCore.Cache;
using BlamCore.Legacy.Base;

namespace TagTool.Commands.Porting
{
    class ListBitmapsCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private CacheFile BlamCache { get; }

        public ListBitmapsCommand(GameCacheContext cacheContext, CacheFile blamCache)
            : base(CommandFlags.None,

                  "ListBitmaps",
                  "",

                  "ListBitmaps <Blam Tag>",
                  "")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count != 1)
                return false;

            CacheFile.IndexItem item = null;

            Console.WriteLine("Verifying blam shader tag...");

            var shaderName = args[0];

            foreach (var tag in BlamCache.IndexItems)
            {
                if ((tag.ParentClass == "rm") && tag.Filename == shaderName)
                {
                    item = tag;
                    break;
                }
            }

            if (item == null)
            {
                Console.WriteLine("Blam shader tag does not exist: " + shaderName);
                return false;
            }

            /* TODO:
            var renderMethod = DefinitionsManager.rmsh(BlamCache, item);

            var templateItem = BlamCache.IndexItems.Find(i =>
                i.ID == renderMethod.Properties[0].TemplateTagID);

            var template = DefinitionsManager.rmt2(BlamCache, templateItem);

            for (var i = 0; i < template.UsageBlocks.Count; i++)
            {
                var bitmItem = BlamCache.IndexItems.Find(j =>
                j.ID == renderMethod.Properties[0].ShaderMaps[i].BitmapTagID);
                Console.WriteLine(bitmItem);
            }
            */
            
            var BlamShader = new BlamCore.Legacy.Halo3Beta.shader(BlamCache, item.Offset);

            var templateItem = BlamCache.IndexItems.Find(i =>
                i.ID == BlamShader.Properties[0].TemplateTagID);

            var template = new BlamCore.Legacy.Halo3Beta.render_method_template(BlamCache, templateItem.Offset);

            for (var i = 0; i < template.UsageBlocks.Count; i++)
            {
                var entry = template.UsageBlocks[i].Usage;

                var bitmItem = BlamCache.IndexItems.Find(j =>
                j.ID == BlamShader.Properties[0].ShaderMaps[i].BitmapTagID);
                Console.WriteLine(string.Format("{0:D2} {2}\t {1}", i, bitmItem.Filename, entry));
            }

            return true;
        }
    }
}
