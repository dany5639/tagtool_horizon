﻿using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.IO;
using BlamCore.Legacy.Base;
using BlamCore.Serialization;
using BlamCore.TagDefinitions;
using BlamCore.TagResources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TagTool.Commands.Porting
{
    class TestBlamCommand : Command
    {
        private GameCacheContext CacheContext { get; }
        private CacheFile BlamCache { get; }

        public TestBlamCommand(GameCacheContext cacheContext, CacheFile blamCache) :
            base(CommandFlags.None,

                "TestBlam",
                "",

                "TestBlam",

                "")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        public override bool Execute(List<string> args)
        {
            #region Open cache file.
            if (args.Count != 1)
                return false;

            var blamCacheFile = new FileInfo(@"D:\Halo\Map packs\ODSTmaps\SC140.map");

            if (!blamCacheFile.Exists)
                throw new FileNotFoundException(blamCacheFile.FullName);

            CacheFile BlamCache = null;

            using (var fs = new FileStream(blamCacheFile.FullName, FileMode.Open, FileAccess.Read))
            {
                var reader = new EndianReader(fs, EndianFormat.BigEndian);

                var head = reader.ReadInt32();

                if (head == 1684104552)
                    reader.Format = EndianFormat.LittleEndian;

                var v = reader.ReadInt32();

                reader.SeekTo(284);
                var version = CacheVersionDetection.GetFromBuildName(reader.ReadString(32));

                switch (version)
                {
                    case CacheVersion.Halo3Beta:
                        BlamCache = new BlamCore.Legacy.Halo3Beta.CacheFile(blamCacheFile, version);
                        break;

                    case CacheVersion.Halo3Retail:
                        BlamCache = new BlamCore.Legacy.Halo3Retail.CacheFile(blamCacheFile, version);
                        break;

                    case CacheVersion.Halo3ODST:
                        BlamCache = new BlamCore.Legacy.Halo3ODST.CacheFile(blamCacheFile, version);
                        break;

                    default:
                        throw new NotSupportedException(CacheVersionDetection.GetBuildName(version));
                }
            }

            BlamCache.LoadResourceTags();
            #endregion

            #region Read blam tag.
            CacheFile.IndexItem blamTag = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "sbsp" && tag.Filename == @"levels\atlas\sc140\sc140_010")
                {
                    blamTag = tag;
                    break;
                }
            }

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamSbsp = blamDeserializer.Deserialize<ScenarioStructureBsp>(blamContext);
            #endregion

            #region Load ED tag
            CachedTagInstance Tag = CacheContext.GetTag(0x4442);

            ScenarioStructureBsp edSbsp;

            Console.Write("Deserializing; ");
            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                var edContext = new TagSerializationContext(cacheStream, CacheContext, Tag);
                edSbsp = CacheContext.Deserializer.Deserialize<ScenarioStructureBsp>(edContext);
            }
            #endregion

            

            // using (var stream = CacheContext.TagCacheFile.Open(FileMode.Open, FileAccess.ReadWrite))
            // {
            //     var context = new TagSerializationContext(stream, CacheContext, Tag);
            //     CacheContext.Serializer.Serialize(context, blamScenario);
            // }

            Console.WriteLine("Done");
            return true;
        }

        public void GetVertexFormats(List<string> args)
        {
            CacheFile.IndexItem blamTag = null;

            Console.WriteLine("Verifying Blam Scenario tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "sbsp" && tag.Filename == args[0])
                {
                    blamTag = tag;
                    break;
                }
            }

            var RawID2 = 0;

            CacheFile.IndexItem blamTagLbsp = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "Lbsp" && tag.Filename == args[0]) // Lbsp name always matches the sbsp name. there's extra Lbsps with a longer name
                {
                    blamTagLbsp = tag;
                    break;
                }
            }

            var blamDeserializerLbsp = new TagDeserializer(BlamCache.Version);
            var blamContextLbsp = new CacheSerializationContext(CacheContext, BlamCache, blamTagLbsp);
            var blamLbsp = blamDeserializerLbsp.Deserialize<ScenarioLightmapBspData>(blamContextLbsp);

            RawID2 = BitConverter.ToInt16(BitConverter.GetBytes(blamLbsp.Geometry.ZoneAssetIndex), 0);

            var blamDeserializer = new TagDeserializer(BlamCache.Version);
            var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
            var blamSbsp = blamDeserializer.Deserialize<ScenarioStructureBsp>(blamContext);
            var serializer = new TagSerializer(CacheVersion.HaloOnline106708);

            var geometryFixups2 = new List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry>(); // has list of all groups and list of members, for members offsets

            var Entry2 = BlamCache.ResourceGestalt.DefinitionEntries[RawID2 & ushort.MaxValue];
            geometryFixups2.Add(Entry2);

            blamSbsp.Geometry2.Resource = new ResourceReference
            {
                Type = 5, // FIXME: hax
                DefinitionFixups = new List<ResourceDefinitionFixup>(),
                D3DObjectFixups = new List<D3DObjectFixup>(),
                Unknown68 = 1
            };

            foreach (var fixup in Entry2.Fixups)
            {
                var fixup2 = new ResourceDefinitionFixup
                {
                    DefinitionDataOffset = (uint)fixup.BlockOffset
                };
                if (fixup.FixupType == 4)
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Resource, fixup.Offset);
                }
                else
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Definition, fixup.Offset);
                }
                blamSbsp.Geometry2.Resource.DefinitionFixups.Add(fixup2);
            }

            blamSbsp.Geometry2.Resource.DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, Entry2.DefinitionAddress);
            blamSbsp.Geometry2.Resource.DefinitionData = BlamCache.ResourceGestalt.FixupData.Skip(Entry2.Offset).Take(Entry2.Size).ToArray();

            var resourceDataStream2 = new MemoryStream(BlamCache.GetRawFromID(RawID2));

            var geostream2 = new MemoryStream(blamSbsp.Geometry2.Resource.DefinitionData);
            var georeader2 = new EndianReader(geostream2, EndianFormat.BigEndian);

            georeader2.BaseStream.Position = blamSbsp.Geometry2.Resource.DefinitionAddress.Offset;
            georeader2.Format = EndianFormat.BigEndian;

            var geodeserializer2 = new TagDeserializer(BlamCore.Cache.CacheVersion.Halo3ODST);
            var geodataContext2 = new DataSerializationContext(georeader2, null);

            var geowriter2 = new EndianWriter(geostream2, EndianFormat.BigEndian);
            foreach (var fixup in blamSbsp.Geometry2.Resource.DefinitionFixups)
            {
                geostream2.Position = fixup.DefinitionDataOffset;
                geowriter2.Write(fixup.Address.Value);
            }
            geostream2.Position = blamSbsp.Geometry2.Resource.DefinitionAddress.Offset;

            var geodef2 = geodeserializer2.Deserialize<RenderGeometryApiResourceDefinition>(geodataContext2);

            Dictionary<string, int> list = new Dictionary<string, int>();

            

            using (var blamResourceStream = new MemoryStream(BlamCache.GetRawFromID(RawID2)))
            using (var edResourceStream = new MemoryStream())
            {
                for (var i = 0; i < geodef2.VertexBuffers.Count; i++)
                {
                    var vertexBuffer = geodef2.VertexBuffers[i].Definition;

                    var count = vertexBuffer.Count;
                    
                    blamResourceStream.Position = Entry2.Fixups[i].Offset;
                    
                    try
                    {
                        list.Add(vertexBuffer.Format.ToString(), count);

                    }
                    catch (Exception)
                    {
                        list[vertexBuffer.Format.ToString()] = count;
                    }
                }
            }

            foreach(var a in list)
                Console.WriteLine(string.Format("{0, -20}: {1}", a.Key, a.Value));

        }
    }
}
