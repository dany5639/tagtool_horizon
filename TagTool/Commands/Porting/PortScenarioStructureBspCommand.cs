﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BlamCore.Common;
using BlamCore.Legacy.Base;
using BlamCore.Cache;
using BlamCore.IO;
using BlamCore.Geometry;
using BlamCore.Serialization;
using BlamCore.TagDefinitions;
using BlamCore.TagResources;
using BlamCore.Bitmaps;

namespace TagTool.Commands.Porting
{
    class PortScenarioStructureBspCommand : Command
    {
        public GameCacheContext CacheContext { get; }
        public CacheFile BlamCache { get; }

        public PortScenarioStructureBspCommand(GameCacheContext cacheContext, CacheFile blamCache)
            : base(CommandFlags.Inherit,

                  "PortScenarioStructureBsp",
                  "!!! EXPERIMENTAL, NOT MEANT FOR PUBLIC USE ATM !!!",

                  "PortScenarioStructureBsp [New] <Blam Tag> <ElDorado Tag>",

                  "!!! EXPERIMENTAL, NOT MEANT FOR PUBLIC USE ATM !!!")
        {
            CacheContext = cacheContext;
            BlamCache = blamCache;
        }

        private void ConvertVertices<T>(int count, Func<T> readVertex, Action<T> writeVertex)
        {
            for (var i = 0; i < count; i++)
                writeVertex(readVertex());
        }

        public override bool Execute(List<string> args)
        {
            if (args.Count < 2 || args.Count > 3)
                return false;

            bool isNew = false;
            if (args[0].ToLower() == "new")
            {
                isNew = true;
                args.RemoveAt(0);
            }

            var initialStringIDCount = CacheContext.StringIdCache.Strings.Count;

            //
            // Verify the Blam scenario_structure_bsp tag
            //

            var blamTagName = args[0];

            CacheFile.IndexItem blamTag = null;

            // Console.WriteLine("Verifying Blam scenario_structure_bsp tag...");

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "sbsp" && tag.Filename == blamTagName)
                {
                    blamTag = tag;
                    break;
                }
            }

            if (blamTag == null)
            {
                // Console.WriteLine("Blam tag does not exist: " + args[0]);
                return false;
            }

            //
            // Verify the ED scenario_structure_bsp tag
            //

            CachedTagInstance edTag = null;

            if (!isNew)
            {
                // Console.Write("Verifying ElDorado scenario_structure_bsp tag index...");

                edTag = ArgumentParser.ParseTagSpecifier(CacheContext, args[1]);

                if (edTag.Group.Name != CacheContext.GetStringId("scenario_structure_bsp"))
                {
                    // Console.WriteLine($"Specified tag index is not a scenario_structure_bsp: 0x{edTag.Index:X4}");
                    return false;
                }

                // Console.WriteLine("done.");
            }

            //
            // Deserialize the Blam render_model tag
            //

            var blamDeserializer = new TagDeserializer(BlamCache.Version);

            ScenarioStructureBsp blamSbsp;

            try
            {
                var blamContext = new CacheSerializationContext(CacheContext, BlamCache, blamTag);
                blamSbsp = blamDeserializer.Deserialize<ScenarioStructureBsp>(blamContext);
            }
            catch
            {
                Console.WriteLine("Failed to deserialize Blam render_model tag: " + blamTagName);
                return true;
            }

            var serializer = new TagSerializer(CacheVersion.HaloOnline106708);

            //
            // Create the new ElDorado scenario_structure_bsp tag
            //

            CachedTagInstance newTag;
            
            if (isNew)
            {
                // Console.Write("Allocating the new ElDorado scenario_structure_bsp tag...");
            
                using (var stream = CacheContext.OpenTagCacheReadWrite())
                    newTag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[new Tag("cfgt")]);
            
                // Console.WriteLine("done.");
            }
            else
            {
                newTag = edTag;
            }
            
            CacheContext.TagNames[newTag.Index] = blamTagName;

            //
            // Load the Blam scenario_structure_bsp tag
            //

            #region Nuke everything
            // blamSbsp.UnknownRaw1sts = new List<ScenarioStructureBsp.UnknownRaw1st>();
            // blamSbsp.UnknownRaw6ths = new List<ScenarioStructureBsp.UnknownRaw6th>();
            // blamSbsp.UnknownRaw7ths = new List<ScenarioStructureBsp.UnknownRaw7th>();
            // blamSbsp.Unknown44 = new List<ScenarioStructureBsp.UnknownBlock3>();
            // blamSbsp.ClusterPortals = new List<ScenarioStructureBsp.ClusterPortal>();
            // blamSbsp.Fog = new List<ScenarioStructureBsp.FogBlock>();
            
            // blamSbsp.DetailObjects = new List<ScenarioStructureBsp.DetailObject>();
            // blamSbsp.Clusters = new List<ScenarioStructureBsp.Cluster>();
            // blamSbsp.Materials = new List<RenderMaterial>();
            // blamSbsp.CollisionMaterials = new List<ScenarioStructureBsp.CollisionMaterial>();
            // blamSbsp.Leaves = new List<ScenarioStructureBsp.Leaf>();
            // blamSbsp.SkyOwnerCluster = new List<short>();

            // blamSbsp.BackgroundSoundEnvironmentPalette = new List<ScenarioStructureBsp.BackgroundSoundEnvironmentPaletteBlock>();
            // blamSbsp.Markers = new List<ScenarioStructureBsp.Marker>();
            // blamSbsp.RuntimeDecals = new List<ScenarioStructureBsp.RuntimeDecal>();
            // blamSbsp.InstancedGeometryInstances = new List<ScenarioStructureBsp.InstancedGeometryInstance>();
            // blamSbsp.Decorators = new List<CachedTagInstance>();

            // blamSbsp.UnknownSoundClustersA = new List<ScenarioStructureBsp.UnknownSoundClustersBlock>();
            // blamSbsp.UnknownSoundClustersB = new List<ScenarioStructureBsp.UnknownSoundClustersBlock>();
            // blamSbsp.UnknownSoundClustersC = new List<ScenarioStructureBsp.UnknownSoundClustersBlock>();
            // blamSbsp.CollisionMoppCodes = new List<ScenarioStructureBsp.MoppCode>();

            // blamSbsp.Unknown18 = 0;
            // blamSbsp.Unknown19 = 0;
            // blamSbsp.Unknown20 = 0;
            // blamSbsp.Unknown30 = 0;
            // blamSbsp.Unknown31 = 0;
            // blamSbsp.Unknown32 = 0;
            // blamSbsp.Unknown33 = 0;
            // blamSbsp.Unknown34 = 0;
            // blamSbsp.Unknown35 = 0;
            // blamSbsp.Unknown36 = 0;
            // blamSbsp.Unknown37 = 0;
            // blamSbsp.Unknown38 = 0;
            // blamSbsp.Unknown39 = 0;
            // blamSbsp.Unknown3  = 0;
            // blamSbsp.Unknown40 = 0;
            // blamSbsp.Unknown41 = 0;
            // blamSbsp.Unknown42 = 0;
            // blamSbsp.Unknown43 = 0;
            // blamSbsp.Unknown45 = 0;
            // blamSbsp.Unknown46 = 0;
            // blamSbsp.Unknown47 = 0;
            // blamSbsp.Unknown48 = 0;
            // blamSbsp.Unknown49 = 0;
            // blamSbsp.Unknown50 = 0;
            // blamSbsp.Unknown51 = 0;
            // blamSbsp.Unknown52 = 0;
            // blamSbsp.Unknown53 = 0;
            // blamSbsp.Unknown54 = 0;
            // blamSbsp.Unknown64 = 0;
            // blamSbsp.Unknown65 = 0;
            // blamSbsp.Unknown66 = 0;
            // blamSbsp.Unknown67 = 0;
            // blamSbsp.Unknown68 = 0;
            // blamSbsp.Unknown69 = 0;
            // blamSbsp.Unknown70 = 0;
            // blamSbsp.Unknown71 = 0;
            // blamSbsp.Unknown72 = 0;
            // blamSbsp.Unknown73 = 0;
            // blamSbsp.Unknown83 = 0;
            // blamSbsp.Unknown84 = 0;
            // blamSbsp.Unknown85 = 0;
            // blamSbsp.Unknown87 = 0;
            // blamSbsp.Unknown88 = 0;
            // blamSbsp.Unknown89 = 0;
            // blamSbsp.Unknown74 = 0;
            // blamSbsp.Unknown86 = 0;
            #endregion



            #region Geometry1

            Console.WriteLine("Porting Geometry 1...");

            // Nuke decorators and geometry resource, which is only used to place decorators on the map. if resource raw = 0, game insta crashes with a white screen of death
            for (int i = 0; i < blamSbsp.Clusters.Count; i++)
                blamSbsp.Clusters[i].DecoratorGrids = new List<ScenarioStructureBsp.Cluster.DecoratorGrid>();


            // if (BlamCache.GetRawFromID(blamSbsp.Geometry.ZoneAssetIndex) != null)
            // {
            //     var geometryFixups = new List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry>(); // has list of all groups and list of members, for members offsets
            // 
            //     var RawID = blamSbsp.Geometry.ZoneAssetIndex;
            //     var Entry = BlamCache.ResourceGestalt.RawEntries[RawID & ushort.MaxValue];
            //     geometryFixups.Add(Entry);
            // 
            //     blamSbsp.Geometry.Resource = new ResourceReference
            //     {
            //         Type = 5, // FIXME: hax
            //         DefinitionFixups = new List<ResourceDefinitionFixup>(),
            //         D3DObjectFixups = new List<D3DObjectFixup>(),
            //         OldLocationFlags = OldResourceLocationFlags.InResources,
            //         Owner = newTag
            //     };
            // 
            //     foreach (var fixup in Entry.Fixups)
            //     {
            //         var fixup2 = new ResourceDefinitionFixup
            //         {
            //             DefinitionDataOffset = (uint)fixup.BlockOffset
            //         };
            //         if (fixup.FixupType == 4)
            //         {
            //             fixup2.Address = new ResourceAddress(ResourceAddressType.Resource, fixup.Offset);
            //         }
            //         else
            //         {
            //             fixup2.Address = new ResourceAddress(ResourceAddressType.Definition, fixup.Offset);
            //         }
            //         blamSbsp.Geometry.Resource.DefinitionFixups.Add(fixup2);
            //     }
            // 
            //     blamSbsp.Geometry.Resource.DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, Entry.DefinitionAddress);
            //     blamSbsp.Geometry.Resource.DefinitionData = BlamCache.ResourceGestalt.FixupData.Skip(Entry.FixupOffset).Take(Entry.FixupSize).ToArray();
            // 
            //     var resourceDataStream = new MemoryStream(BlamCache.GetRawFromID(RawID));
            // 
            //     var geostream = new MemoryStream(blamSbsp.Geometry.Resource.DefinitionData);
            //     var georeader = new EndianReader(geostream, EndianFormat.BigEndian);
            // 
            //     georeader.BaseStream.Position = blamSbsp.Geometry.Resource.DefinitionAddress.Offset;
            //     georeader.Format = EndianFormat.BigEndian;
            // 
            //     var geodeserializer = new TagDeserializer(BlamCore.Cache.CacheVersion.Halo3ODST);
            //     var geodataContext = new DataSerializationContext(georeader, null);
            // 
            //     var geowriter = new EndianWriter(geostream, EndianFormat.BigEndian);
            //     foreach (var fixup in blamSbsp.Geometry.Resource.DefinitionFixups)
            //     {
            //         geostream.Position = fixup.DefinitionDataOffset;
            //         geowriter.Write(fixup.Address.Value);
            //     }
            //     geostream.Position = blamSbsp.Geometry.Resource.DefinitionAddress.Offset;
            // 
            //     var geometryResourceDefinition = geodeserializer.Deserialize<RenderGeometryApiResourceDefinition>(geodataContext);
            // 
            //     using (var blamResourceStream = new MemoryStream(BlamCache.GetRawFromID(RawID)))
            //     using (var edResourceStream = new MemoryStream())
            //     {
            //         //
            //         // Convert Blam render_geometry_api_resource_definition
            //         //
            // 
            //         var inVertexStream = VertexStreamFactory.Create(BlamCache.Version, blamResourceStream);
            //         var outVertexStream = VertexStreamFactory.Create(CacheContext.Version, edResourceStream);
            // 
            //         for (var i = 0; i < geometryResourceDefinition.VertexBuffers.Count; i++)
            //         {
            //             var vertexBuffer = geometryResourceDefinition.VertexBuffers[i].Definition;
            // 
            //             var count = vertexBuffer.Count;
            // 
            //             var startPos = (int)edResourceStream.Position;
            //             vertexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);
            // 
            //             blamResourceStream.Position = Entry.Fixups[i].Offset;
            // 
            //             switch (vertexBuffer.Format)
            //             {
            //                 case VertexBufferFormat.World:
            //                     ConvertVertices(count, inVertexStream.ReadWorldVertex, v =>
            //                     {
            //                         v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
            //                         outVertexStream.WriteWorldVertex(v);
            //                     });
            //                     break;
            // 
            //                 case VertexBufferFormat.Rigid:
            //                     ConvertVertices(count, inVertexStream.ReadRigidVertex, v =>
            //                     {
            //                         v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
            //                         outVertexStream.WriteRigidVertex(v);
            //                     });
            //                     break;
            // 
            //                 case VertexBufferFormat.Skinned:
            //                     ConvertVertices(count, inVertexStream.ReadSkinnedVertex, v =>
            //                     {
            //                         v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
            //                         outVertexStream.WriteSkinnedVertex(v);
            //                     });
            //                     break;
            // 
            //                 case VertexBufferFormat.StaticPerPixel:
            //                     ConvertVertices(count, inVertexStream.ReadStaticPerPixelData, outVertexStream.WriteStaticPerPixelData);
            //                     break;
            // 
            //                 case VertexBufferFormat.StaticPerVertex:
            //                     ConvertVertices(count, inVertexStream.ReadStaticPerVertexData, outVertexStream.WriteStaticPerVertexData);
            //                     break;
            // 
            //                 case VertexBufferFormat.AmbientPrt:
            //                     ConvertVertices(count, inVertexStream.ReadAmbientPrtData, outVertexStream.WriteAmbientPrtData);
            //                     break;
            // 
            //                 case VertexBufferFormat.LinearPrt:
            //                     ConvertVertices(count, inVertexStream.ReadLinearPrtData, outVertexStream.WriteLinearPrtData);
            //                     break;
            // 
            //                 case VertexBufferFormat.QuadraticPrt:
            //                     ConvertVertices(count, inVertexStream.ReadQuadraticPrtData, outVertexStream.WriteQuadraticPrtData);
            //                     break;
            // 
            //                 case VertexBufferFormat.StaticPerVertexColor:
            //                     ConvertVertices(count, inVertexStream.ReadStaticPerVertexColorData, outVertexStream.WriteStaticPerVertexColorData);
            //                     break;
            // 
            //                 case VertexBufferFormat.Decorator:
            //                     ConvertVertices(count, inVertexStream.ReadDecoratorVertex, outVertexStream.WriteDecoratorVertex);
            //                     break;
            // 
            //                 case VertexBufferFormat.World2:
            //                     vertexBuffer.Format = VertexBufferFormat.World;
            //                     goto default;
            // 
            //                 default:
            //                     // throw new NotSupportedException(vertexBuffer.Format.ToString());
            //                     Console.Write("{0} ", vertexBuffer.Format.ToString());
            //                     continue;
            //             }
            // 
            //             vertexBuffer.Data.Size = (int)edResourceStream.Position - startPos;
            //             vertexBuffer.VertexSize = (short)(vertexBuffer.Data.Size / vertexBuffer.Count);
            //         }
            //         Console.WriteLine("");
            // 
            //         for (var i = 0; i < geometryResourceDefinition.IndexBuffers.Count; i++)
            //         {
            //             var indexBuffer = geometryResourceDefinition.IndexBuffers[i].Definition;
            //    
            //             var indexCount = indexBuffer.Data.Size / 2;
            //    
            //             var inIndexStream = new IndexBufferStream(
            //                 blamResourceStream,
            //                 IndexBufferFormat.UInt16,
            //                 EndianFormat.BigEndian);
            //    
            //             var outIndexStream = new IndexBufferStream(
            //                 edResourceStream,
            //                 IndexBufferFormat.UInt16,
            //                 EndianFormat.LittleEndian);
            //    
            //             var startPos = (int)edResourceStream.Position;
            //             indexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);
            //    
            //             blamResourceStream.Position = Entry.Fixups[geometryResourceDefinition.VertexBuffers.Count * 2 + i].Offset;
            //    
            //             for (var j = 0; j < indexCount; j++)
            //                 outIndexStream.WriteIndex(inIndexStream.ReadIndex());
            //         }
            // 
            //         edResourceStream.Position = 0;
            // 
            //         var resourceContext = new ResourceSerializationContext(blamSbsp.Geometry.Resource);
            //         serializer.Serialize(resourceContext, geometryResourceDefinition);
            //         CacheContext.AddResource(blamSbsp.Geometry.Resource, ResourceLocation.Resources, edResourceStream);
            // 
            //         // var geoserializer = new TagSerializer(CacheVersion.HaloOnline106708);
            //     }
            // }
            // else
            // {
                 // Console.WriteLine("Nothing in the cache! Create empty resource.");
                 blamSbsp.Geometry = new RenderGeometry
                 {
                 };
                 blamSbsp.Geometry.Resource = new ResourceReference
                 {
                     DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, 0),
                     D3DObjectFixups = new List<D3DObjectFixup>(),
                     DefinitionFixups = new List<ResourceDefinitionFixup>(),
                     OldLocationFlags = OldResourceLocationFlags.InResources,
                     Type = 5,
                     Index = -1,
                     DefinitionData = new byte[0x30],
                     Owner = newTag
                 };
            // }

            // blamSbsp.Geometry = new RenderGeometry
            // {
            //     RuntimeFlags = (Int16)RenderGeometryRuntimeFlags.Processed + (Int16)RenderGeometryRuntimeFlags.Available + RenderGeometryRuntimeFlags.HasValidBudgets,
            // };
            // blamSbsp.Geometry.Resource = new ResourceReference
            // {
            //     DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, 0), // ??
            //     D3DObjectFixups = new List<D3DObjectFixup>(),
            //     DefinitionFixups = new List<ResourceDefinitionFixup>(),
            //     Unknown68 = 0, // 1?
            //     Type = 5, // FIXME: hax
            //     Index = -1,
            //     DefinitionData = new byte[0x30],
            //     CompressionType = -1,
            //     Owner = newTag
            // };


            // Console.WriteLine("Finished porting Geometry 1!");

            #endregion



            #region Deserialize Lbsp to get geometry resource ID

            // Console.WriteLine("Deserializing Lbsp for sbsp geometry2 resource...");

            var RawID2 = 0;

            CacheFile.IndexItem blamTagLbsp = null;

            foreach (var tag in BlamCache.IndexItems)
            {
                if (tag.ClassCode == "Lbsp" && tag.Filename == blamTagName) // Lbsp name always matches the sbsp name. there's extra Lbsps with a longer name
                {
                    blamTagLbsp = tag;
                    break;
                }
            }

            var blamDeserializerLbsp = new TagDeserializer(BlamCache.Version);
            var blamContextLbsp = new CacheSerializationContext(CacheContext, BlamCache, blamTagLbsp);
            var blamLbsp = blamDeserializerLbsp.Deserialize<ScenarioLightmapBspData>(blamContextLbsp);

            RawID2 = BitConverter.ToInt16(BitConverter.GetBytes(blamLbsp.Geometry.ZoneAssetIndex), 0);

            blamLbsp = new ScenarioLightmapBspData();
            #endregion



            #region Geometry2
            Console.WriteLine("Porting Geometry 2/Main Render Geometry...");
            var geometryFixups2 = new List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry>(); // has list of all groups and list of members, for members offsets

            var Entry2 = BlamCache.ResourceGestalt.DefinitionEntries[RawID2 & ushort.MaxValue];
            geometryFixups2.Add(Entry2);

            blamSbsp.Geometry2.Resource = new ResourceReference
            {
                Type = 5, // FIXME: hax
                DefinitionFixups = new List<ResourceDefinitionFixup>(),
                D3DObjectFixups = new List<D3DObjectFixup>(),
                OldLocationFlags = OldResourceLocationFlags.InResources,
                Owner = newTag
            };

            foreach (var fixup in Entry2.Fixups)
            {
                var fixup2 = new ResourceDefinitionFixup
                {
                    DefinitionDataOffset = (uint)fixup.BlockOffset
                };
                if (fixup.FixupType == 4)
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Resource, fixup.Offset);
                }
                else
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Definition, fixup.Offset);
                }
                blamSbsp.Geometry2.Resource.DefinitionFixups.Add(fixup2);
            }

            blamSbsp.Geometry2.Resource.DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, Entry2.DefinitionAddress);
            blamSbsp.Geometry2.Resource.DefinitionData = BlamCache.ResourceGestalt.FixupData.Skip(Entry2.Offset).Take(Entry2.Size).ToArray();

            var resourceDataStream2 = new MemoryStream(BlamCache.GetRawFromID(RawID2));

            var geostream2 = new MemoryStream(blamSbsp.Geometry2.Resource.DefinitionData);
            var georeader2 = new EndianReader(geostream2, EndianFormat.BigEndian);

            georeader2.BaseStream.Position = blamSbsp.Geometry2.Resource.DefinitionAddress.Offset;
            georeader2.Format = EndianFormat.BigEndian;

            var geodeserializer2 = new TagDeserializer(CacheVersion.Halo3ODST);
            var geodataContext2 = new DataSerializationContext(georeader2, null);

            var geowriter2 = new EndianWriter(geostream2, EndianFormat.BigEndian);
            foreach (var fixup in blamSbsp.Geometry2.Resource.DefinitionFixups)
            {
                geostream2.Position = fixup.DefinitionDataOffset;
                geowriter2.Write(fixup.Address.Value);
            }
            geostream2.Position = blamSbsp.Geometry2.Resource.DefinitionAddress.Offset;

            var resourceDefinition = geodeserializer2.Deserialize<RenderGeometryApiResourceDefinition>(geodataContext2);

            using (var blamResourceStream = new MemoryStream(BlamCache.GetRawFromID(RawID2)))
            using (var edResourceStream = new MemoryStream())
            {
                //
                // Convert Blam render_geometry_api_resource_definition
                //

                var inVertexStream = VertexStreamFactory.Create(BlamCache.Version, blamResourceStream);
                var outVertexStream = VertexStreamFactory.Create(CacheContext.Version, edResourceStream);

                for (var i = 0; i < resourceDefinition.VertexBuffers.Count; i++)
                {
                    var vertexBuffer = resourceDefinition.VertexBuffers[i].Definition;

                    var count = vertexBuffer.Count;

                    var startPos = (int)edResourceStream.Position;
                    vertexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                    blamResourceStream.Position = Entry2.Fixups[i].Offset;

                    switch (vertexBuffer.Format)
                    {
                        case VertexBufferFormat.World:
                            ConvertVertices(count, inVertexStream.ReadWorldVertex, v =>
                            {
                                v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
                                outVertexStream.WriteWorldVertex(v);
                            });
                            break;

                        case VertexBufferFormat.Rigid:
                            ConvertVertices(count, inVertexStream.ReadRigidVertex, v =>
                            {
                                v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
                                outVertexStream.WriteRigidVertex(v);
                            });
                            break;

                        case VertexBufferFormat.Skinned:
                            ConvertVertices(count, inVertexStream.ReadSkinnedVertex, v =>
                            {
                                v.Binormal = new RealVector3d(v.Position.W, v.Tangent.W, 0); // Converted shaders use this
                                outVertexStream.WriteSkinnedVertex(v);
                            });
                            break;

                        case VertexBufferFormat.StaticPerPixel:
                            ConvertVertices(count, inVertexStream.ReadStaticPerPixelData, outVertexStream.WriteStaticPerPixelData);
                            break;

                        case VertexBufferFormat.StaticPerVertex: // C100
                            ConvertVertices(count, inVertexStream.ReadStaticPerVertexData, v =>
                            {
                                v.Texcoord = v.Texcoord - 0x80808081;
                                v.Texcoord2 = v.Texcoord2 - 0x80808081;
                                v.Texcoord3 = v.Texcoord3 - 0x80808081;
                                v.Texcoord4 = v.Texcoord4 - 0x80808081;
                                v.Texcoord5 = v.Texcoord5 - 0x80808081;

                                outVertexStream.WriteStaticPerVertexData(v);
                            });
                            break;

                        case VertexBufferFormat.AmbientPrt:
                            ConvertVertices(count, inVertexStream.ReadAmbientPrtData, outVertexStream.WriteAmbientPrtData);
                            break;

                        case VertexBufferFormat.LinearPrt:
                            ConvertVertices(count, inVertexStream.ReadLinearPrtData, outVertexStream.WriteLinearPrtData);
                            break;

                        case VertexBufferFormat.QuadraticPrt:
                            ConvertVertices(count, inVertexStream.ReadQuadraticPrtData, outVertexStream.WriteQuadraticPrtData);
                            break;

                        case VertexBufferFormat.StaticPerVertexColor:
                            ConvertVertices(count, inVertexStream.ReadStaticPerVertexColorData, outVertexStream.WriteStaticPerVertexColorData);
                            break;

                        case VertexBufferFormat.Decorator:
                            ConvertVertices(count, inVertexStream.ReadDecoratorVertex, outVertexStream.WriteDecoratorVertex);
                            break;

                        case VertexBufferFormat.World2:
                            vertexBuffer.Format = VertexBufferFormat.World;
                            goto default;

                        // case VertexBufferFormat.Unknown14:
                        //     Console.Write("{0:X2}", blamResourceStream.ReadByte());
                        //     Console.Write("{0:X2}", blamResourceStream.ReadByte());
                        //     Console.Write("{0:X2}", blamResourceStream.ReadByte());
                        //     Console.Write("{0:X2}", blamResourceStream.ReadByte());
                        //     blamResourceStream.Position -= 0x4;
                        // 
                        // 
                        //     ConvertVertices(count, inVertexStream.ReadAmbientPrtData, v =>
                        //     {
                        //         Console.WriteLine(v.Brightness);
                        // 
                        //         outVertexStream.WriteAmbientPrtData(v);
                        //     });
                        //     
                        //     break;

                        default:
                            // throw new NotSupportedException(vertexBuffer.Format.ToString());
                            Console.Write("{0} ", vertexBuffer.Format.ToString());
                            break;
                    }

                    vertexBuffer.Data.Size = (int)edResourceStream.Position - startPos;
                    vertexBuffer.VertexSize = (short)(vertexBuffer.Data.Size / vertexBuffer.Count);
                }
                Console.WriteLine("");
                for (var i = 0; i < resourceDefinition.IndexBuffers.Count; i++)
                {
                    var indexBuffer = resourceDefinition.IndexBuffers[i].Definition;

                    var indexCount = indexBuffer.Data.Size / 2;

                    var inIndexStream = new IndexBufferStream(
                        blamResourceStream,
                        IndexBufferFormat.UInt16,
                        EndianFormat.BigEndian);

                    var outIndexStream = new IndexBufferStream(
                        edResourceStream,
                        IndexBufferFormat.UInt16,
                        EndianFormat.LittleEndian);

                    var startPos = (int)edResourceStream.Position;
                    indexBuffer.Data.Address = new ResourceAddress(ResourceAddressType.Resource, startPos);

                    blamResourceStream.Position = Entry2.Fixups[resourceDefinition.VertexBuffers.Count * 2 + i].Offset;

                    for (var j = 0; j < indexCount; j++)
                        outIndexStream.WriteIndex(inIndexStream.ReadIndex());
                }

                // blamSbsp.Geometry2.Resource = new ResourceReference
                // {
                //     Type = 5, // FIXME: hax
                //     DefinitionFixups = new List<ResourceDefinitionFixup>(),
                //     D3DObjectFixups = new List<D3DObjectFixup>(),
                //     Unknown68 = 1,
                //     Owner = newTag
                // };

                edResourceStream.Position = 0;

                var resourceContext = new ResourceSerializationContext(blamSbsp.Geometry2.Resource);
                serializer.Serialize(resourceContext, resourceDefinition);
                CacheContext.AddResource(blamSbsp.Geometry2.Resource, ResourceLocation.Resources, edResourceStream);
            }

            // Console.WriteLine("Finished porting Geometry 2/Main Render Geometry!");

            #endregion



            #region CollisionBSP
            // Console.WriteLine("Porting collision BSP...");
            var collisionFixups = new List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry>(); // has list of all groups and list of members, for members offsets

            var CollRawID = blamSbsp.ZoneAssetIndex3;
            var CollEntry = BlamCache.ResourceGestalt.DefinitionEntries[CollRawID & ushort.MaxValue];
            collisionFixups.Add(CollEntry);

            blamSbsp.CollisionBspResource = new ResourceReference
            {
                // Index = newTag.Index, // WARNING
                DefinitionFixups = new List<ResourceDefinitionFixup>(),
                D3DObjectFixups = new List<D3DObjectFixup>(),
                Type = 0,
                OldLocationFlags = OldResourceLocationFlags.InResources,
                Owner = newTag
            };

            foreach (var fixup in CollEntry.Fixups)
            {
                var fixup2 = new ResourceDefinitionFixup
                {
                    DefinitionDataOffset = (uint)fixup.BlockOffset
                };
                if (fixup.FixupType == 4)
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Resource, fixup.Offset);
                }
                else
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Definition, fixup.Offset);
                }
                blamSbsp.CollisionBspResource.DefinitionFixups.Add(fixup2);

            }

            blamSbsp.CollisionBspResource.DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, CollEntry.DefinitionAddress);
            blamSbsp.CollisionBspResource.DefinitionData = BlamCache.ResourceGestalt.FixupData.Skip(CollEntry.Offset).Take(CollEntry.Size).ToArray();

            var CollresourceDataStream = new MemoryStream(BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex3));

            var Collstream = new MemoryStream(blamSbsp.CollisionBspResource.DefinitionData);
            var Collreader = new EndianReader(Collstream, EndianFormat.BigEndian);

            Collreader.BaseStream.Position = blamSbsp.CollisionBspResource.DefinitionAddress.Offset;
            Collreader.Format = EndianFormat.BigEndian;

            var Colldeserializer = new TagDeserializer(BlamCore.Cache.CacheVersion.Halo3ODST);
            var ColldataContext = new DataSerializationContext(Collreader, null);

            var Collwriter = new EndianWriter(Collstream, EndianFormat.BigEndian);

            // apply fixups
            foreach (var fixup in blamSbsp.CollisionBspResource.DefinitionFixups)
            {
                Collstream.Position = fixup.DefinitionDataOffset;
                Collwriter.Write(fixup.Address.Value);
            }

            Collstream.Position = blamSbsp.CollisionBspResource.DefinitionAddress.Offset;

            var Colldef = Colldeserializer.Deserialize<StructureBspTagResources>(ColldataContext);

            var Colldeserialization = new ResourceSerializationContext(blamSbsp.CollisionBspResource);

            foreach (var thing in Colldef.Compressions)
            {
                thing.Unknown5 = new ResourceBlockReference<StructureBspTagResources.Compression.Unknown4Block>();
                thing.Unknown6 = 0.0f;
            }
            
            serializer.Serialize(Colldeserialization, Colldef);

            // Erase all definition fixups because they get regenerated with the proper locations for HO when we reserialize
            blamSbsp.CollisionBspResource.DefinitionFixups = new List<ResourceDefinitionFixup>();

            // reserialize collision BSP into HO format
            var Collserialcontext = new ResourceSerializationContext(blamSbsp.CollisionBspResource);
            CacheContext.Serializer.Serialize(Collserialcontext, Colldef);

            var Collfixups = blamSbsp.CollisionBspResource.DefinitionFixups;

            for (var i = 0; i < blamSbsp.CollisionBspResource.DefinitionFixups.Count; i++)
            {
                if (blamSbsp.CollisionBspResource.DefinitionFixups[i].Address.Value == 0x0)
                {
                    blamSbsp.CollisionBspResource.DefinitionFixups.Remove(blamSbsp.CollisionBspResource.DefinitionFixups[i]);
                }
            }

            blamSbsp.CollisionBspResource.DefinitionFixups = Collfixups;

            var bspWriter = new EndianWriter(new MemoryStream(new byte[BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex3).Length]), EndianFormat.LittleEndian);
            var bspReader = new EndianReader(new MemoryStream(BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex3)), EndianFormat.BigEndian);

            // Collisions
            foreach (var coll in Colldef.CollisionBsps)
            {
                bspReader.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                for (var i = 0; i < coll.Bsp3dNodes.Count; i++)
                {
                    // read 3d nodes in H3 format
                    var temp1 = bspReader.ReadByte();
                    var temp2 = bspReader.ReadByte();
                    var temp3 = bspReader.ReadByte();
                    var temp4 = bspReader.ReadByte();
                    var temp5 = bspReader.ReadByte();
                    var temp6 = bspReader.ReadByte();
                    var temp7 = bspReader.ReadInt16();

                    // write them in HO format
                    bspWriter.Write(temp7);
                    bspWriter.Write(temp6);
                    bspWriter.Write(temp5);
                    bspWriter.Write(temp4);
                    bspWriter.Write(temp3);
                    bspWriter.Write(temp2);
                    bspWriter.Write(temp1);

                }

                bspReader.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                for (var i = 0; i < coll.Bsp2dNodes.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Planes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Planes.Address.Offset;
                for (var i = 0; i < coll.Planes.Count; i++)
                {

                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                }

                bspReader.BaseStream.Position = coll.Leaves.Address.Offset;
                bspWriter.BaseStream.Position = coll.Leaves.Address.Offset;
                for (var i = 0; i < coll.Leaves.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt32());
                }

                bspReader.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                for (var i = 0; i < coll.Bsp2dReferences.Count; i++)
                {

                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Surfaces.Address.Offset;
                bspWriter.BaseStream.Position = coll.Surfaces.Address.Offset;
                for (var i = 0; i < coll.Surfaces.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadByte());
                    bspWriter.Write(bspReader.ReadByte());
                }

                bspReader.BaseStream.Position = coll.Edges.Address.Offset;
                bspWriter.BaseStream.Position = coll.Edges.Address.Offset;
                for (var i = 0; i < coll.Edges.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Vertices.Address.Offset;
                bspWriter.BaseStream.Position = coll.Vertices.Address.Offset;
                for (var i = 0; i < coll.Vertices.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }
            }

            // Large collision BSPs
            // Collisions
            foreach (var coll in Colldef.LargeCollisionBsps)
            {
                bspReader.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                for (var i = 0; i < coll.Bsp3dNodes.Count; i++)
                {
                    // read 3d nodes in H3 format
                    var temp1 = bspReader.ReadByte();
                    var temp2 = bspReader.ReadByte();
                    var temp3 = bspReader.ReadByte();
                    var temp4 = bspReader.ReadByte();
                    var temp5 = bspReader.ReadByte();
                    var temp6 = bspReader.ReadByte();
                    var temp7 = bspReader.ReadInt16();

                    bspWriter.Write(temp7);
                    bspWriter.Write(temp6);
                    bspWriter.Write(temp5);
                    bspWriter.Write(temp4);
                    bspWriter.Write(temp3);
                    bspWriter.Write(temp2);
                    bspWriter.Write(temp1);
                }

                bspReader.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                for (var i = 0; i < coll.Bsp2dNodes.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Planes.Address.Offset;
                bspWriter.BaseStream.Position = coll.Planes.Address.Offset;
                for (var i = 0; i < coll.Planes.Count; i++)
                {

                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                }

                bspReader.BaseStream.Position = coll.Leaves.Address.Offset;
                bspWriter.BaseStream.Position = coll.Leaves.Address.Offset;
                for (var i = 0; i < coll.Leaves.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt32());
                }

                bspReader.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                bspWriter.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                for (var i = 0; i < coll.Bsp2dReferences.Count; i++)
                {

                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Surfaces.Address.Offset;
                bspWriter.BaseStream.Position = coll.Surfaces.Address.Offset;
                for (var i = 0; i < coll.Surfaces.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadByte());
                    bspWriter.Write(bspReader.ReadByte());
                }

                bspReader.BaseStream.Position = coll.Edges.Address.Offset;
                bspWriter.BaseStream.Position = coll.Edges.Address.Offset;
                for (var i = 0; i < coll.Edges.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = coll.Vertices.Address.Offset;
                bspWriter.BaseStream.Position = coll.Vertices.Address.Offset;
                for (var i = 0; i < coll.Vertices.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }
            }

            // Compressions
            foreach (var comp in Colldef.Compressions)
            {
                bspReader.BaseStream.Position = comp.Bsp3dNodes.Address.Offset;
                bspWriter.BaseStream.Position = comp.Bsp3dNodes.Address.Offset;
                for (var i = 0; i < comp.Bsp3dNodes.Count; i++)
                {
                    // read 3d nodes in H3 format
                    var temp1 = bspReader.ReadByte();
                    var temp2 = bspReader.ReadByte();
                    var temp3 = bspReader.ReadByte();
                    var temp4 = bspReader.ReadByte();
                    var temp5 = bspReader.ReadByte();
                    var temp6 = bspReader.ReadByte();
                    var temp7 = bspReader.ReadInt16();

                    bspWriter.Write(temp7);
                    bspWriter.Write(temp6);
                    bspWriter.Write(temp5);
                    bspWriter.Write(temp4);
                    bspWriter.Write(temp3);
                    bspWriter.Write(temp2);
                    bspWriter.Write(temp1);
                }

                bspReader.BaseStream.Position = comp.Bsp2dNodes.Address.Offset;
                bspWriter.BaseStream.Position = comp.Bsp2dNodes.Address.Offset;
                for (var i = 0; i < comp.Bsp2dNodes.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = comp.Planes.Address.Offset;
                bspWriter.BaseStream.Position = comp.Planes.Address.Offset;
                for (var i = 0; i < comp.Planes.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                }

                bspReader.BaseStream.Position = comp.Leaves.Address.Offset;
                bspWriter.BaseStream.Position = comp.Leaves.Address.Offset;
                for (var i = 0; i < comp.Leaves.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt32());
                }

                bspReader.BaseStream.Position = comp.Bsp2dReferences.Address.Offset;
                bspWriter.BaseStream.Position = comp.Bsp2dReferences.Address.Offset;
                for (var i = 0; i < comp.Bsp2dReferences.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = comp.Surfaces.Address.Offset;
                bspWriter.BaseStream.Position = comp.Surfaces.Address.Offset;
                for (var i = 0; i < comp.Surfaces.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadByte());
                    bspWriter.Write(bspReader.ReadByte());
                }

                bspReader.BaseStream.Position = comp.Edges.Address.Offset;
                bspWriter.BaseStream.Position = comp.Edges.Address.Offset;
                for (var i = 0; i < comp.Edges.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = comp.Vertices.Address.Offset;
                bspWriter.BaseStream.Position = comp.Vertices.Address.Offset;
                for (var i = 0; i < comp.Vertices.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadSingle());
                    bspWriter.Write(bspReader.ReadInt16());
                    bspWriter.Write(bspReader.ReadInt16());
                }

                bspReader.BaseStream.Position = comp.Unknown1.Address.Offset;
                bspWriter.BaseStream.Position = comp.Unknown1.Address.Offset;
                for (var i = 0; i < comp.Unknown1.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt32());
                }

                bspReader.BaseStream.Position = comp.Unknown2.Address.Offset;
                bspWriter.BaseStream.Position = comp.Unknown2.Address.Offset;
                for (var i = 0; i < comp.Unknown2.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt32());
                    bspWriter.Write(bspReader.ReadUInt32());
                }

                bspReader.BaseStream.Position = comp.Unknown3.Address.Offset;
                bspWriter.BaseStream.Position = comp.Unknown3.Address.Offset;
                for (var i = 0; i < comp.Unknown3.Count; i++)
                {
                    bspWriter.Write(bspReader.ReadUInt32());
                }

                foreach (var coll in comp.CollisionBsps)
                {
                    bspReader.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Bsp3dNodes.Address.Offset;
                    for (var i = 0; i < coll.Bsp3dNodes.Count; i++)
                    {
                        // read 3d nodes in H3 format
                        var temp1 = bspReader.ReadByte();
                        var temp2 = bspReader.ReadByte();
                        var temp3 = bspReader.ReadByte();
                        var temp4 = bspReader.ReadByte();
                        var temp5 = bspReader.ReadByte();
                        var temp6 = bspReader.ReadByte();
                        var temp7 = bspReader.ReadInt16();

                        bspWriter.Write(temp7);
                        bspWriter.Write(temp6);
                        bspWriter.Write(temp5);
                        bspWriter.Write(temp4);
                        bspWriter.Write(temp3);
                        bspWriter.Write(temp2);
                        bspWriter.Write(temp1);
                    }

                    bspReader.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Bsp2dNodes.Address.Offset;
                    for (var i = 0; i < coll.Bsp2dNodes.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                    }

                    bspReader.BaseStream.Position = coll.Planes.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Planes.Address.Offset;
                    for (var i = 0; i < coll.Planes.Count; i++)
                    {

                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                    }

                    bspReader.BaseStream.Position = coll.Leaves.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Leaves.Address.Offset;
                    for (var i = 0; i < coll.Leaves.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadUInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt32());
                    }

                    bspReader.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Bsp2dReferences.Address.Offset;
                    for (var i = 0; i < coll.Bsp2dReferences.Count; i++)
                    {

                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                    }

                    bspReader.BaseStream.Position = coll.Surfaces.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Surfaces.Address.Offset;
                    for (var i = 0; i < coll.Surfaces.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadByte());
                        bspWriter.Write(bspReader.ReadByte());
                    }

                    bspReader.BaseStream.Position = coll.Edges.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Edges.Address.Offset;
                    for (var i = 0; i < coll.Edges.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                    }

                    bspReader.BaseStream.Position = coll.Vertices.Address.Offset;
                    bspWriter.BaseStream.Position = coll.Vertices.Address.Offset;
                    for (var i = 0; i < coll.Vertices.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadSingle());
                        bspWriter.Write(bspReader.ReadInt16());
                        bspWriter.Write(bspReader.ReadInt16());
                    }
                }

                foreach (var mopp in comp.CollisionMoppCodes)
                {
                    bspReader.BaseStream.Position = mopp.Data.Address.Offset;
                    bspWriter.BaseStream.Position = mopp.Data.Address.Offset;
                    for (var i = 0; i < mopp.Data.Count; i++)
                    {
                        bspWriter.Write(bspReader.ReadByte());
                    }
                }
            }

            bspWriter.BaseStream.Position = 0;
            
            CacheContext.AddResource(blamSbsp.CollisionBspResource, ResourceLocation.Resources, bspWriter.BaseStream);
            // Console.WriteLine("Finished porting collision BSP!");

            #endregion



            #region BSP Tagblock Resource 4th Resource, pathfinding resource
            // Console.WriteLine("Porting tagblock resource/4th resource...");

            blamSbsp.PathfindingResource = new ResourceReference
            {
                DefinitionFixups = new List<ResourceDefinitionFixup>(),
                D3DObjectFixups = new List<D3DObjectFixup>(),
                Owner = newTag
            };

            var tagblockFixups = new List<BlamCore.Legacy.cache_file_resource_gestalt.RawEntry>(); // has list of all groups and list of members, for members offsets

            var RawID4 = blamSbsp.ZoneAssetIndex4;
            var Entry4 = BlamCache.ResourceGestalt.DefinitionEntries[RawID4 & ushort.MaxValue];
            tagblockFixups.Add(Entry4);

            foreach (var fixup in Entry4.Fixups)
            {
                var fixup2 = new ResourceDefinitionFixup
                {
                    DefinitionDataOffset = (uint)fixup.BlockOffset
                };
                if (fixup.FixupType == 4)
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Resource, fixup.Offset);
                }
                else
                {
                    fixup2.Address = new ResourceAddress(ResourceAddressType.Definition, fixup.Offset);
                }
                blamSbsp.PathfindingResource.DefinitionFixups.Add(fixup2);
            }

            blamSbsp.PathfindingResource.DefinitionAddress = new ResourceAddress(ResourceAddressType.Definition, Entry4.DefinitionAddress);
            blamSbsp.PathfindingResource.DefinitionData = BlamCache.ResourceGestalt.FixupData.Skip(Entry4.Offset).Take(Entry4.Size).ToArray();

            // Need to add Index 4 into SBSP struct
            var tagblockDataStream = new MemoryStream(BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex4));

            var tagblockStream = new MemoryStream(blamSbsp.PathfindingResource.DefinitionData);
            var tagblockReader = new EndianReader(tagblockStream, EndianFormat.BigEndian);

            tagblockReader.BaseStream.Position = blamSbsp.PathfindingResource.DefinitionAddress.Offset;
            tagblockReader.Format = EndianFormat.BigEndian;

            var tagblockDeserializer = new TagDeserializer(BlamCore.Cache.CacheVersion.Halo3ODST);
            var tagblockDataContext = new DataSerializationContext(tagblockReader, null);

            var tagblockWriter = new EndianWriter(tagblockStream, EndianFormat.BigEndian);
            foreach (var fixup in blamSbsp.PathfindingResource.DefinitionFixups)
            {
                tagblockStream.Position = fixup.DefinitionDataOffset;
                tagblockWriter.Write(fixup.Address.Value);
            }
            tagblockStream.Position = blamSbsp.PathfindingResource.DefinitionAddress.Offset;

            var tagblockDef = tagblockDeserializer.Deserialize<StructureBspCacheFileTagResources>(tagblockDataContext);

            var tagblockSerialization = new ResourceSerializationContext(blamSbsp.PathfindingResource);

            blamSbsp.PathfindingResource.Type = 5;

            var tagblockResourceWriter = new EndianWriter(new MemoryStream(new byte[BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex4).Length]), EndianFormat.LittleEndian);
            var tagblockResourceReader = new EndianReader(new MemoryStream(BlamCache.GetRawFromID(blamSbsp.ZoneAssetIndex4)), EndianFormat.BigEndian);

            // fix endian
            tagblockResourceReader.BaseStream.Position = tagblockDef.UnknownRaw1sts.Address.Offset;
            tagblockResourceWriter.BaseStream.Position = tagblockDef.UnknownRaw1sts.Address.Offset;
            for (var i = 0; i < tagblockDef.UnknownRaw1sts.Count; i++)
            {
                tagblockResourceWriter.Write(tagblockResourceReader.ReadUInt16());
                tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
            }

            tagblockResourceReader.BaseStream.Position = tagblockDef.UnknownRaw7ths.Address.Offset;
            tagblockResourceWriter.BaseStream.Position = tagblockDef.UnknownRaw7ths.Address.Offset;
            for (var i = 0; i < tagblockDef.UnknownRaw7ths.Count; i++)
            {
                tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
            }

            // Not really required since int8/byte are endian agnostic but do it because otherwise all of them would be 0x00
            tagblockResourceReader.BaseStream.Position = tagblockDef.UnknownRaw6ths.Address.Offset;
            tagblockResourceWriter.BaseStream.Position = tagblockDef.UnknownRaw6ths.Address.Offset;
            for (var i = 0; i < tagblockDef.UnknownRaw6ths.Count; i++)
            {
                tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
                tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
            }

            /*
            foreach (var data in tagblockDef.Pathfinding)
            {
                tagblockResourceReader.BaseStream.Position = data.sectors.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.sectors.Address.Offset;
                for (var i = 0; i < data.sectors.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
                }

                tagblockResourceReader.BaseStream.Position = data.link.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.link.Address.Offset;
                for (var i = 0; i < data.link.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                }

                tagblockResourceReader.BaseStream.Position = data.references.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.references.Address.Offset;
                for (var i = 0; i < data.references.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
                }

                tagblockResourceReader.BaseStream.Position = data.bsp2dnodes.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.bsp2dnodes.Address.Offset;
                for (var i = 0; i < data.bsp2dnodes.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                }

                tagblockResourceReader.BaseStream.Position = data.vertices.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.vertices.Address.Offset;
                for (var i = 0; i < data.vertices.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadSingle());
                }

                foreach(var reference in data.objectreferences)
                {
                    foreach(var unk in reference.unknown2)
                    {
                        // TODO: tagblockResourceReader.BaseStream.Position = unk.unknown3.Address.Offset;
                        // TODO: tagblockResourceWriter.BaseStream.Position = unk.unknown3.Address.Offset;
                        for (var i = 0; i < unk.unknown3.Count; i++)
                        {
                            bspWriter.Write(tagblockResourceReader.ReadInt32());
                        }
                    }
                }

                tagblockResourceReader.BaseStream.Position = data.pathfindinghints.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.pathfindinghints.Address.Offset;
                for (var i = 0; i < data.pathfindinghints.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                }

                tagblockResourceReader.BaseStream.Position = data.instancedgeometryreferences.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.instancedgeometryreferences.Address.Offset;
                for (var i = 0; i < data.instancedgeometryreferences.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt16());
                }

                tagblockResourceReader.BaseStream.Position = data.UnknownTagblock1.Address.Offset;
                tagblockResourceWriter.BaseStream.Position = data.UnknownTagblock1.Address.Offset;
                for (var i = 0; i < data.UnknownTagblock1.Count; i++)
                {
                    tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
                }

                foreach(var unk in data.UnknownTagblock2)
                {
                    // TODO: tagblockResourceReader.BaseStream.Position = unk.Unknown.Address.Offset;
                    // TODO: tagblockResourceWriter.BaseStream.Position = unk.Unknown.Address.Offset;
                    for (var i = 0; i < unk.Unknown.Count; i++)
                    {
                        tagblockResourceWriter.Write(tagblockResourceReader.ReadInt32());
                    }
                }

            }*/

            tagblockResourceWriter.BaseStream.Position = 0;
            serializer.Serialize(tagblockSerialization, tagblockDef);
            CacheContext.AddResource(blamSbsp.PathfindingResource, ResourceLocation.Resources, tagblockResourceWriter.BaseStream);
            
            blamSbsp.PathfindingResource.Type = 7;

            // var tagblockSerializer = new TagSerializer(CacheVersion.HaloOnline106708);

            // Console.WriteLine("Finished porting tagblock resource/4th resource!");
            #endregion



            bspWriter.Flush();
            bspWriter.Close();

            blamSbsp.Geometry.Resource.Unknown68 = 1;
            blamSbsp.Geometry2.Resource.Unknown68 = 1;
            blamSbsp.CollisionBspResource.Unknown68 = 1;
            blamSbsp.PathfindingResource.Unknown68 = 1;

            #region Tweak some tagblocks
            foreach (var mat in blamSbsp.CollisionMaterials)
            {
                try // TO FIX
                {
                    mat.RenderMethod = ConvertTagReference(mat.RenderMethod.Index);
                }
                catch (Exception)
                {
                    mat.RenderMethod = CacheContext.GetTag(0x101F);
                }

                if (mat.RenderMethod == null)
                    mat.RenderMethod = CacheContext.GetTag(0x101F);
            }

            foreach (var mat in blamSbsp.Materials)
            {
                try
                {
                    mat.RenderMethod = ConvertTagReference(mat.RenderMethod.Index);
                }
                catch (Exception)
                {
                    mat.RenderMethod = CacheContext.GetTag(0x101F);
                }
                if (mat.RenderMethod == null)
                    mat.RenderMethod = CacheContext.GetTag(0x101F);
            }

            // for (var i = 0; i < blamSbsp.Decorators.Count; i++)
            // {
            //     blamSbsp.Decorators[i] = CacheContext.TagCache.Index[0x2ecd];
            // }

            // foreach (var geo in blamSbsp.InstancedGeometryInstances)
            // {
            //     foreach (var def in geo.CollisionDefinitions)
            //     {
            //         def.BspIndex = 0;
            //     }
            // }

            foreach (var cluster in blamSbsp.Clusters)
            {
                cluster.BackgroundSoundEnvironmentIndex = -1;
            }

            foreach (var thing in blamSbsp.UnknownSoundClustersA)
            {
                thing.BackgroundSoundEnvironmentIndex = -1;
            }

            foreach (var thing in blamSbsp.UnknownSoundClustersB)
            {
                thing.BackgroundSoundEnvironmentIndex = -1;
            }

            foreach (var thing in blamSbsp.UnknownSoundClustersC)
            {
                thing.BackgroundSoundEnvironmentIndex = -1;
            }

            //
            // Set PRTtype to None temporarly
            //

            foreach (var a in blamSbsp.Geometry2.Meshes)
                a.PrtType = PrtType.None;
            #endregion

            //
            // Finalize new ElDorado tag
            //

            using (var cacheStream = CacheContext.OpenTagCacheReadWrite())
            {
                // Console.WriteLine($"Writing \"{CacheContext.TagNames[newTag.Index]}.scenario_structure_bsp\" tag data...");

                var context = new TagSerializationContext(cacheStream, CacheContext, newTag);
                CacheContext.Serializer.Serialize(context, blamSbsp);
            }

            //
            // Save new string_ids
            //

            if (CacheContext.StringIdCache.Strings.Count != initialStringIDCount)
            {
                // Console.Write("Saving string_ids...");

                using (var stringIdStream = CacheContext.StringIdCacheFile.Open(FileMode.Open, FileAccess.ReadWrite))
                    CacheContext.StringIdCache.Save(stringIdStream);

                // Console.WriteLine("done.");
            }

            //
            // Done!
            //

            // Console.WriteLine("Ported scenario_structure_bsp \"" + blamTagName + "\" successfully!");

            return true;
        }

        private CachedTagInstance ConvertTagReference(int index)
        {
            var instance = BlamCache.IndexItems.Find(i => i.ID == index);

            if (instance != null)
            {
                var chars = new char[] { ' ', ' ', ' ', ' ' };
                for (var i = 0; i < instance.ClassCode.Length; i++)
                    chars[i] = instance.ClassCode[i];

                var tags = CacheContext.TagCache.Index.FindAllInGroup(new string(chars));

                foreach (var tag in tags)
                {
                    if (instance.Filename == CacheContext.TagNames[tag.Index])
                        return tag;
                }
            }

            return null;
        }
    }
}