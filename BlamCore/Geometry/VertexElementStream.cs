﻿using System;
using System.IO;
using BlamCore.Common;
using BlamCore.IO;

namespace BlamCore.Geometry
{
    public class VertexElementStream
    {
        private EndianReader Reader { get; }
        private EndianWriter Writer { get; }

        public VertexElementStream(Stream baseStream, EndianFormat format = EndianFormat.LittleEndian)
        {
            Reader = new EndianReader(baseStream, format);
            Writer = new EndianWriter(baseStream, format);
        }

        public float ReadFloat1()
        {
            return Reader.ReadSingle();
        }

        public void WriteFloat1(float v)
        {
            Writer.Write(v);
        }

        public RealVector2d ReadFloat2()
        {
            return new RealVector2d(Read(2, () => Reader.ReadSingle()));
        }

        public void WriteFloat2(RealVector2d v)
        {
            Write(v.ToArray(), 2, e => Writer.Write(e));
        }

        public RealVector3d ReadFloat3()
        {
            return new RealVector3d(Read(3, () => Reader.ReadSingle()));
        }

        public void WriteFloat3(RealVector3d v)
        {
            Write(v.ToArray(), 3, e => Writer.Write(e));
        }

        public RealVector4d ReadFloat4()
        {
            return new RealVector4d(Read(4, () => Reader.ReadSingle()));
        }

        public void WriteFloat4(RealVector4d v)
        {
            Write(v.ToArray(), 4, e => Writer.Write(e));
        }

        public uint ReadColor()
        {
            return Reader.ReadUInt32();
        }

        public void WriteColor(uint v)
        {
            Writer.Write(v);
        }

        public byte[] ReadUByte4()
        {
            return Reader.ReadBytes(4);
        }

        public void WriteUByte4(byte[] v)
        {
            Writer.Write(v, 0, 4);
        }

        public short[] ReadShort2()
        {
            return Read(2, () => Reader.ReadInt16());
        }

        public void WriteShort2(short[] v)
        {
            Write(v, 2, e => Writer.Write(e));
        }

        public short[] ReadShort4()
        {
            return Read(2, () => Reader.ReadInt16());
        }

        public void WriteShort4(short[] v)
        {
            Write(v, 4, e => Writer.Write(e));
        }

        public RealVector4d ReadUByte4N()
        {
            return new RealVector4d(Read(4, () => Reader.ReadByte() / 255.0f));
        }

        public void WriteUByte4N(RealVector4d v)
        {
            Write(v.ToArray(), 4, e => Writer.Write((byte)(Clamp(e) * 255.0f)));
        }

        public RealVector2d ReadShort2N()
        {
            return new RealVector2d(Read(2, () => Reader.ReadInt16() / 32767.0f));
        }

        public void WriteShort2N(RealVector2d v)
        {
            Write(v.ToArray(), 2, e => Writer.Write((short)(Clamp(e) * 32767.0f)));
        }

        public RealVector4d ReadShort4N()
        {
            return new RealVector4d(Read(4, () => Reader.ReadInt16() / 32767.0f));
        }

        public void WriteShort4N(RealVector4d v)
        {
            Write(v.ToArray(), 4, e => Writer.Write((short)(Clamp(e) * 32767.0f)));
        }

        public RealVector2d ReadUShort2N()
        {
            return new RealVector2d(Read(2, () => Reader.ReadUInt16() / 65535.0f));
        }

        public void WriteUShort2N(RealVector2d v)
        {
            Write(v.ToArray(), 2, e => Writer.Write((ushort)(Clamp(e) * 65535.0f)));
        }

        public RealVector4d ReadUShort4N()
        {
            return new RealVector4d(Read(4, () => Reader.ReadUInt16() / 65535.0f));
        }

        public void WriteUShort4N(RealVector4d v)
        {
            Write(v.ToArray(), 4, e => Writer.Write((ushort)(Clamp(e) * 65535.0f)));
        }

        public RealVector3d ReadUDec3()
        {
            var val = Reader.ReadUInt32();
            var x = (float)(val >> 22);
            var y = (float)((val >> 12) & 0x3FF);
            var z = (float)((val >> 2) & 0x3FF);
            return new RealVector3d(x, y, z);
        }

        public void WriteUDec3(RealVector3d v)
        {
            var x = (uint)v.I & 0x3FF;
            var y = (uint)v.J & 0x3FF;
            var z = (uint)v.K & 0x3FF;
            Writer.Write((x << 22) | (y << 12) | (z << 2));
        }

        public RealVector3d ReadDec3N()
        {
            var val = Reader.ReadUInt32();
            var x = ((val >> 22) - 512) / 511.0f;
            var y = (((val >> 12) & 0x3FF) - 512) / 511.0f;
            var z = (((val >> 2) & 0x3FF) - 512) / 511.0f;
            return new RealVector3d(x, y, z);
        }

        public void WriteDec3N(RealVector3d v)
        {
            var x = (((uint)(Clamp(v.I) * 511.0f)) + 512) & 0x3FF;
            var y = (((uint)(Clamp(v.J) * 511.0f)) + 512) & 0x3FF;
            var z = (((uint)(Clamp(v.K) * 511.0f)) + 512) & 0x3FF;
            Writer.Write((x << 22) | (y << 12) | (z << 2));
        }

        public RealVector3d ReadDHenN3()
        {
            var DHenN3 = Reader.ReadUInt32();

            uint[] SignExtendX = { 0x00000000, 0xFFFFFC00 };
            uint[] SignExtendYZ = { 0x00000000, 0xFFFFF800 };
            uint temp;

            temp = DHenN3 & 0x3FF;
            var a = (float)(short)(temp | SignExtendX[temp >> 9]) / (float)0x1FF;

            temp = (DHenN3 >> 10) & 0x7FF;
            var b = (float)(short)(temp | SignExtendYZ[temp >> 10]) / (float)0x3FF;

            temp = (DHenN3 >> 21) & 0x7FF;
            var c = (float)(short)(temp | SignExtendYZ[temp >> 10]) / (float)0x3FF;

            return new RealVector3d(a, b, c);
        }

        public RealVector2d ReadFloat16_2()
        {
            return new RealVector2d(Read(2, () => (float)Half.ToHalf(Reader.ReadUInt16())));
        }

        public void WriteFloat16_2(RealVector2d v)
        {
            Write(v.ToArray(), 2, e => Writer.Write(Half.GetBytes(new Half(e))));
        }

        public RealVector4d ReadFloat16_4()
        {
            return new RealVector4d(Read(4, () => (float)Half.ToHalf(Reader.ReadUInt16())));
        }

        public void WriteFloat16_4(RealVector4d v)
        {
            Write(v.ToArray(), 4, e => Writer.Write(Half.GetBytes(new Half(e))));
        }

        private static T[] Read<T>(int count, Func<T> readFunc)
        {
            var c = new T[count];
            for (var i = 0; i < count; i++)
                c[i] = readFunc();
            return c;
        }

        private static void Write<T>(T[] elems, int count, Action<T> writeAction)
        {
            for (var i = 0; i < count; i++)
                writeAction(elems[i]);
        }

        private static float Clamp(float e)
        {
            return Math.Max(-1.0f, Math.Min(1.0f, e));
        }
    }
}
