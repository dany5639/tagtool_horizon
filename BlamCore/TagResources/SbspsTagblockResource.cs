﻿using System.Collections.Generic;
using BlamCore.Serialization;
using BlamCore.Cache;

namespace BlamCore.TagResources
{
    // All of this is in the tag in Halo 3
    [TagStructure(Name = "sbsp_tagblock_resource", Size = 0x30, MinVersion = Cache.CacheVersion.Halo3ODST)]
    public class SbspTagblockResource
    {
        public ResourceBlockReference<UnknownRaw6th> UnknownRaw6ths;
        public ResourceBlockReference<Unknown1> Unknown1s;
        public ResourceBlockReference<UnknownRaw7th> UnknownRaw7ths;
        public int Pathfinding1;
        public int Pathfinding2;
        public int Pathfinding3;
    }

    public class Unknown1
    {
        public ushort unknown1;
        public short unknown2;
    }

    public class UnknownRaw6th
    {
        // this is 2 shorts in halo 3, seems they upgraded for ODST/HO
        public int unknown1;
        public int unknown2;
    }

    public class UnknownRaw7th
    {
        public short unknown1;
        public short unknown2;
    }

    [TagStructure]
    public class PathFindingData
    {
        public ResourceBlockReference<Sector> sectors;
        public ResourceBlockReference<Link> link;
        public ResourceBlockReference<Ref> references;
        public ResourceBlockReference<Bsp2dNode> bsp2dnodes;
        public ResourceBlockReference<Vertex> vertices;
        public List<ObjectRef> objectreferences;
        public ResourceBlockReference<PathfindingHint> pathfindinghints;
        public ResourceBlockReference<InstancedGeometryReference> instancedgeometryreferences;
        public int structurechecksum;
        public int unknown1;
        public int unknown2;
        public int unknown3;
        public ResourceBlockReference<Unknown1Tagblock> UnknownTagblock1;
        public List<Unknown2Tagblock> UnknownTagblock2;
        public List<Unknown3Tagblock> UnknownTagblock3;
    }

    [TagStructure]
    public class Sector
    {
        public short PathfindingSectorFlags;
        public short HintIndex;
        public int FirstLink;
    }
    [TagStructure]
    public class Link
    {
        public short Vertex1;
        public short Vertex2;
        public short LinkFlags;
        public short HintIndex;
        public short ForwardLink;
        public short ReverseLink;
        public short LeftSector;
        public short RightSector;
    }
    [TagStructure]
    public class Ref
    {
        public int NodeOrSectorRef;
    }
    [TagStructure]
    public class Bsp2dNode
    {
        public float PlaneI;
        public float PlaneJ;
        public float PlaneD;
        public short LeftChild;
        public short RightChild;
    }
    [TagStructure]
    public class Vertex
    {
        public float x;
        public float y;
        public float z;
    }
    [TagStructure]
    public class ObjectRef
    {
        public int unknown;
        public List<Unknown1> unknown2;
        public int unknown3;
        public short Unknown4;
        public short Unknown5;

        [TagStructure]
        public class Unknown1
        {
            public int unknown1;
            public int unknown2;
            public ResourceBlockReference<Unknown3> unknown3;

            public class Unknown3
            {
                public int unknown;
            }
        }
    }
    [TagStructure]
    public class PathfindingHint
    {
        public short HintType;
        public short NextHintIndex;
        public short HintData0;
        public short HintData1;
        public short HintData2;
        public short HintData3;
        public short HintData4;
        public short HintData5;
        public short HintData6;
        public short HintData7;
    }
    [TagStructure]
    public class InstancedGeometryReference
    {
        public short PathfindingObjectInstance;
        public short Unknown;
    }
    [TagStructure]
    public class Unknown1Tagblock
    {
        public int Unknown;
    }
    [TagStructure]
    public class Unknown2Tagblock
    {
        public ResourceBlockReference<Unknown1> Unknown;

        public class Unknown1
        {
            public int Unknown;
        }
    }
    [TagStructure]
    public class Unknown3Tagblock
    {
        public short Unknown1;
        public short Unknown2;
        public float Unknown3;

        public ResourceBlockReference<Unknown> Unknown4;

        public class Unknown
        {
            public short Unknown1;
            public short Unknown2;
        }
    }


}