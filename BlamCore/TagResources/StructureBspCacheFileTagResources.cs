﻿using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;
using BlamCore.TagDefinitions;
using System.Collections.Generic;

namespace BlamCore.TagResources
{
    [TagStructure(Name = "structure_bsp_cache_file_tag_resources", Size = 0x30, MinVersion = CacheVersion.Halo3ODST)]
    public class StructureBspCacheFileTagResources
    {
        public ResourceBlockReference<ScenarioStructureBsp.UnknownRaw6th> UnknownRaw6ths;
        public ResourceBlockReference<ScenarioStructureBsp.UnknownRaw1st> UnknownRaw1sts;
        public ResourceBlockReference<ScenarioStructureBsp.UnknownRaw7th> UnknownRaw7ths;
        public ResourceBlockReference<PathfindingDatum> PathfindingData;

        [TagStructure(Size = 0xA0)]
        public class PathfindingDatum
        {
            public ResourceBlockReference<ScenarioStructureBsp.PathfindingDatum.Sector> Sectors;
            public ResourceBlockReference<ScenarioStructureBsp.PathfindingDatum.Link> Links;
            public ResourceBlockReference<ScenarioStructureBsp.PathfindingDatum.Reference> References;
            public ResourceBlockReference<ScenarioStructureBsp.PathfindingDatum.Bsp2dNode> Bsp2dNodes;
            public ResourceBlockReference<RealPoint3d> Vertices;
            public List<ObjectReference> ObjectReferences;
            public ResourceBlockReference<ScenarioStructureBsp.PathfindingDatum.PathfindingHint> PathfindingHints;
            public ResourceBlockReference<ScenarioStructureBsp.PathfindingDatum.InstancedGeometryReference> InstancedGeometryReferences;
            public int StructureChecksum;
            public int Unknown1;
            public int Unknown2;
            public int Unknown3;
            public ResourceBlockReference<ScenarioStructureBsp.PathfindingDatum.Unknown1Block> Unknown1s;
            public List<Unknown2Block> Unknown2s;
            public List<Unknown3Block> Unknown3s;
            public ResourceBlockReference<ScenarioStructureBsp.PathfindingDatum.UnknownBlock4> Unknown4s;

            [TagStructure(Size = 0x18)]
            public class ObjectReference
            {
                public int Unknown;
                public List<Unknown1Block> unknown2;
                public int Unknown3;
                public short Unknown4;
                public short Unknown5;

                [TagStructure(Size = 0x18)]
                public class Unknown1Block
                {
                    public int Unknown1;
                    public int Unknown2;
                    public ResourceBlockReference<Unknown3Block> Unknown3;

                    [TagStructure(Size = 0x4)]
                    public class Unknown3Block
                    {
                        public int Unknown;
                    }
                }
            }

            [TagStructure(Size = 0xC)]
            public class Unknown2Block
            {
                public ResourceBlockReference<Unknown1> Unknown;

                [TagStructure(Size = 0x4)]
                public class Unknown1
                {
                    public int Unknown;
                }
            }

            [TagStructure(Size = 0x14)]
            public class Unknown3Block
            {
                public short Unknown1;
                public short Unknown2;
                public float Unknown3;
                public ResourceBlockReference<Unknown> Unknown4;

                [TagStructure(Size = 0x4)]
                public class Unknown
                {
                    public short Unknown1;
                    public short Unknown2;
                }
            }
        }
    }
}