using BlamCore.Serialization;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "shader_decal", Tag = "rmd ", Size = 0x0)]
    public class ShaderDecal : RenderMethod
    {
    }
}
