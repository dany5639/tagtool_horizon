using System.Collections.Generic;
using BlamCore.Serialization;
using BlamCore.Cache;
using System;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "create", Tag = "bloc", Size = 0x4, MaxVersion = CacheVersion.Halo3Beta)]
    [TagStructure(Name = "crate", Tag = "bloc", Size = 0x14, MinVersion = CacheVersion.Halo3Retail)]
    public class Crate : GameObject
    {
        public ushort Flags2;
        public short Unknown6;
        [MinVersion(CacheVersion.Halo3Retail)]
        public List<MetagameProperty> MetagameProperties;
        [MinVersion(CacheVersion.Halo3Retail)]
        public sbyte Unknown7;
        [MinVersion(CacheVersion.Halo3Retail)]
        public sbyte Unknown8;
        [MinVersion(CacheVersion.Halo3Retail)]
        public sbyte Unknown9;
        [MinVersion(CacheVersion.Halo3Retail)]
        public sbyte Unknown10;

        [Flags]
        public enum CrateFlagsValue : ushort
        {
            None = 0,
            DoesNotBlockAreaOfEffect = 1 << 0,
            Camera = 1 << 1
        }

        [TagStructure(Size = 0x8)]
        public class MetagameProperty
        {
            public FlagsValue Flags;
            public UnitValue Unit;
            public ClassificationValue Classification;
            public sbyte Unknown;
            public short Points;
            public short Unknown2;

            [Flags]
            public enum FlagsValue : byte
            {
                None = 0,
                MustHaveActiveSeats = 1 << 0
            }

            public enum UnitValue : sbyte
            {
                Brute,
                Grunt,
                Jackal,
                Marine,
                Bugger,
                Hunter,
                FloodInfection,
                FloodCarrier,
                FloodCombat,
                FloodPureform,
                Sentinel,
                Elite,
                Turret,
                Mongoose,
                Warthog,
                Scorpion,
                Hornet,
                Pelican,
                Shade,
                Watchtower,
                Ghost,
                Chopper,
                Mauler,
                Wraith,
                Banshee,
                Phantom,
                Scarab,
                Guntower,
                Engineer,
                EngineerRechargeStation
            }

            public enum ClassificationValue : sbyte
            {
                Infantry,
                Leader,
                Hero,
                Specialist,
                LightVehicle,
                HeavyVehicle,
                GiantVehicle,
                StandardVehicle
            }
        }
    }
}
