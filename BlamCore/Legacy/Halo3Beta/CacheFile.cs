﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.IO;

namespace BlamCore.Legacy.Halo3Beta
{
    public class CacheFile : Base.CacheFile
    {
        public CacheFile(FileInfo file, CacheVersion version = CacheVersion.Halo3Beta)
            : base(file, version)
        {
            Version = CacheVersion.Halo3Beta;

            Header = new CacheHeader(this);
            IndexHeader = new CacheIndexHeader(this);
            IndexItems = new IndexTable(this);
            Strings = new StringTable(this);

            LocaleTables = new List<LocaleTable>();
            try
            {
                for (int i = 0; i < int.Parse(buildNode.Attributes["languageCount"].Value); i++)
                    LocaleTables.Add(new LocaleTable(this, (GameLanguage)i));
            }
            catch { LocaleTables.Clear(); }
        }

        new public class CacheHeader : Base.CacheFile.CacheHeader
        {
            public CacheHeader(Base.CacheFile Cache)
            {
                cache = Cache;
                var Reader = cache.Reader;

                #region Read Values
                XmlNode headerNode = cache.versionNode.ChildNodes[0];
                XmlAttribute attr = headerNode.Attributes["fileSize"];
                int offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                fileSize = Reader.ReadInt32();

                attr = headerNode.Attributes["indexOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                indexOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["tagDataAddress"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                tagDataAddress = Reader.ReadInt32();

                attr = headerNode.Attributes["stringCount"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                stringCount = Reader.ReadInt32();

                attr = headerNode.Attributes["stringTableSize"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                stringTableSize = Reader.ReadInt32();

                attr = headerNode.Attributes["stringTableIndexOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                stringTableIndexOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["stringTableOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                stringTableOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["scenarioName"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                scenarioName = Reader.ReadString(256);

                attr = headerNode.Attributes["fileCount"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                fileCount = Reader.ReadInt32();

                attr = headerNode.Attributes["fileTableOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                fileTableOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["fileTableSize"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                fileTableSize = Reader.ReadInt32();

                attr = headerNode.Attributes["fileTableIndexOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                fileTableIndexOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["virtualBaseAddress"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                virtualBaseAddress = Reader.ReadInt32();
                #endregion

                indexOffset = indexOffset - virtualBaseAddress + tagDataAddress;
                cache.Magic = virtualBaseAddress - tagDataAddress;
            }
        }

        new public class CacheIndexHeader : Base.CacheFile.CacheIndexHeader
        {
            public CacheIndexHeader(Base.CacheFile Cache)
            {
                cache = Cache;
                var Reader = cache.Reader;

                #region Read Values
                XmlNode indexHeaderNode = cache.versionNode.ChildNodes[1];

                XmlAttribute attr = indexHeaderNode.Attributes["tagClassCount"];
                int offset = int.Parse(attr.Value);
                Reader.SeekTo(offset + cache.Header.indexOffset);
                tagClassCount = Reader.ReadInt32();

                attr = indexHeaderNode.Attributes["tagInfoOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset + cache.Header.indexOffset);
                tagInfoOffset = Reader.ReadInt32() - cache.Magic;

                attr = indexHeaderNode.Attributes["tagClassIndexOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset + cache.Header.indexOffset);
                tagClassIndexOffset = Reader.ReadInt32() - cache.Magic;

                attr = indexHeaderNode.Attributes["tagCount"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset + cache.Header.indexOffset);
                tagCount = Reader.ReadInt32();

                attr = indexHeaderNode.Attributes["tagInfoHeaderCount"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset + cache.Header.indexOffset);
                tagInfoHeaderCount = Reader.ReadInt32();

                attr = indexHeaderNode.Attributes["tagInfoHeaderOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset + cache.Header.indexOffset);
                tagInfoHeaderOffset = Reader.ReadInt32() - cache.Magic;

                attr = indexHeaderNode.Attributes["tagInfoHeaderCount2"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset + cache.Header.indexOffset);
                tagInfoHeaderCount2 = Reader.ReadInt32();

                attr = indexHeaderNode.Attributes["tagInfoHeaderOffset2"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset + cache.Header.indexOffset);
                tagInfoHeaderOffset2 = Reader.ReadInt32() - cache.Magic;
                #endregion
            }
        }

        new public class IndexTable : Base.CacheFile.IndexTable
        {
            public IndexTable(Base.CacheFile Cache)
            {
                cache = Cache;

                var indexHeader = cache.IndexHeader;
                var cacheHeader = cache.Header;
                var reader = cache.Reader;

                ClassList = new List<TagClass>();

                #region Read Class List

                reader.SeekTo(indexHeader.tagClassIndexOffset);
                for (int i = 0; i < indexHeader.tagClassCount; i++)
                {
                    TagClass tc = new TagClass();
                    tc.ClassCode = reader.ReadString(4);
                    tc.Parent = reader.ReadString(4);
                    tc.Parent2 = reader.ReadString(4);
                    tc.StringID = reader.ReadInt32();
                    ClassList.Add(tc);
                }
                #endregion

                #region Read Tags' Info
                reader.SeekTo(indexHeader.tagInfoOffset);
                for (int i = 0; i < indexHeader.tagCount; i++)
                {
                    IndexItem item = new IndexItem() { Cache = cache };
                    item.ClassIndex = reader.ReadInt16();
                    item.ID = (reader.ReadInt16() << 16) | i;
                    item.Offset = reader.ReadInt32() - cache.Magic;
                    item.metaIndex = i;
                    this.Add(item);
                }
                #endregion

                #region Read Indices
                reader.SeekTo(cacheHeader.fileTableIndexOffset);
                int[] indices = new int[indexHeader.tagCount];
                for (int i = 0; i < indexHeader.tagCount; i++)
                    indices[i] = reader.ReadInt32();
                #endregion

                #region Read Names
                reader.SeekTo(cacheHeader.fileTableOffset);

                EndianReader newReader = null;

                if (cache.TagsKey == "" || cache.TagsKey == null)
                {
                    newReader = new EndianReader(new MemoryStream(reader.ReadBytes(cacheHeader.fileTableSize)), EndianFormat.BigEndian);
                }
                else
                {
                    reader.BaseStream.Position = cacheHeader.fileTableOffset;
                    newReader = new EndianReader(reader.DecryptAesSegment(cacheHeader.fileTableSize, cache.TagsKey), EndianFormat.BigEndian);
                }

                for (int i = 0; i < indices.Length; i++)
                {
                    if (indices[i] == -1)
                    {
                        this[i].Filename = "<null>";
                        continue;
                    }

                    newReader.SeekTo(indices[i]);

                    int length;
                    if (i == indices.Length - 1)
                        length = cacheHeader.fileTableSize - indices[i];
                    else
                    {
                        if (indices[i + 1] == -1)
                        {
                            int index = -1;

                            for (int j = i + 1; j < indices.Length; j++)
                            {
                                if (indices[j] != -1)
                                {
                                    index = j;
                                    break;
                                }
                            }

                            length = (index == -1) ? cacheHeader.fileTableSize - indices[i] : indices[index] - indices[i];
                        }
                        else
                            length = indices[i + 1] - indices[i];
                    }

                    if (length == 1)
                    {
                        this[i].Filename = "<blank>";
                        continue;
                    }

                    if (length < 0)
                    {
                        int i0 = indices[i];
                        int i1 = indices[i + 1];
                        int i2 = indices[i + 2];
                        int i3 = indices[i + 3];
                    }

                    this[i].Filename = newReader.ReadString(length);
                }

                newReader.Close();
                newReader.Dispose();
                #endregion
            }
        }

        public override void LoadResourceTags()
        {
            foreach (IndexItem item in IndexItems)
            {
                if (item.ClassCode == "zone")
                {
                    ResourceGestalt = new cache_file_resource_gestalt(this, item.Offset);
                    break;
                }
            }
        }

        public override byte[] GetRawFromID(int ID, int DataLength)
        {
            EndianReader er;
            string fName = "";

            var Entry = (Halo3Beta.cache_file_resource_gestalt.RawEntry)ResourceGestalt.DefinitionEntries[ID & ushort.MaxValue];

            var offset = (Entry.OptionalOffset > 0) ? Entry.OptionalOffset : Entry.RequiredOffset;
            var size = (Entry.OptionalSize > 0) ? Entry.OptionalSize : Entry.RequiredSize;

            if (DataLength > size)
                size = DataLength;

            if (Entry.CacheIndex != -1)
            {
                fName = File.DirectoryName + "\\shared.map";
                FileStream fs = new FileStream(fName, FileMode.Open, FileAccess.Read);
                er = new EndianReader(fs, EndianFormat.BigEndian);
            }
            else
                er = Reader;

            er.SeekTo(offset);
            var data = er.ReadBytes(size);

            if (er != Reader)
            {
                er.Close();
                er.Dispose();
            }

            return data;
        }
    }
}
