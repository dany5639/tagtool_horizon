﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Xml;
using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.IO;

namespace BlamCore.Legacy.Halo3Retail
{
    public class CacheFile : Base.CacheFile
    {
        public CacheFile(FileInfo file, CacheVersion version = CacheVersion.Halo3Retail)
            : base(file, version)
        {
            Header = new CacheHeader(this);
            IndexHeader = new Halo3Beta.CacheFile.CacheIndexHeader(this);
            IndexItems = new Halo3Beta.CacheFile.IndexTable(this);
            Strings = new StringTable(this);

            LocaleTables = new List<LocaleTable>();
            try
            {
                for (int i = 0; i < int.Parse(buildNode.Attributes["languageCount"].Value); i++)
                    LocaleTables.Add(new LocaleTable(this, (GameLanguage)i));
            }
            catch { LocaleTables.Clear(); }
        }

        new public class CacheHeader : Base.CacheFile.CacheHeader
        {
            public CacheHeader(Base.CacheFile Cache)
            {
                cache = Cache;
                var Reader = cache.Reader;

                #region Read Values
                XmlNode headerNode = cache.versionNode.ChildNodes[0];
                XmlAttribute attr = headerNode.Attributes["fileSize"];
                int offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                fileSize = Reader.ReadInt32();

                attr = headerNode.Attributes["indexOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                indexOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["tagDataAddress"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                tagDataAddress = Reader.ReadInt32();

                attr = headerNode.Attributes["stringCount"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                stringCount = Reader.ReadInt32();

                attr = headerNode.Attributes["stringTableSize"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                stringTableSize = Reader.ReadInt32();

                attr = headerNode.Attributes["stringTableIndexOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                stringTableIndexOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["stringTableOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                stringTableOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["scenarioName"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                scenarioName = Reader.ReadString(256);

                attr = headerNode.Attributes["fileCount"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                fileCount = Reader.ReadInt32();

                attr = headerNode.Attributes["fileTableOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                fileTableOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["fileTableSize"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                fileTableSize = Reader.ReadInt32();

                attr = headerNode.Attributes["fileTableIndexOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                fileTableIndexOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["virtualBaseAddress"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                virtualBaseAddress = Reader.ReadInt32();
                attr = headerNode.Attributes["rawTableOffset"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                rawTableOffset = Reader.ReadInt32();

                attr = headerNode.Attributes["localeModifier"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                localeModifier = Reader.ReadInt32();

                attr = headerNode.Attributes["rawTableSize"];
                offset = int.Parse(attr.Value);
                Reader.SeekTo(offset);
                rawTableSize = Reader.ReadInt32();
                #endregion

                #region Modify Offsets
                if (rawTableOffset == 0)
                {
                    cache.Magic = virtualBaseAddress - tagDataAddress;
                }
                else
                {

                    this.Magic = stringTableIndexOffset - cache.HeaderSize;

                    fileTableOffset -= this.Magic;
                    fileTableIndexOffset -= this.Magic;
                    stringTableIndexOffset -= this.Magic;
                    stringTableOffset -= this.Magic;

                    cache.Magic = virtualBaseAddress - (rawTableOffset + rawTableSize);
                }
                indexOffset -= cache.Magic;
                #endregion
            }
        }

        public override void LoadResourceTags()
        {
            foreach (IndexItem item in IndexItems)
            {
                if (item.ClassCode == "play")
                {
                    if (item.Offset > Reader.Length)
                    {
                        foreach (IndexItem item2 in IndexItems)
                        {
                            if (item2.ClassCode == "zone")
                            {
                                //fix for H4 prologue, play address is out of 
                                //bounds and data is held inside the zone tag 
                                //instead so make a fake play tag using zone data
                                item.Offset = item2.Offset + 28;
                                break;
                            }
                        }
                    }

                    ResourceLayoutTable = new cache_file_resource_layout_table(this, item.Offset);
                    break;
                }
            }

            foreach (IndexItem item in IndexItems)
            {
                if (item.ClassCode == "zone")
                {
                    ResourceGestalt = new cache_file_resource_gestalt(this, item.Offset);
                    break;
                }
            }

            foreach (IndexItem item in IndexItems)
            {
                if (item.ClassCode == "ugh!")
                {
                    SoundGestalt = new sound_cache_file_gestalt(this, item.Offset);
                    break;
                }
            }
        }

        public override byte[] GetRawFromID(int ID, int DataLength)
        {
            EndianReader er;
            string fName = "";

            var Entry = ResourceGestalt.DefinitionEntries[ID & ushort.MaxValue];

            if (Entry.SegmentIndex == -1) return null;

            var Loc = ResourceLayoutTable.Segments[Entry.SegmentIndex];

            //if (Loc.SoundRawIndex != -1)
            //    return GetSoundRaw(ID);

            int index = (Loc.OptionalPageIndex2 != -1) ? Loc.OptionalPageIndex2 : (Loc.OptionalPageIndex != -1) ? Loc.OptionalPageIndex : Loc.RequiredPageIndex;
            int locOffset = (Loc.OptionalPageOffset2 != -1) ? Loc.OptionalPageOffset2 : (Loc.OptionalPageOffset != -1) ? Loc.OptionalPageOffset : Loc.RequiredPageOffset;

            if (index == -1 || locOffset == -1) return null;

            if (ResourceLayoutTable.Pages[index].RawOffset == -1)
            {
                index = Loc.RequiredPageIndex;
                locOffset = Loc.RequiredPageOffset;
            }

            var Pool = ResourceLayoutTable.Pages[index];

            if (Pool.CacheIndex != -1)
            {
                fName = ResourceLayoutTable.SharedCaches[Pool.CacheIndex].FileName;
                fName = fName.Substring(fName.LastIndexOf('\\'));
                fName = File.DirectoryName + fName;

                if (fName == File.FullName)
                    er = Reader;
                else
                {
                    var fs = new FileStream(fName, FileMode.Open, FileAccess.Read);
                    er = new EndianReader(fs, EndianFormat.BigEndian);
                }
            }
            else
                er = Reader;

            er.SeekTo(int.Parse(versionNode.ChildNodes[0].Attributes["rawTableOffset"].Value));
            int offset = Pool.RawOffset + er.ReadInt32();
            er.SeekTo(offset);
            byte[] compressed = er.ReadBytes(Pool.CompressedSize);
            byte[] decompressed = new byte[Pool.DecompressedSize];

            BinaryReader BR = new BinaryReader(new DeflateStream(new MemoryStream(compressed), CompressionMode.Decompress));
            decompressed = BR.ReadBytes(Pool.DecompressedSize);
            BR.Close();
            BR.Dispose();

            byte[] data = new byte[(DataLength != -1) ? DataLength : (Pool.DecompressedSize - locOffset)];
            int length = data.Length;
            if (length > decompressed.Length) length = decompressed.Length;
            Array.Copy(decompressed, locOffset, data, 0, length);

            if (er != Reader)
            {
                er.Close();
                er.Dispose();
            }

            return data;
        }

        public override byte[] GetSoundRaw(int ID, int size)
        {
            var Entry = ResourceGestalt.DefinitionEntries[ID & ushort.MaxValue];

            if (Entry.SegmentIndex == -1) throw new InvalidDataException("Raw data not found.");

            var segment = ResourceLayoutTable.Segments[Entry.SegmentIndex];
            var sRaw = ResourceLayoutTable.SoundRawChunks[segment.SoundRawIndex];
            var reqPage = ResourceLayoutTable.Pages[segment.RequiredPageIndex];
            var optPage = ResourceLayoutTable.Pages[segment.OptionalPageIndex];

            if (size == 0) size = (reqPage.CompressedSize != 0) ? reqPage.CompressedSize : optPage.CompressedSize;

            var reqSize = size - sRaw.RawSize;
            var optSize = size - reqSize;

            //if (reqPage.CompressedSize != reqPage.DecompressedSize || optPage.CompressedSize != optPage.DecompressedSize)
            //    throw new Exception("COMPRESSED DATA");

            //if (sRaw.Sizes.Count > 1)
            //    throw new Exception("MULTIPLE SEGMENTS");

            byte[] buffer;
            byte[] data = new byte[size];
            int offset;
            EndianReader er;
            string fName = "";

            #region REQUIRED
            if (reqSize > 0)
            {
                if (reqPage.CacheIndex != -1)
                {
                    fName = ResourceLayoutTable.SharedCaches[reqPage.CacheIndex].FileName;
                    fName = fName.Substring(fName.LastIndexOf('\\'));
                    fName = File.DirectoryName + fName;

                    if (fName == File.FullName)
                        er = Reader;
                    else
                        er = new EndianReader(new FileStream(fName, FileMode.Open, FileAccess.Read), EndianFormat.BigEndian);
                }
                else
                    er = Reader;

                er.SeekTo(1136);
                offset = reqPage.RawOffset + er.ReadInt32();

                er.SeekTo(offset);
                buffer = er.ReadBytes(reqPage.CompressedSize);

                Array.Copy(buffer, segment.RequiredPageOffset, data, 0, reqSize);

                if (er != Reader)
                {
                    er.Close();
                    er.Dispose();
                }
            }
            #endregion

            #region OPTIONAL
            if (segment.OptionalPageIndex != -1 && optSize > 0)
            {
                if (optPage.CacheIndex != -1)
                {
                    fName = ResourceLayoutTable.SharedCaches[optPage.CacheIndex].FileName;
                    fName = fName.Substring(fName.LastIndexOf('\\'));
                    fName = File.DirectoryName + fName;

                    if (fName == File.FullName)
                        er = Reader;
                    else
                        er = new EndianReader(new FileStream(fName, FileMode.Open, FileAccess.Read), EndianFormat.BigEndian);
                }
                else
                    er = Reader;

                er.SeekTo(1136);
                offset = optPage.RawOffset + er.ReadInt32();

                er.SeekTo(offset);
                buffer = er.ReadBytes(optPage.CompressedSize);

                if (buffer.Length > data.Length)
                    data = buffer;
                else
                    Array.Copy(buffer, segment.OptionalPageOffset, data, reqSize, optSize);


                if (er != Reader)
                {
                    er.Close();
                    er.Dispose();
                }
            }
            #endregion

            return data;
        }
    }
}
